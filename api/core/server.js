"use strict";
require('babel-register');

const env = process.env.NODE_ENV || require('./package.json')['NODE_ENV'];

process.env.NODE_ENV = env;
process.configure = require('./config')(env);
const db = require('./db/index');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const app = require('./app');
const logUtils = require('./util/log');
let works = [];

// //负载均衡
if (cluster.isMaster) {
    console.log("master start in [" + process.env.NODE_ENV + "] model ...");

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        let worker = cluster.fork();
        works.push(worker);
    }
    cluster.on('listening', function(worker, address) {
        logUtils.iotlog.error('listening: worker ' + worker.process.pid + ', Address: ' + address.address + ":" + address.port);
    });

    cluster.on('exit', function(worker, code, signal) {
        logUtils.iotlog.error('worker ' + worker.process.pid + ' died');
        process.exit(0);
    });
    cluster.setMaxListeners(0);
} else {
    try {
        app.start();
    } catch (err) {
        console.error('starterr', err);
        logUtils.iotlog.error('appServer.start error', err);
    }
}