var path = require('path');
module.exports = {
    "appenders": [{
        "type": "dateFile",
        'filename': path.format({ dir: '/home/MZLL/logs/iot', base: 'main.log' }),
        "pattern": "-yyyy-MM-dd",
        "alwaysIncludePattern": true,
        "category": "main"
    }, {
        "type": "dateFile",
        'filename': path.format({ dir: '/home/MZLL/logs/iot', base: 'mongo.log' }),
        "pattern": "-yyyy-MM-dd",
        "alwaysIncludePattern": true,
        "category": "mongo"
    }]
};