var path = require('path');
var logConfig = require('./_config_log');

module.exports = {
    log4js: logConfig,
    apiport: 3055,
    servicePort: 3056,
    SMS: {
        host: '127.0.0.1',
        port: '2020',
        path: '/SMSSend',
        password: 'password',
        SP_CODE: 'sp_code',
        CUST_CODE: 'cust_code'
    },
    ordercode: {
        '200': '成功',
        '201': '订单处理中',
        '530': '失败',
        '630': '订单已取消',
        '501': '无法匹配运营商',
        '502': 'sign签名错误',
        '503': '无法执行此操作',
        '507': '缺少必要接口参数',
        '509': '参数长度不合法',
        '511': '内部异常',
        '514': 'ip不在白名单',
        '515': '该客户不存在',
    },
    redis: {
        host: '127.0.0.1',
        port: '6379',
        index: 5
    }
};