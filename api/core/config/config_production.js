var path = require('path');
var logConfig = require('./_config_log');

module.exports = {
    log4js: logConfig,
    apiport: 3055,
    servicePort: 3056,
    SMS: {
        host: '123.58.255.70',
        port: '8860',
        path: '',
        password: 'R7N7J3OMM6',
        SP_CODE: '1069040254000315',
        CUST_CODE: '300166'
    },
    ordercode: {
        '200': '成功',
        '201': '订单处理中',
        '530': '失败',
        '630': '订单已取消',
        '501': '无法匹配运营商',
        '502': 'sign签名错误',
        '503': '无法执行此操作',
        '507': '缺少必要接口参数',
        '509': '参数长度不合法',
        '511': '内部异常',
        '514': 'ip不在白名单',
        '515': '该客户不存在',
    },
    redis: {
        host: 'e6171efc867b4d2b644.redis.rds.aliyuncs.com',
        port: '6379',
        auth: 'QMmzlltime2016',
        index: 5
    }
};