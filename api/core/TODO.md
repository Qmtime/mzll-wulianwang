(一)WATCH OUT!
	1.联通返回下单结果中的orderid,是联通生成的订单；
	2.移动回调接口是字符串

(二)TODO!
x	1.增加对返回状态码的数据库记录
x	2.对于联通的超时异议：他们会返回10067？-》问题：第一次超时生产一次订单，重新请求再超时，就会生产2个订单。
x	3.超时需调用查询接口，是否重新发起请求。==》a.超时是发起查询接口；b.创建订单是绑定settimtout时间自动查询
x	4.更新数据库增加容错。
x	5.根据运营商返回的失败数据进行构造存储。
x	6.电信查询接口暂时不能测试+超时的订单查询,还有start_time,end_time?????
x	7.当内部发生不太严重的异常（比如更新数据库失败）时，是否返回用户异常信息，还是返回从运营商得到的构造消息。
x	8.在订单数据库中增加运营商的字段。
x	9.将运营商的错误归结为“其他错误”.
	10.log4js.
x	11.下游余额的验证;
x	12.增加号码归属地查询接口.
x	13.为数据库创建索引.
	14.获取token是https.？？
x	15.增加ip白名单；
x	16.增加合同
x	17.增加了charge_time，看着办吧
	19.细节流程图的增加
	20.用户扣款->	  
    只有充值成功才会扣款:回调充值成功扣款，还是查询的时候扣款？：答：首先是回调充值成功后，看是否有扣款记录，否则进行一次扣款，写进订单数据库，其次是在查询时，如果订单数据库有扣款记录，就不用扣款,还有联通没有回调接口呢。。。。
    21.增加数据库记录的缓存
x	21.验证失败的订单需要存入数据库吗？
x	22.order中没有记录customer_id.
!!!	23.将电信查询补充完整
x	24.开关
x	25.对下单参数验证：还要验证当前用户手机号，token是不是这个用户的！目前只是做了解密的处理
	26.增加sequelize-session
	27.将model层独立，两个系统都公用model层
	28.暂时在产生订单时没有加入活动
	29.api补充
x	30.回调接口补充完整
x	31.返回客户端时加上平台的订单号
x	32.测试时模仿运营商进行返回
	33.注意电信msg_id-平台受理唯一标识
x	34.user表字段还没加呢
	35.云运营商的订单号没入库
（三）QUESTION!
	1.移动CRMApplyCode啥意思？
	2.移动channelid?
（四）THINk!
x	1.mongo连接池.
x	2.mongo缓存.
	3.日志系统.
x	4.返回给客户端的状态码头是经过组织过的，而记录到数据库中的应该是运营商原始返回的状态码,等到手动查询的时候返回运营商对错误码的说明。
x	5.在手动查询是，应该返回运营商的原始解释。
	6.思考借鉴面向对象思想，抽象出充值+查询接口。
（五）CHANGES	!
	1.product,productOrder中各种price不应该是float(4,2);
	2.vendor_order_no不是非null
	3.order增加了customer_id字段。


超时有误！