/**
 * 微信模板消息
//  */
var WechatAPI = require('wechat-api');
var wechatConfig = require('../config/wechat')(process.env.NODE_ENV);

const RedisKeys = require('../db/REDIS_KEYS');
const redis = require("../db/redisdb");

var wechat_api = new WechatAPI(wechatConfig.messageTemplage.appId, wechatConfig.messageTemplage.appSecret, function (callback) {
    redis.getAsync(RedisKeys.REDIS_KEY_PRE_OFFIACCOUNT_TOKEN).then(txt => {
        "use strict";
        callback(null, JSON.parse(txt));
    }).catch(err => {
        callback(err);
    });
}, function (token, callback) {
    redis.setAsync(RedisKeys.REDIS_KEY_PRE_OFFIACCOUNT_TOKEN, JSON.stringify(token)).then(data=> {
        callback(data, null);
    }).catch(reason => {
        callbcak(reason, null);
    });
});

/**
 * wechat send Template
 * @param {String} openid 用户的openid
 * @param {String} templateId 模板ID
 * @param {String} url URL置空，则在发送后，点击模板消息会进入一个空白页面（ios），或无法点击（android）
 * @param {Object} data 渲染模板的数据
 * @param {Function} callback 回调函数
 * @returns {*}
 */
exports.WECHAT_sendTemplate = function(openid, templateId, url, data, callback) {
    "use strict";
    wechat_api.sendTemplate(openid, templateId, url, data, callback);
};
