"use strict";

var log = require('log4js').getLogger('responseHelper');

function reswapper(res) {
    res.badRequest = function() {
        res.end(res.writeHead(400));
    };

    res.badGateway = function() {
        res.end(res.writeHead(502));
    };
    res.recallback = function(Response) {
        if (typeof Response == 'object') {
            Response['info'] = process.configure.ordercode[Response['code']];
        };
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(Response));
        res.end();
    };
    res.timeout = function() {
        res.writeHead(408);
        res.write(JSON.stringify({ 'info': 'timeout' }));
        res.end();
    };
}

exports.reswapper = reswapper;