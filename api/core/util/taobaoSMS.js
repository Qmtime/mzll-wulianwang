"use strict"
const request = require('request'),
    utils = require('./index'),
    moment = require('moment'),
    path = require('path'),
    querystring = require('querystring'),
    config = require('../provider/config/taobao.js'),
    logUtils = require('./log'),
    DayvSMS = require('../../../libs/DayvSMS'),
    db = require(path.join(__dirname, '..', '..', '..', 'model-public', 'sequelize')).init(process.env.NODE_ENV),
    SMSOrder = db.SMSOrder;
var taobaoUtil = new DayvSMS();

function taobaoSMS() {

}

taobaoSMS.prototype.sendMsg = function(phone) {
    var paramObj = {
        method: 'alibaba.aliqin.fc.sms.num.send',
        app_key: config.app_key,
        sign_method: config.sign_method,
        timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
        format: config.format,
        v: config.v,
        sms_type: 'normal',
        sms_free_sign_name: '流量提醒',
        rec_num: phone,
        sms_template_code: "SMS_47940011"
    };
    paramObj['sign'] = utils.md5(config.app_secret + utils.sortObj(paramObj) + config.app_secret).toUpperCase();
    var options = {
        url: 'http://gw.api.taobao.com/router/rest?' + querystring.stringify(paramObj),
        method: 'GET'
    };
    request(options, (err, re, body) => {
        logUtils.smslog.info('taobaoSMS sendMsg phone:', phone, err, body);
        body = JSON.parse(body);
        //记录数据库
        if (body['alibaba_aliqin_fc_sms_num_send_response']) {
            body = body['alibaba_aliqin_fc_sms_num_send_response'];
            var smsObj = {
                sms_mobile: phone,
                sms_contents: "SMS_47940011",
                sms_provider_order: body['request_id'],
                sms_send_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                sms_send_status: body['result']['success']
            };
        } else {
            //发送失败
            var smsObj = {
                sms_mobile: phone,
                sms_contents: "SMS_47940011",
                sms_send_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                sms_send_status: "failed"
            };
        }
        SMSOrder.create(smsObj).then(data => {}).catch(err => {
            logUtils.smslog.info('taobaoSMS create phone:', phone, err);
        });
    });
};
/**
 * 流量提醒
 * @return {[type]} [description]
 */
taobaoSMS.prototype.remindMsg = function(msgObj, paramMsgObj, callback) {
    taobaoUtil.remindMsg(msgObj, paramMsgObj['phone'], paramMsgObj['sms_template_code'], callback);
};

taobaoSMS.prototype.sendMessageToDiDiFailed = function(customer, time, service, reason, price, server, code, mobile, callback) {
    taobaoUtil.sendMessageToDiDiFailed(customer, time, service, reason, price, server, code, mobile, (err, data) => {});
};
taobaoSMS.prototype.queryMsg = function(phone) {
    var paramObj = {
        method: 'alibaba.aliqin.fc.sms.num.query',
        app_key: config.app_key,
        sign_method: config.sign_method,
        timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
        format: config.format,
        v: config.v,
        rec_num: phone,
        query_date: moment().format('YYYYMMDD'),
        current_page: 1,
        page_size: 10
    };
    paramObj['sign'] = utils.md5(config.app_secret + utils.sortObj(paramObj) + config.app_secret).toUpperCase();
    var options = {
        url: 'http://gw.api.taobao.com/router/rest?' + querystring.stringify(paramObj),
        method: 'GET'
    };
    request(options, (err, re, body) => {
        logUtils.smslog.info('taobaoSMS queryMsg phone:', phone, err, body);
    });

};
module.exports = taobaoSMS;