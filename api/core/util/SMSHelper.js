"use strict";
var crypto = require('crypto'),
    request = require('request'),
    logUtils = require('./log'),
    http = require('http');


function SMSHelper() {
    "use strict";

}

/**
 *
 * 发送短信
 * 目前单个手机号的日上限设置为10条, 超过短信在未来无线将会被屏蔽
 *
 * @param contents 所要发送短信的内容, 固定短信和变量短信的内容都在于此
 * @param params 发送短信对应的参数数组, 其中其中包含手机号码和对应变量参数, 如 1391212121212|para1|para2
 * @param callback 发送短信结束后的回调函数
 * */
SMSHelper.prototype.sendSMS = function(contents, params, callback) {
    "use strict";
    if(contents.indexOf('【手机充值】') == -1){
        contents = '【拇指流量】' + contents;
    }
    var postData = _buildMessageSendData(contents, params);
    const options = {
        host: process.configure.SMS.host,
        port: process.configure.SMS.port,
        path: process.configure.SMS.path,
        // 必须设置http headers, 否则失败
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8',
            'Connection': 'keep-alive',
            'Content-Length': postData.length
        },
        method: 'POST'
    };
    var request = http.request(options);
    request.on('error', function(error) {
        logUtils.mzlllog.error('mzll->sendSMS : post error', error);
        callback(error, null);
    });
    request.on('response', function(client) {
        var buffers = [];
        client.on('data', function(chunk) {
            buffers.push(chunk);
        });
        client.on('end', function() {
            var result = Buffer.concat(buffers).toString();
            logUtils.smslog.info(' SMSHelper => sendSMS response -> ', result);
            if (result.indexOf('SUCCESS:') === 0) {
                result = result.split('\r\n');
                result.splice(0, 1);
                result.splice(result.length - 1, 1);
                callback(null, result);
            } else {
                callback(result, null);
            }
        })
    });
    logUtils.smslog.info(' SMSHelper => sendSMS request -> ', postData);
    request.write(postData);
    request.end();

};

SMSHelper.prototype.sendSimpleSMSes = function(content, mobiles) {
    for (const mobile in mobiles) {
        this.sendSMS(content, mobile, function(err, results) {
            if (err) {
                console.error('error', err);
            } else if (results) {
                if (results) {
                    const SMSdb = require('../db/SMSdb');
                    results.forEach(function(result) {
                        result = SMSHelper.parseSendResponseResult(result);
                        SMSdb.generate_SMS_OrderPO(mobile, result.sms_provider_order, content, result.sms_send_status, function(err, result) {
                            if (err) {
                                return;
                            }
                        });
                    });
                }
            }
        });
    }
};

// 内建短信POST发送数据
function _buildMessageSendData(contents, params) {
    "use strict";
    var custCode = process.configure.SMS.CUST_CODE;
    var spCode = process.configure.SMS.SP_CODE;
    var codedContent = encodeURIComponent(contents);
    var unionMobiles = encodeURIComponent(_unionParams(params));
    var sign = _getContentSign(codedContent);
    return 'cust_code=' + custCode + '&sp_code=' + spCode + '&content=' + codedContent + '&destMobiles=' + unionMobiles + '&sign=' + sign;
}

// 对参数进行拼接
function _unionParams(params) {
    "use strict";
    var union = params[0];
    for (var i = 1; i < params.length; i++) {
        union += ',';
        union += params[i];
    }
    return union;
}

// 短信内容URL编码后+客户密码的字符串进行MD5加密
function _getContentSign(encodedContents) {
    "use strict";
    var str = encodedContents + process.configure.SMS.password;
    return crypto.createHash('md5').update(str).digest('hex');
}

SMSHelper.convertTemplate = function(template, params) {
    "use strict";
    if (!template||!params) {
        return;
    }
    params = params.split('|');
    for (var i = 0; i < params.length; i++) {
        var _var = '{$var' + i + '}';
        template = template.replace(_var, params[i]);
    }
    return template;
};
/**
 * 13581830325:59106103191341980039:0
 * */
SMSHelper.parseSendResponseResult = function(responseResult) {
    "use strict";
    let params = responseResult.split(':');
    return {
        sms_mobile: params[0],
        sms_provider_order: params[1],
        sms_send_status: params[2]
    };
};

module.exports = SMSHelper;
