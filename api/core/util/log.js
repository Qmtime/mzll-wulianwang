"use strict";
const log4js = require('log4js');
log4js.configure(process.configure.log4js);

const iotlog = log4js.getLogger('main');
iotlog.setLevel('ALL');


module.exports = {
    iotlog: iotlog
};