"use strict"
const util = require('util'),
    crypto = require('crypto'),
    moment = require('moment'),
    uuid = require('uuid'),
    parseString = require('xml2js').parseString,
    xml = require('xml'),
    random = require(('random-gen')),
    os = require('os'),
    to_json = require('xmljson').to_json,
    objectSort = require('object-sort'),
    sha256Util = require('js-sha256');

/**
 * 生成num位订单号
 * @return {[type]} [description]
 */
exports.generateRandom = function(num) {
    return moment().format('YYMMDDHHmmssSSS') + Math.floor(Math.random() * Math.pow(10, num));
};

/**
 * 生成md5加密
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */

exports.md5 = function(str) {
    var md5 = crypto.createHash('md5').update(str).digest('hex');
    return md5;
};


exports.sha1 = function(str) {
    var sha1 = crypto.createHash('SHA1').update(str).digest('hex');
    return sha1;
};

exports.sha256base64 = function(str) {
    var sha1 = crypto.createHash('SHA256').update(str).digest('base64');
    return sha1;

};

exports.sha1Hmac = function(str, key) {
    var sha1 = crypto.createHmac('SHA1', key).update(str).digest('hex');
    return sha1;
};
exports.sha256Hmac = function(str, key) {
    return sha256Util.hmac(key, str);
};

exports.sortObj = function(signobj) {
    signobj = objectSort(signobj);
    var parmstr = '';
    for (var i in signobj) {
        parmstr += i + signobj[i];
    }
    return parmstr;
};

/**
 * 将xml解析成json
 * @param  {[type]} xml [description]
 * @return {[type]}     [description]
 */
exports.xmlJson = function(xmlstr, callback) {
    parseString(xmlstr, function(err, result) {
        callback(result);
    });
};
/**
 * 将json转成xml
 * @param  {[type]}   obj      [description]
 * @param  {[type]}   option   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
exports.jsonToXml = function(obj, option) {
    return xml(obj, option);
};

/**
 * 生成16位的随机字符串
 * @return {[type]} [description]
 */
exports.randomToken = function() {
    return _randomStr(16);
};

exports.random = function(length) {
    return _randomStr(length);
};

function _randomStr(size) {
    "use strict";
    const TEMPLATE = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const MAX = TEMPLATE.length;
    var result = '';
    for (var i = 0; i < size; i++) {
        var index = parseInt(Math.random() * MAX);
        result = result + TEMPLATE.charAt(index);
    }
    return result;
}
/**
 * 随机整数
 * @param  {[type]} length [description]
 * @return {[type]}        [description]
 */
exports.randomInteger = function(length) {
    "use strict";
    const TEMPLATE = '0123456789',
        MAX = TEMPLATE.length;
    var result = '';
    for (var i = 0; i < length; i++) {
        var index = parseInt(Math.random() * MAX);
        result = result + TEMPLATE.charAt(index);
    }
    return result;
};

/**
 * 根据主机名区分服务器
 * @return {[type]} [description]
 */
exports.gethostname = function() {
    var hst = os.hostname();
    var host = 'oth';
    return hst.split('_')[1] ? hst.split('_')[1] : host;
};

/**
 * xml转json的直接方式，比xmlJson方便
 * @return {[type]} [description]
 */
exports.xml_to_json = function(xml) {
    let handlePromise = new Promise((resolve, reject) => {
        to_json(xml, function(error, data) {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
    return handlePromise;
};
/**
 * 将key:value构造成非url转码的form形式
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
exports.constructQueryString = function(obj) {
    var urlstr = "";
    for (var param in obj) {
        if (typeof obj[param] === 'object') {
            obj[param] = JSON.stringify(obj[param]);
        }
        urlstr += param + '=' + obj[param] + '&';
    };
    urlstr = urlstr.substr(0, urlstr.lastIndexOf('&'));
    return urlstr;
};
/**
 * 根据mustproperty参数来生成md5 sign
 * @param  {[type]} params       [description]
 * @param  {[type]} mustproperty [description]
 * @return {[type]}              [description]
 */
exports.generateSign = function(params, mustproperty) {
    params = objectSort(params);
    let parmstr = '';
    const filterKeys = Object.keys(params).filter(value => { return mustproperty.indexOf(value) > -1 });
    for (let i of filterKeys) {
        if (params.hasOwnProperty(i)) {
            parmstr += i + params[i];
        }
    };

    var md5 = crypto.createHash('md5').update(parmstr + params['appsecret']).digest('hex');
    return md5;
}
exports.isContainedArray = function(source, destination) {
    if (!(source instanceof Array) || !(destination instanceof Array) || ((destination.length < source.length))) {
        return false;
    }
    var destinationStr = destination.toString();
    for (var i = 0; i < source.length; i++) {
        if (destinationStr.indexOf(source[i]) < 0) return false;
    }
    return true;
}
exports.destructProviderByPath = function(path) {
    let index = path.lastIndexOf('/');
    return path.substring(index + 1, path.length);
}
exports.filterObjectNullProperty = function(obj) {
    const keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
        const value = obj[keys[i]];
        if (value === null) {
            //值为null，删除对应的key
            delete obj[keys[i]];
        }
    }
    return obj;
}