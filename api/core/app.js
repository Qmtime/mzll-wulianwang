"use strict";
require('babel-register');

var http = require('http'),
    path = require('path'),
    resHelper = require('./util/resHelper'),
    fs = require('fs'),
    clientIp = require('client-ip'),
    url = require('url'),
    utils = require('./util'),
    logUtils = require('./util/log'),
    db = require('./db/index'),
    moment = require('moment'),
    queryString = require('querystring');


exports.start = function() {
    http.createServer(function(request, response) {
        resHelper.reswapper(response);

        if (request.method == "POST") {
            //*************资产管理**************
            if (/^\/iot\/assetManage\/query_groupInfo/.test(request.url)) {
                //集团客户信息查询
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;

                        const api = require('./api/assetManage/query_groupInfo');

                        const result = await api.query_groupInfo(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`queryCardInfo error`, err);
                        logUtils.iotlog.error(`queryCardInfo error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_memberInfo/.test(request.url)) {
                //单个号卡信息查询
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_memberInfo');
                        const result = await api.query_memberInfo(params);
                        response.recallback(result);
                    } catch (err) {
                        logUtils.iotlog.error(`query_memberInfo error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_batchMemberInfo/.test(request.url)) {
                //分批号卡信息查询
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_memberInfo');
                        const result = await api.query_batchMemberInfo(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`query_batchMemberInfo error`, err);
                        logUtils.iotlog.error(`query_batchMemberInfo error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_activeTime/.test(request.url)) {
                //查询单个号卡进入正使用期的时间
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_activeTime');
                        const result = await api.query_activeTime(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`query_activeTime error`, err);
                        logUtils.iotlog.error(`query_activeTime error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_batchActiveTime/.test(request.url)) {
                //查询分批号卡进入正使用期的时间
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_activeTime');
                        const result = await api.query_batchActiveTime(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`query_activeTime error`, err);
                        logUtils.iotlog.error(`query_activeTime error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_lifeCycle/.test(request.url)) {
                // 查询单个号卡当前生命周期状态
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_lifeCycle');
                        const result = await api.query_lifeCycle(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`query_lifeCycle error`, err);
                        logUtils.iotlog.error(`query_lifeCycle error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/query_batchLifeCycle/.test(request.url)) {
                // 查询分批号卡当前生命周期状态
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/query_lifeCycle');
                        const result = await api.query_batchLifeCycle(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`query_batchLifeCycle error`, err);
                        logUtils.iotlog.error(`query_batchLifeCycle error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/change_lifeCycle/.test(request.url)) {
                // 对单个号卡的生命周期进行变更
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/change_lifeCycle');
                        const result = await api.change_lifeCycle(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`change_lifeCycle error`, err);
                        logUtils.iotlog.error(`change_lifeCycle error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/change_batchLifiCycle/.test(request.url)) {
                // 对单个号卡的生命周期进行变更
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/change_lifeCycle');
                        const result = await api.change_batchLifiCycle(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`change_batchLifiCycle error`, err);
                        logUtils.iotlog.error(`change_batchLifiCycle error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/switch_member/.test(request.url)) {
                // 对单个号卡的生命周期进行变更
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/switch_member');
                        const result = await api.switch_member(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`switch_member error`, err);
                        logUtils.iotlog.error(`switch_member error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/switch_batchMember/.test(request.url)) {
                // 对单个号卡的生命周期进行变更
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/switch_member');
                        const result = await api.switch_batchMember(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`switch_batchMember error`, err);
                        logUtils.iotlog.error(`switch_batchMember error`, err);
                    }
                });
            } else if (/^\/iot\/assetManage\/switch_service/.test(request.url)) {
                // 对单个号卡的生命周期进行变更
                var clientip = clientIp(request);
                var params = [];
                request.on('data', function(chunk) {
                    params.push(chunk);
                });
                request.on('end', async function() {
                    try {
                        params = JSON.parse(params.toString());
                        params['clientip'] = clientip;
                        const api = require('./api/assetManage/switch_service');
                        const result = await api.switch_service(params);
                        response.recallback(result);
                    } catch (err) {
                        console.error(`switch_service error`, err);
                        logUtils.iotlog.error(`switch_service error`, err);
                    }
                });
            } else {
                response.badRequest();
            }
        } else {
            response.badRequest();
        }
        process.setMaxListeners(0);
        process.on('uncaughtException', function(err) {
            console.log('====uncaughtException', err.stack);
        });
        process.on("unhandledRejection", function(r, e) {});
    }).listen(process.configure.apiport);
};