"use strict";
var path = require('path');
var moment = require('moment');
var redis = require("../db/redisdb");
var logUtils = require(path.join(__dirname, '../', 'util', 'log'));
var utils = require(path.join(__dirname, '../', 'util'));
var RedisKeys = require('../db/REDIS_KEYS');
var bluebird = require('bluebird');
var async = require('async');
var requestHelper = require('request');
var schedule = require('node-schedule');

const lock = require("redis-lock")(client),
    LOCKTIME = 20000,
    LOCK_SYNC_CARDORDER = 'lock_sync_cardOrder_',
    LOCK_CHECK_CARDORDER = 'lock_check_cardOrder_',
    LOCK_CREATE_CARDORDER = 'lock_create_cardOrder_',
    LOCK_NOTIFY_CARDORDER = 'lock_notify_cardOrder_',
    LOCK_CHECK_PACKAGEORDER = 'lock_check_packageOrder_',
    LOCK_CREATE_PACKAGEORDER = 'lock_create_packageOrder_',
    LOCK_NOTIFY_PACKAGEORDER = 'lock_notify_packageOrder_',
    LOCK_SYNC_DELIVERY = 'lock_sync_delivery_';



var api = require('../api');
var models = require(path.join(__dirname, '..', '..', 'model-public', 'sequelize')).init(process.env.NODE_ENV);
var didicardOrder = models['didiCardOrder'],
    didiDelivery = models['didiDelivery'],
    didiPackageOrder = models['didiPackageOrder'];

const HOST_SERVER = utils.gethostname();

module.exports = (function() {
    //缓存选号订单队列进数据库
    cachedCardOrderToDB();
    //缓存专属套餐订单队列进数据库
    cachedPackageOrderToDB();
    //缓存物流信息队列进数据库
    deliveryOrdrerToDB();
    //定时向运营商同步办理中的订单
    syncCardOrder();
    //查询订购成功的物流信息
    syncDeliveryInfo();
})();


function cachedCardOrderToDB() {
    var j = schedule.scheduleJob(`*/3 * * * * *`, function() {
        redis.keysAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDCARDORDER + '*').then(orderkeys => {
            if (orderkeys.length > 0) {
                async.mapLimit(orderkeys, 10, function(orderKey, callback) {
                    redis.getAsync(orderKey).then(order => {
                        if (order) {
                            order = JSON.parse(order);
                            //根据服务器分布式处理
                            if (order['server'] == HOST_SERVER) {
                                if (order['callback_flag'] == 0 || order['callback_flag'] == undefined) {
                                    //初始未回调
                                    if (order['order_status'] == 0) {
                                        //客户下单失败
                                        callback(null, { createfail_key: orderKey, createfail_value: order });
                                    } else if (order['order_status'] == 1) {
                                        //向供应商提交订单
                                        if (!order['created_flag']) {
                                            //已提交过不再继续
                                            callback(null, { cached_key: orderKey, cached_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (order['order_status'] == 4) {
                                        //订购成功-更新状态
                                        //如果已经同步了成功的信息后，不再继续
                                        callback(null, { success_key: orderKey, success_value: order });
                                    } else if (order['order_status'] == 5) {
                                        //订购失败-更新状态
                                        callback(null, { fail_key: orderKey, fail_value: order });
                                    } else if (order['order_status'] == 6) {
                                        //接口失败-改变状态同步数据库
                                        callback(null, { error_key: orderKey, error_value: order });
                                    } else if (order['order_status'] == 7) {
                                        //取消中
                                        //取消下单半小时后的订单
                                        if (moment(order['request_time']) <= moment().subtract(30, 'minutes')) {
                                            callback(null, { incancel_key: orderKey, incancel_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (order['order_status'] == 8) {
                                        //已取消
                                        callback(null, { cancel_key: orderKey, cancel_value: order });
                                    } else if (order['order_status'] == 9) {
                                        //内部异常--自动查询
                                        //取消下单10分钟后的订单
                                        if (moment(order['request_time']) <= moment().subtract(10, 'minutes')) {
                                            callback(null, { exception_key: orderKey, exception_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (order['order_status'] == 10) {
                                        //已下单充值中
                                        if (!order['incharged_flag']) {
                                            callback(null, { incharge_key: orderKey, incharge_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (!order['order_status']) {
                                        //初始订单-批量进数据库，改变修改状态
                                        callback(null, { init_key: orderKey, init_value: order });
                                    }
                                } else if (order['callback_flag'] == 1) {
                                    //回调成功
                                    callback(null, { callback_key: orderKey, callback_value: order });
                                } else if (order['callback_flag'] == 2) {
                                    //回调失败
                                    callback(null, { callback_key: orderKey, callback_value: order });
                                } else {
                                    callback(null, null);
                                }
                            } else {
                                callback(null, null);
                            }
                        } else {
                            callback(null, null);
                        }
                    }).catch(err => {
                        console.error(err);
                        logUtils.didi_polllog.error('didi-cachedCardOrderToDB');
                    });
                }, (err, result) => {
                    if (err) {
                        logUtils.didi_polllog.error('didi-cachedCardOrderToDB err : ', err);
                        return;
                    } else {
                        if (result.length > 0) {
                            //批量缓存
                            const createfailKeys = [],
                                initKeys = [],
                                cachedKeys = [],
                                successkeys = [],
                                failKeys = [],
                                errorkeys = [],
                                exceptionKeys = [],
                                incancelkeys = [],
                                cancelKeys = [],
                                callbackeys = [],
                                inchargeKeys = [],
                                cardOrders = [];
                            result.forEach(result => {
                                if (result) {
                                    if (result['createfail_key']) {
                                        createfailKeys.push(result.createfail_key);
                                        cardOrders.push(result.createfail_value);
                                    } else if (result['init_key']) {
                                        initKeys.push(result.init_key);
                                        cardOrders.push(result.init_value);
                                    } else if (result['cached_key']) {
                                        cachedKeys.push(result.cached_key);
                                        cardOrders.push(result.cached_value);
                                    } else if (result['success_key']) {
                                        successkeys.push(result.success_key);
                                        cardOrders.push(result.success_value);
                                    } else if (result['fail_key']) {
                                        failKeys.push(result.fail_key);
                                        cardOrders.push(result.fail_value);
                                    } else if (result['error_key']) {
                                        errorkeys.push(result.error_key);
                                        cardOrders.push(result.error_value);
                                    } else if (result['exception_key']) {
                                        exceptionKeys.push(result.exception_key);
                                        cardOrders.push(result.exception_value);
                                    } else if (result['incancel_key']) {
                                        incancelkeys.push(result.incancel_key);
                                        cardOrders.push(result.incancel_value);
                                    } else if (result['cancel_key']) {
                                        cancelKeys.push(result.cancel_key);
                                        cardOrders.push(result.cancel_value);
                                    } else if (result.callback_key) {
                                        callbackeys.push(result.callback_key);
                                        cardOrders.push(result.callback_value);
                                    } else if (result.incharge_key) {
                                        inchargeKeys.push(result.incharge_key);
                                        cardOrders.push(result.incharge_value);
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            });
                            if (cardOrders.length > 0) {
                                didicardOrder.bulkCreate(cardOrders, {
                                    ignoreDuplicates: true,
                                    updateOnDuplicate: ['delivery_label', 'order_status', 'provider_order_no', 'msg', 'charge_time', 'callback_flag']
                                }).then(createdata => {
                                    if (initKeys.length > 0) {
                                        //改变状态成已进数据库的初始订单
                                        redis.mgetAsync(initKeys).then(value => {
                                            for (let i = 0, orderSize = initKeys.length; i < orderSize; i++) {
                                                var orderObj = JSON.parse(value[i]);
                                                orderObj['order_status'] = 1;
                                                redis.setAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDCARDORDER + orderObj['customer_order_no'], JSON.stringify(orderObj)).catch(err => {
                                                    console.error(err);
                                                    logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
                                                });
                                            }
                                        }).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
                                        })
                                    }
                                    if (inchargeKeys.length > 0) {
                                        //已下单办理中，等待查询接口自动同步结果
                                        redis.mgetAsync(inchargeKeys).then(value => {
                                            for (let i = 0, orderSize = inchargeKeys.length; i < orderSize; i++) {
                                                var orderObj = JSON.parse(value[i]);
                                                orderObj['incharged_flag'] = true;
                                                redis.setAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDCARDORDER + orderObj['customer_order_no'], JSON.stringify(orderObj)).catch(err => {
                                                    console.error(err);
                                                    logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
                                                });
                                            }
                                        }).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
                                        });
                                    }

                                    if (cachedKeys.length > 0) {
                                        //往上游提交订单
                                        async.eachLimit(cachedKeys, 1, (key, callback) => {
                                            _doCreateCardOrder(key, (err, data) => {
                                                callback(null, true);
                                            })
                                        }, err => {
                                            console.log(' error', err);
                                        });
                                    }

                                    if (incancelkeys.length > 0) {
                                        //取消订单
                                        async.eachLimit(incancelkeys, 1, (key, callback) => {
                                            // _doCancelCardOrder(key, (err, data) => {
                                            //     callback(null, true);
                                            // });
                                        });
                                    }

                                    if (successkeys.length > 0 || failKeys.length > 0 || cancelKeys.length > 0 || errorkeys.length > 0) {
                                        //业务成功/失败/已取消订单/校验失败/接口失败，回调给客户
                                        const notifyCardOrderkeys = successkeys.concat(failKeys, cancelKeys, errorkeys);
                                        async.eachLimit(notifyCardOrderkeys, 1, (key, callback) => {
                                            _doNotifyCardOrder(key, (err, data) => {
                                                callback(null, true);
                                            });
                                        });
                                    }

                                    if (exceptionKeys.length > 0) {
                                        //内部异常订单，调用查询接口
                                        async.eachLimit(exceptionKeys, 1, (key, callback) => {
                                            _doQueryCardOrder(key, (err, data) => {
                                                callback(null, true);
                                            });
                                        });
                                    }
                                    if (callbackeys.length > 0) {
                                        //回调成功订单
                                        redis.delAsync(callbackeys).catch(err => {
                                            logUtils.didilog.info('delAsync error: ', err);
                                        });
                                    }
                                    if (createfailKeys.length > 0) {
                                        //客户下单失败订单
                                        redis.delAsync(createfailKeys).catch(err => {
                                            logUtils.didilog.info('delAsync error: ', err);
                                        });
                                    }
                                }).catch(err => {
                                    console.error(err);
                                    logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
                                });
                            }
                        }
                    }
                });
            }
        }).catch(err => {
            console.error(err);
            logUtils.didi_polllog.error(`didi-cachedCardOrderToDB,error:,${err}`);
        });
    });
};

function cachedPackageOrderToDB() {
    var j = schedule.scheduleJob(`*/${process.configure['syncerInterval']} * * * * *`, function() {
        redis.keysAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDPACKAGEORDER + '*').then(orderkeys => {
            if (orderkeys.length > 0) {
                async.mapLimit(orderkeys, 1000, function(orderKey, callback) {
                    redis.getAsync(orderKey).then(order => {
                        if (order) {
                            order = JSON.parse(order);
                            if (order['server'] == HOST_SERVER) {
                                if (order['callback_flag'] == 0 || order['callback_flag'] == undefined) {
                                    //初始未回调
                                    if (order['order_status'] == 0) {
                                        //客户下单失败
                                        callback(null, { createfail_key: orderKey, createfail_value: order });
                                    } else if (order['order_status'] == 1) {
                                        //已进数据库的初始订单，进入校验机制并向供应商提交订单
                                        if (!order['created_flag']) {
                                            callback(null, { cached_key: orderKey, cached_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (order['order_status'] == 4) {
                                        //订购成功-更新状态
                                        callback(null, { success_key: orderKey, success_value: order });
                                    } else if (order['order_status'] == 5) {
                                        //订购失败-更新状态
                                        callback(null, { fail_key: orderKey, fail_value: order });
                                    } else if (order['order_status'] == 6) {
                                        //接口失败-改变状态同步数据库
                                        callback(null, { error_key: orderKey, error_value: order });
                                    } else if (order['order_status'] == 7) {
                                        //取消中
                                        //取消下单半小时后的订单
                                        if (moment(order['request_time']) <= moment().subtract(30, 'minutes')) {
                                            callback(null, { incancel_key: orderKey, incancel_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (order['order_status'] == 8) {
                                        //已取消
                                        callback(null, { cancel_key: orderKey, cancel_value: order });
                                    } else if (order['order_status'] == 9) {
                                        //内部异常--自动查询
                                        //取消下单10分钟后的订单
                                        if (moment(order['request_time']) <= moment().subtract(10, 'minutes')) {
                                            callback(null, { exception_key: orderKey, exception_value: order });
                                        } else {
                                            callback(null, null);
                                        }
                                    } else if (!order['order_status']) {
                                        //初始订单-批量进数据库，改变修改状态
                                        callback(null, { init_key: orderKey, init_value: order });
                                    }
                                } else if (order['callback_flag'] == 1) {
                                    //回调成功
                                    callback(null, { callback_key: orderKey, callback_value: order });
                                } else if (order['callback_flag'] == 2) {
                                    //回调失败,每分钟重复回调一次，最多3次
                                    callback(null, { callback_key: orderKey, callback_value: order });
                                } else {
                                    callback(null, null);
                                }
                            } else {
                                callback(null, null);
                            }
                        } else {
                            callback(null, null);
                        }
                    }).catch(err => {
                        console.error(err);
                        logUtils.didi_polllog.error('didi-cachedPackageOrderToDB');
                    });
                }, (err, result) => {
                    if (err) {
                        logUtils.didi_polllog.error('didi-cachedPackageOrderToDB err : ', err);
                        return;
                    } else {
                        if (result.length > 0) {
                            //批量缓存
                            const createfailKeys = [],
                                initKeys = [],
                                cachedKeys = [],
                                successkeys = [],
                                failKeys = [],
                                errorkeys = [],
                                exceptionKeys = [],
                                incancelkeys = [],
                                cancelKeys = [],
                                callbackeys = [],
                                packageOrders = [];
                            result.forEach(result => {
                                if (result) {
                                    if (result['createfail_key']) {
                                        createfailKeys.push(result.createfail_key);
                                        packageOrders.push(result.createfail_value);
                                    } else if (result['init_key']) {
                                        initKeys.push(result.init_key);
                                        packageOrders.push(result.init_value);
                                    } else if (result['cached_key']) {
                                        cachedKeys.push(result.cached_key);
                                        packageOrders.push(result.cached_value);
                                    } else if (result['success_key']) {
                                        successkeys.push(result.success_key);
                                        packageOrders.push(result.success_value);
                                    } else if (result['fail_key']) {
                                        failKeys.push(result.fail_key);
                                        packageOrders.push(result.fail_value);
                                    } else if (result['error_key']) {
                                        errorkeys.push(result.error_key);
                                        packageOrders.push(result.error_value);
                                    } else if (result['incancel_key']) {
                                        incancelkeys.push(result.incancel_key);
                                        packageOrders.push(result.incancel_value);
                                    } else if (result['cancel_key']) {
                                        cancelKeys.push(result.cancel_key);
                                        packageOrders.push(result.cancel_value);
                                    } else if (result['exception_key']) {
                                        exceptionKeys.push(result.exception_key);
                                        packageOrders.push(result.exception_value);
                                    } else if (result['callback_key']) {
                                        callbackeys.push(result.callback_key);
                                        packageOrders.push(result.callback_value);
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            });
                            if (packageOrders.length > 0) {
                                didiPackageOrder.bulkCreate(packageOrders, {
                                    ignoreDuplicates: true,
                                    updateOnDuplicate: ['order_status', 'provider_order_no', 'msg', 'charge_time', 'callback_flag']
                                }).then(createdata => {

                                    if (initKeys.length > 0) {
                                        //改变状态成已进数据库的初始订单
                                        redis.mgetAsync(initKeys).then(value => {
                                            for (let i = 0, orderSize = initKeys.length; i < orderSize; i++) {
                                                var orderObj = JSON.parse(value[i]);
                                                orderObj['order_status'] = 1;
                                                redis.setAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDPACKAGEORDER + orderObj['customer_order_no'], JSON.stringify(orderObj)).catch(err => {
                                                    console.error(err);
                                                    logUtils.didi_polllog.error(`didi-cachedPackageOrderToDB,error:,${err}`);
                                                });
                                            }
                                        }).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-cachedPackageOrderToDB,error:,${err}`);
                                        })
                                    }
                                    if (cachedKeys.length > 0) {
                                        //进入校验机制
                                        async.eachLimit(cachedKeys, 1, (key, callback) => {
                                            _doCreatePackageOrder(key, (err, data) => {
                                                callback(null, true);
                                            });
                                        }, err => {
                                            console.log(' error', err);
                                        });
                                    }

                                    if (incancelkeys.length > 0) {
                                        //取消订单
                                        async.eachLimit(incancelkeys, 1, (key, callback) => {
                                            // _doCancelCardOrder(key, (err, data) => {
                                            //     callback(null, true);
                                            // });
                                        });
                                    }
                                    if (successkeys.length > 0 || failKeys.length > 0 || cancelKeys.length > 0 || errorkeys.length > 0) {
                                        //业务成功/失败/已取消订单/校验失败/接口失败，回调给客户
                                        const notifyPackOrderkeys = successkeys.concat(failKeys, cancelKeys, errorkeys);
                                        async.eachLimit(notifyPackOrderkeys, 1, (key, callback) => {
                                            _doNotifyPackageOrder(key, (err, data) => {
                                                callback(null, true);
                                            });
                                        });
                                    }
                                    if (exceptionKeys.length > 0) {
                                        //内部异常订单，调用查询接口
                                        async.eachLimit(exceptionKeys, 1, (key, callback) => {
                                            _doQueryPackageOrder(key, (err, data) => {
                                                callback(null, true);
                                            });
                                        });
                                    }
                                    if (callbackeys.length > 0) {
                                        //回调成功订单
                                        redis.delAsync(callbackeys).catch(err => {
                                            logUtils.didilog.info('delAsync error: ', err);
                                        });
                                    }
                                    if (createfailKeys.length > 0) {
                                        //客户下单失败订单
                                        redis.delAsync(createfailKeys).catch(err => {
                                            logUtils.didilog.info('delAsync error: ', err);
                                        });
                                    }

                                }).catch(err => {
                                    console.error(err);
                                    logUtils.didi_polllog.error(`didi-cachedPackageOrderToDB,error:${err}`);
                                });
                            }
                        }
                    }
                });
            }
        }).catch(err => {
            console.error(err);
            logUtils.didi_polllog.error(`didi-cachedPackageOrderToDB,error:${err}`);
        });
    });
};

function deliveryOrdrerToDB() {
    var j = schedule.scheduleJob(`*/${process.configure['syncerInterval']} * * * * *`, function() {
        redis.keysAsync(RedisKeys.REDIS_KEY_PER_DIDI_DELIVERYORDER + '*').then(orderkeys => {
            if (orderkeys.length > 0) {
                async.mapLimit(orderkeys, 5, function(orderKey, callback) {
                    redis.getAsync(orderKey).then(order => {
                        if (order) {
                            order = JSON.parse(order);
                            if (order['server'] == HOST_SERVER) {
                                callback(null, { key: orderKey, value: order });
                            } else {
                                callback(null, null);
                            }
                        } else {
                            callback(null, null);
                        }
                    }).catch(err => {
                        logUtils.didi_polllog.error('didi-deliveryOrdrerToDB err : ', err);
                    });
                }, (err, result) => {
                    if (err) {
                        logUtils.didi_polllog.error('didi-deliveryOrdrerToDB err : ', err);
                        return;
                    } else {
                        if (result.length > 0) {
                            const keys = [],
                                orders = [];
                            result.forEach(result => {
                                keys.push(result.key);
                                orders.push(result.value);
                            });
                            if (orders.length > 0) {
                                didiDelivery.bulkCreate(orders, {
                                    ignoreDuplicates: true,
                                    updateOnDuplicate: ['delivery_info']
                                }).then(data => {
                                    //改变状态成已进数据库的初始订单
                                    if (keys.length > 0) {
                                        redis.delAsync(keys).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-deliveryOrdrerToDB,error:${err}`);
                                        });
                                    };
                                }).catch(err => {
                                    console.error(err);
                                    logUtils.didi_polllog.error(`didi-deliveryOrdrerToDB,error:${err}`);
                                });
                            }
                        }
                    }
                });
            }
        }).catch(err => {
            console.error(err);
            logUtils.didi_polllog.error(`didi-deliveryOrdrerToDB,error:,${err}`);
        });
    });
};

function syncCardOrder() {
    var j = schedule.scheduleJob('*/' + process.configure['syncCardOrderInterval'] + ' * * * *', function() {
        redis.keysAsync(RedisKeys.REDIS_KEY_PER_DIDI_CACHEDCARDORDER + '*').then(orderkeys => {
            if (orderkeys.length > 0) {
                async.mapLimit(orderkeys, 5, function(orderKey, callback) {
                    redis.getAsync(orderKey).then(order => {
                        if (order) {
                            order = JSON.parse(order);
                            if (order['server'] == HOST_SERVER) {
                                //办理中的订单，且自下单起syncCardOrderStart后才查询
                                if (order['order_status'] == 10 && moment().subtract(process.configure['syncCardOrderStart'], 'minutes') >= moment(order['charge_time'])) {
                                    callback(null, orderKey);
                                } else {
                                    callback(null, null);
                                }
                            } else {
                                callback(null, null);
                            }
                        } else {
                            callback(null, null);
                        }
                    }).catch(err => {
                        console.error(err);
                        logUtils.didi_polllog.error(`didi-deliveryOrdrerToDB,error:,${err}`);
                    });
                }, (err, result) => {
                    if (err) {
                        logUtils.didi_polllog.error('syncCardOrderB err : ', err);
                        return;
                    } else {
                        if (result.length > 0) {
                            async.eachLimit(result, 1, (key, callback) => {
                                _doSyncCardOrder(key, (err, data) => {
                                    callback(null, true);
                                });
                            }, err => {
                                console.log(' error', err);
                            });
                        }
                    }
                });
            }
        }).catch(err => {
            console.error(err);
            logUtils.didi_polllog.error(`didi-deliveryOrdrerToDB,error:,${err}`);
        });
    });
}

function syncDeliveryInfo() {
    var j = schedule.scheduleJob('*/' + process.configure['syncDeliveryInfoInterval'] + ' * * * *', function() {
        redis.keysAsync(RedisKeys.REDIS_KEY_PER_DIDI_SYNCDELIVERY + '*').then(orderkeys => {
            if (orderkeys.length > 0) {
                async.mapLimit(orderkeys, 5, function(orderKey, callback) {
                    redis.getAsync(orderKey).then(order => {
                        if (order) {
                            order = JSON.parse(order);
                            order['delivery_flag'] = true;
                            if (order['server'] == HOST_SERVER) {
                                //物流标签有流转更新
                                if (order['transfer_flag']) {
                                    if (order['delivery_label'] == 5) {
                                        //已签收状态就删除记录
                                        callback(null, { deliveryFinish_key: orderKey, deliveryFinish_value: order });
                                    } else {
                                        //未签收状态继续查询
                                        callback(null, { deliveryIn_key: orderKey, deliveryIn_value: order });
                                    }
                                } else {
                                    callback(null, null);
                                }
                            } else {
                                callback(null, null);
                            }
                        } else {
                            callback(null, null);
                        }
                    }).catch(err => {
                        console.error(err);
                        logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
                    });
                }, (err, result) => {
                    if (err) {
                        logUtils.didi_polllog.error('syncDeliveryInfo err : ', err);
                        return;
                    } else {
                        if (result.length > 0) {
                            const deliveryInKeys = [],
                                deliveryFinishKeys = [],
                                deliveryOrders = [];
                            result.forEach(result => {
                                if (result) {
                                    if (result['deliveryIn_key']) {
                                        deliveryKeys.push(result['deliveryIn_key']);
                                        deliveryOrders.push(result['deliveryIn_value']);
                                    } else if (result['deliveryFinish_key']) {
                                        deliveryFinishKeys.push(result['deliveryFinish_key']);
                                        deliveryOrders.push(result['deliveryFinish_value']);
                                    }
                                } else {
                                    return;
                                }
                            });
                            if (deliveryOrders.length > 0) {
                                didicardOrder.bulkCreate(deliveryOrders, {
                                    ignoreDuplicates: true,
                                    updateOnDuplicate: ['delivery_label']
                                }).then(createdata => {
                                    if (deliveryInKeys.length > 0) {
                                        //未签收查询物流标签
                                        redis.mgetAsync(deliveryInKeys).then(value => {
                                            for (let i = 0, orderSize = deliveryInKeys.length; i < orderSize; i++) {
                                                var orderObj = JSON.parse(value[i]);
                                                delete orderObj['transfer_flag'];
                                                const key = RedisKeys.REDIS_KEY_PER_DIDI_SYNCDELIVERY + orderObj['customer_order_no'];
                                                redis.setAsync(key, JSON.stringify(orderObj)).then(data => {
                                                    _doSyncCardOrder(key, (err, data) => {});
                                                }).catch(err => {
                                                    console.error(err);
                                                    logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
                                                });
                                            }
                                        }).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
                                        });
                                    }
                                    if (deliveryFinishKeys.length > 0) {
                                        //已签收删除记录
                                        redis.delAsync(deliveryFinishKeys).catch(err => {
                                            console.error(err);
                                            logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
                                        });
                                    }
                                }).catch(err => {
                                    console.error(err);
                                    logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
                                });

                            }
                        }
                    }
                });
            }
        }).catch(err => {
            console.error(err);
            logUtils.didi_polllog.error(`didi-syncDeliveryInfo,error:,${err}`);
        });
    });
}


//向运营商同步选号订单
function _doSyncCardOrder(key, callback) {
    const sync_cardOrder = api.sync_cardOrder;
    lock(LOCK_SYNC_CARDORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    sync_cardOrder(order, (err, result) => {
                        callback(null, true);
                        done();
                    });
                } catch (err) {
                    logUtils.didilog.info(`_doSyncCardOrder key:${key},error:${err}`);
                    console.log('_doSyncCardOrder', err);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doSyncCardOrder key:${key},error:${err}`);
            console.log('_doSyncCardOrder', err);
            callback(null, true);
            done();
        });
    });
}
//向运营商查询物流信息
function _doSyncDeliveryInfo() {
    const sync_cardDelivery = api.sync_cardDelivery;
    lock(LOCK_SYNC_DELIVERY + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    sync_cardDelivery(order, (err, result) => {
                        callback(null, true);
                        done();
                    });
                } catch (err) {
                    logUtils.didilog.info(`_doSyncDeliveryInfo key:${key},error:${err}`);
                    console.log('_doSyncDeliveryInfo', err);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doSyncDeliveryInfo key:${key},error:${err}`);
            console.log('_doSyncDeliveryInfo', err);
            callback(null, true);
            done();
        });
    });
}
//校验选号订单并向运营商提交订单
function _docheckCardOrder(key, callback) {
    const checkCardOrder = api.checkCardOrder;
    //校验订单
    lock(LOCK_CHECK_CARDORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    //防止同一笔订单并发校验
                    order['checked_flag'] = true;
                    redis.setAsync(key, JSON.stringify(order)).then(data => {
                        checkCardOrder(order, (err, result) => {
                            if (err) {
                                callback(null, true);
                                done();
                            } else {
                                //校验通过并向运营商提交订单
                                _doCreateCardOrder(key, (err, createResult) => {
                                    callback(null, true);
                                    done();
                                });
                            }
                        });
                    }).catch(err => {
                        logUtils.didilog.info(`_docheckCardOrder key:${key},error:${err}`);
                        console.log('_docheckCardOrder', err);
                        callback(null, true);
                        done();
                    });
                } catch (error) {
                    logUtils.didilog.info(`_docheckCardOrder key:${key},error:${error}`);
                    console.log('_docheckCardOrder', error);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_docheckCardOrder key:${key},error:${err}`);
            console.log('_docheckCardOrder', err);
            callback(null, true);
            done();
        });
    });

};
//校验专属套餐订单，通过后向运营商提交订单
function _docheckPackageOrder(key, callback) {
    const checkPackageOrder = api.checkPackageOrder;
    //校验订单
    lock(LOCK_CHECK_PACKAGEORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    order['checked_flag'] = true;
                    redis.setAsync(key, JSON.stringify(order)).then(data => {
                        checkPackageOrder(order, (err, result) => {
                            if (err) {
                                callback(null, true);
                                done();
                            } else {
                                //校验通过并向运营商提交订单
                                _doCreatePackageOrder(key, (err, createResult) => {
                                    callback(null, true);
                                    done();
                                });
                            }
                        });
                    }).catch(err => {
                        logUtils.didilog.info(`_docheckPackageOrder key:${key},error:${err}`);
                        console.log('_docheckPackageOrder', err);
                        callback(null, true);
                        done();
                    });
                } catch (error) {
                    logUtils.didilog.info(`_docheckPackageOrder key:${key},error:${error}`);
                    console.log('_docheckPackageOrder', error);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_docheckPackageOrder key:${key},error:${err}`);
            console.log('_docheckPackageOrder', err);
            callback(null, true);
            done();
        });
    });
}

function _doCreateCardOrder(key, callback) {
    const create_cardOrder = api.create_cardOrder;
    //往上游提交订单
    lock(LOCK_CREATE_CARDORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    order['created_flag'] = true;
                    redis.setAsync(key, JSON.stringify(order)).then(data => {
                        create_cardOrder(order, (err, orderresult) => {
                            callback(null, true);
                            done();
                        });
                    }).catch(err => {
                        logUtils.didilog.info(`_doCreateCardOrder key:${key},error:${err}`);
                        console.log('_doCreateCardOrder', err);
                        callback(null, true);
                        done();
                    });
                } catch (error) {
                    logUtils.didilog.info(`_doCreateCardOrder key:${key},error:${error}`);
                    console.log('_doCreateCardOrder', error);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doCreateCardOrder key:${key},error:${err}`);
            console.log('_doCreateCardOrder', err);
            callback(null, true);
            done();
        });
    });

}

function _doCreatePackageOrder(key, callback) {
    const create_packageOrder = api.create_packageOrder;
    //往上游提交订单
    lock(LOCK_CREATE_PACKAGEORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    order['created_flag'] = true;
                    redis.setAsync(key, JSON.stringify(order)).then(data => {
                        create_packageOrder(order, (err, orderresult) => {
                            callback(null, true);
                            done();
                        });
                    }).catch(err => {
                        logUtils.didilog.info(`_doCreatePackageOrder key:${key},error:${err}`);
                        console.log('_doCreatePackageOrder', err);
                        callback(null, true);
                        done();
                    });
                } catch (error) {
                    logUtils.didilog.info(`_doCreatePackageOrder key:${key},error:${error}`);
                    console.log('_doCreatePackageOrder', error);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doCreatePackageOrder key:${key},error:${err}`);
            console.log('_doCreatePackageOrder', err);
            callback(null, true);
            done();
        });
    });
}


function _doNotifyCardOrder(key, callback) {
    const notifyToCustomerCardOrder = api.notifyToCustomerCardOrder;
    //回调选号订单
    lock(LOCK_NOTIFY_CARDORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    if (!order['notify_flag']) {
                        //防止重复回调，初始回调
                        order['notify_flag'] = true;
                        redis.setAsync(key, JSON.stringify(order)).then(data => {
                            notifyToCustomerCardOrder(order, (err, cbresult) => {
                                if (err) {
                                    //回调失败
                                    order['callback_flag'] = 2;
                                    //重复回调
                                    _doReCallbackCardOrd(key, (err, doCallResult) => {});
                                } else {
                                    //回调成功
                                    order['callback_flag'] = 1;
                                }
                                redis.setAsync(key, JSON.stringify(order)).then(data => {
                                    callback(null, true);
                                    done();
                                }).catch(err => { logUtils.didilog.info(`_doNotifyCardOrder key:${key},error:${err}`); });
                            });
                        }).catch(err => {
                            logUtils.didilog.info(`_doNotifyCardOrder key:${key},error:${err}`);
                            console.log('_doNotifyCardOrder', err);
                            callback(null, true);
                            done();
                        });
                    } else {
                        callback(null, true);
                        done();
                    }
                } catch (error) {
                    logUtils.didilog.info(`_doNotifyCardOrder key:${key},error:${error}`);
                    console.log('_doNotifyCardOrder', error);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doNotifyCardOrder key:${key},error:${err}`);
            console.log('_doNotifyCardOrder', err);
            callback(null, true);
            done();
        });
    });

}

function _doReCallbackCardOrd(key, callback) {
    const notifyToCustomerCardOrder = api.notifyToCustomerCardOrder;
    let callbackTimes = 0,
        callbackFlag = false;
    redis.getAsync(key).then(order => {
        if (order) {
            order = JSON.parse(order);
            async.whilst(function() {
                return callbackTimes < process.configure['callbackTimes'];
            }, function(cb) {
                if (callbackTimes == 0) {
                    notifyToCustomerCardOrder(order, (err, cbresult) => {
                        if (err) {
                            //回调失败
                            callbackTimes++;
                        } else {
                            //回调成功
                            callbackFlag = true;
                            callbackTimes = process.configure['callbackTimes'];
                        }
                        cb(null, callbackTimes);
                    });
                } else if (callbackTimes > 0) {
                    setTimeout(function() {
                        notifyToCustomerCardOrder(order, function(err, cbresult) {
                            if (cbresult) {
                                callbackTimes = process.configure['callbackTimes'];
                                callbackFlag = true;
                            } else {
                                callbackTimes++;
                            }
                            cb(null, callbackTimes);
                        });
                    }, process.configure['callbackInterval']);
                }
            }, function(err, n) {
                if (!callbackFlag) {
                    //最终回调失败，预警
                }
            });
        }
    }).catch(err => {
        logUtils.didilog.info(`_doReCallbackCardOrd key:${key},error:${err}`);
        console.log('_doReCallbackCardOrd', err);
    });
    callback(null, true);
}

function _doNotifyPackageOrder(key, callback) {
    const notifyToCustomerPackageOrder = api.notifyToCustomerPackageOrder;
    //回调专属套餐订单
    lock(LOCK_NOTIFY_PACKAGEORDER + key, function(done) {
        redis.getAsync(key).then(order => {
            if (order) {
                try {
                    order = JSON.parse(order);
                    if (!order['notify_flag']) {
                        order['notify_flag'] = true;
                        redis.setAsync(key, JSON.stringify(order)).then(data => {
                            notifyToCustomerPackageOrder(order, (err, cbresult) => {
                                if (err) {
                                    //回调失败
                                    order['callback_flag'] = 2;
                                    //重复回调
                                    _doReCallbackPackageOrd(key, (err, doCallResult) => {});
                                } else {
                                    //回调成功
                                    order['callback_flag'] = 1;
                                }
                                redis.setAsync(key, JSON.stringify(order)).then(data => {
                                    callback(null, true);
                                    done();
                                }).catch(err => { logUtils.didilog.info(`_doNotifyPackageOrder key:${key},error:${err}`); });
                            });
                        }).catch(err => {
                            logUtils.didilog.info(`_doNotifyPackageOrder key:${key},error:${err}`);
                            console.log('_doNotifyPackageOrder', err);
                            callback(null, true);
                            done();
                        });
                    } else {
                        callback(null, true);
                        done();
                    }
                } catch (error) {
                    logUtils.didilog.info(`_doNotifyPackageOrder key:${key},error:${error}`);
                    console.log('_doNotifyPackageOrder', error);
                    callback(null, true);
                    done();
                }
            } else {
                callback(null, true);
                done();
            }
        }).catch(err => {
            logUtils.didilog.info(`_doNotifyPackageOrder key:${key},error:${err}`);
            console.log('_doNotifyPackageOrder', err);
            done();
        });
    });
}

function _doReCallbackPackageOrd(key, callback) {
    const notifyToCustomerPackageOrder = api.notifyToCustomerPackageOrder;
    let callbackTimes = 0,
        callbackFlag = false;
    redis.getAsync(key).then(order => {
        if (order) {
            order = JSON.parse(order);
            async.whilst(function() {
                return callbackTimes < process.configure['callbackTimes'];
            }, function(cb) {
                if (callbackTimes == 0) {
                    notifyToCustomerPackageOrder(order, (err, cbresult) => {
                        if (err) {
                            //回调失败
                            callbackTimes++;
                        } else {
                            //回调成功
                            callbackFlag = true;
                            callbackTimes = process.configure['callbackTimes'];
                        }
                        cb(null, callbackTimes);
                    });
                } else if (callbackTimes > 0) {
                    setTimeout(function() {
                        notifyToCustomerPackageOrder(order, function(err, cbresult) {
                            if (cbresult) {
                                callbackTimes = process.configure['callbackTimes'];
                                callbackFlag = true;
                            } else {
                                callbackTimes++;
                            }
                            cb(null, callbackTimes);
                        });
                    }, process.configure['callbackInterval']);
                }
            }, function(err, n) {
                if (!callbackFlag) {
                    //最终回调失败，预警
                }
            });
        }
    }).catch(err => {
        logUtils.didilog.info(`_doReCallbackPackageOrd key:${key},error:${err}`);
        console.log('_doReCallbackPackageOrd', err);
    });
    callback(null, true);
}

function _doCancelCardOrder(key, callback) {
    const cancel_cardOrder = api.cancel_cardOrder;
    //取消订单
    redis.getAsync(key).then(order => {
        if (order) {
            try {
                order = JSON.parse(order);
                cancel_cardOrder(order, (err, orderresult) => {
                    callback(null, true);
                });
            } catch (error) {
                logUtils.didilog.info(`_doCancelCardOrder key:${key},error:${error}`);
                console.log('_doCancelCardOrder', error);
            }
        } else {
            callback(null, true);
        }
    }).catch(err => {
        logUtils.didilog.info(`_doCancelCardOrder key:${key},error:${err}`);
        console.log('_doCancelCardOrder', err);
    });
}

function _doQueryCardOrder(key, callback) {
    const queryCardOrder = api.queryCardOrder;
    //取消订单
    redis.getAsync(key).then(order => {
        if (order) {
            try {
                order = JSON.parse(order);
                queryCardOrder(order, (err, orderresult) => {
                    callback(null, true);
                });
            } catch (error) {
                logUtils.didilog.info(`_doQueryCardOrder key:${key},error:${error}`);
                console.log('_doQueryCardOrder', error);
            }
        } else {
            callback(null, true);
        }
    }).catch(err => {
        logUtils.didilog.info(`_doQueryCardOrder key:${key},error:${err}`);
        console.log('_doQueryCardOrder', err);
    });
}

function _doQueryPackageOrder(key, callback) {
    const queryPackageOrder = api.queryPackageOrder;
    //取消订单
    redis.getAsync(key).then(order => {
        if (order) {
            try {
                order = JSON.parse(order);
                queryPackageOrder(order, (err, orderresult) => {
                    callback(null, true);
                });
            } catch (error) {
                logUtils.didilog.info(`_doQueryPackageOrder key:${key},error:${error}`);
                console.log('_doQueryPackageOrder', error);
            }
        } else {
            callback(null, true);
        }
    }).catch(err => {
        logUtils.didilog.info(`_doQueryPackageOrder key:${key},error:${err}`);
        console.log('_doQueryPackageOrder', err);
    });
}