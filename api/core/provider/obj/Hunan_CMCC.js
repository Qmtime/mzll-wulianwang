const path = require('path'),
    moment = require('moment'),
    utils = require('../../util'),
    logUtils = require('../../util/log'),
    querystring = require('querystring'),
    config = require('../config/' + path.basename(__filename));

var Hunan_CMCC = {
    _buildCommonOptions: function(params) {
        let paramObj = {
            appid: config.appid,
            transid: config.appid + moment().format('YYYYMMDDHHMISS') + parseInt(Math.random() * Math.pow(10, 8)),
        };
        const signstr = paramObj['appid'] + config.httpClientPwd + paramObj['transid'];
        paramObj['token'] = utils.sha256Hex(signstr);
        return paramObj;
    },
    //在线信息实时查询
    _buildRealInfo: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['gprsrealsingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['gprsrealsingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleRealInfo: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleRealInfo body:,${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    return body['result'][0];
                } else {
                    return null;
                }
            } else {
                //失败
                return { code: 1, msg: body['message'] };
            }
        } catch (error) {
            logUtils.wlwlog.info(`Hunan_CMCC  _handleRealInfo error:${error}`);
        }
    },
    //用户状态信息实时查询
    _buildStatus: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['userstatusrealsingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['userstatusrealsingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleStatus: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleStatus body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const statusCode = config.status[body['result'][0]['STATUS']] ? config.status[body['result'][0]['STATUS']]['code'] : '',
                            statusDesc = config.status[body['result'][0]['STATUS']] ? config.status[body['result'][0]['STATUS']]['desc'] : '';
                        return { code: '200', statusCode: statusCode, statusDesc: statusDesc };
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleStatus error:${error}`);
            return { code: '506' };
        }
    },
    //码号信息查询
    _buildCardInfo: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['cardinfo']['ebid'];
        paramObj['card_info'] = params['msisdn'];
        paramObj['type'] = '0';
        const query_url = config['cardinfo'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleCardInfo: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleCardInfo body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    const result = body['result'][0];
                    if (result) {
                        const msisdn = result['MSISDN'],
                            imsi = result['IMSI'],
                            iccid = result['ICCID'];
                        return { msisdn, imsi, iccid };
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleCardInfo error:${error}`);
            return { code: '506' };
        }
    },
    //开关机信息实时查询
    _buildOnAndOff: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['onandoffrealsingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['onandoffrealsingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleOnAndOff: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleOnAndOff,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return config.onstatus[statusbody['result'][0]['status']];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleOnAndOff error:${error}`);
            return { code: '506' };
        }
    },
    //短信服务开通查询
    _buildQryMessgageService: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querysmsopenstatus']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querysmsopenstatus'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleQryMessgageService: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleQryMessgageService,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        if (Number(body['result'][0]['issignsms']) > 0) {
                            return { code: '200', status: '1' };
                        } else {
                            return { code: '200', status: '0' };
                        }
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleQryMessgageService error:${error}`);
            return { code: '506' };
        }
    },
    //GPRS服务开通查询
    _buildQryGprsService: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querygprsopenstatus']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querygprsopenstatus'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleQryGprsService: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleQryGprsService,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        if (Number(config.status[body['result'][0]['issigngprs']]) > 0) {
                            return { code: '200', status: '1' };
                        } else {
                            return { code: '200', status: '0' };
                        }
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleQryGprsService error:${error}`);
            return { code: '506' };
        }
    },
    //APN服务开通查询
    _buildQryApn: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['queryapnopenstatus']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['queryapnopenstatus'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleQryApn: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleQryApn,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    const result = body['result'];
                    return result;
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleQryApn error:${error}`);
            return { code: '506' };
        }
    },
    //生命周期查询
    _buildQrycycle: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querycardlifecycle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querycardlifecycle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleQrycycle: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleQrycycle,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const cycle = config.cycle[body['result'][0]['lifecycle']];
                        const cycleCode = config.cycle[body['result'][0]['lifecycle']] ? config.cycle[body['result'][0]['lifecycle']]['cycleCode'] : '',
                            cycleDesc = config.cycle[body['result'][0]['lifecycle']] ? config.cycle[body['result'][0]['lifecycle']]['cycleDesc'] : '';
                        return { code: '200', cycleCode, cycleDesc };
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleQrycycle error:${error}`);
            return { code: '506' };
        }
    },
    _buildUserOpenService: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['useropenservice']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['useropenservice'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;

    },
    _handleUserOpenService: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleUserOpenService,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const result = body['result'][0];
                        return result;
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleUserOpenService error:${error}`);
            return { code: '506' };
        }
    },
    _buildCardOpenTime: function() {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['useropenservice']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['useropenservice'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleCardOpenTime: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleCardOpenTime,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const result = body['result'][0];
                        return result;
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleCardOpenTime error:${error}`);
            return { code: '506' };
        }
    },
    //集团用户数查询
    _buildRealMemberCount: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['groupuserinfo']['ebid'];
        paramObj['query_date'] = params['time'] || moment().format("YYYYMMDD");
        const query_url = config['groupuserinfo'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleRealMemberCount: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleRealMemberCount,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const total = body['result'][0]['total'];
                        return total;
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleRealMemberCount error:${error}`);
            return { code: '506' };
        }
    },
    //欠费停机用户批量查询
    _buildOweUser: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['arrearageuserinfo']['ebid'];
        paramObj['pageSize'] = params['pageSize'];
        paramObj['pageNum'] = params['pageNum'];
        const query_url = config['arrearageuserinfo'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleOweUser: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleOweUser,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return body['result'][0]['arrearageUserInfo'];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleOweUser error:${error}`);
            return { code: '506' };
        }
    },
    //各生命周期物联卡数量查询
    _buildCycleCardAmount: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querycardcount']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querycardcount'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleCycleCardAmount: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleCycleCardAmount,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    return body['result'][0];
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleCycleCardAmount error:${error}`);
            return { code: '506' };
        }
    },
    //集团异常状态物联卡数据量查询
    _buildExcepCard: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['queryabnormalcardcount']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['queryabnormalcardcount'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleExcepCard: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleExcepCard,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return body['result'][0]['cardUnusualNum '];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleExcepCard error:${error}`);
            return { code: '506' };
        }
    },
    //集团GPRS在线物联卡数量查询
    _buildGprsOnCard: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querygprsonlinecardcount']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querygprsonlinecardcount'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleGprsOnCard: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleGprsCard,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return body['result'][0]['gprstotalnum '];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleGprsOnCard error:${error}`);
            return { code: '506' };
        }
    },
    // 用户余额信息实时查询
    _buildBalance: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['balancerealsingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['balancerealsingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleBalance: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleBalance,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return body['result'][0]['balance'];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleBalance error:${error}`);
            return { code: '506' };
        }
    },
    //套餐内GPRS流量使用情况实时
    _buildRealFlow: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['gprsrealtimeinfo']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['gprsrealtimeinfo'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleRealFlow: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleRealFlow,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const gprs = body['result'][0]['gprs'];
                        //记录实时流量值
                        for(let gpr of gprs){
                            
                        }



                        gprs.forEach(value => {

                        });
                        return { code: '200', gprs };
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleRealFlow error:${error}`);
            return { code: '506' };
        }
    },
    //物联卡单日GPRS使用量查询
    _buildGprsUsedByDate: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['gprsusedinfosinglebydate']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        paramObj['queryDate'] = params['date'] || moment().format("YYYYMMDD");
        const query_url = config['gprsusedinfosinglebydate'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleGprsUsedByDate: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleGprsUsedByDate,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return body['result'][0]['gprs'];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleGprsUsedByDate error:${error}`);
            return { code: '506' };
        }
    },
    //用户当月GPRS使用量查询
    _buildGprsUsedMonth: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['gprsusedinfosingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['gprsusedinfosingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleGprsUsedMonth: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleGprsUsedMonth,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const total_gprs = body['result'][0]['total_gprs'];
                        return total_gprs;
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleGprsUsedMonth error:${error}`);
            return { code: '506' };
        }
    },
    _buildSmsUsedMonth: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['smsusedinfosingle']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['smsusedinfosingle'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleSmsUsedMonth: function(body, params) {
        logUtils.wlwlog.info(`Hunan_CMCC _handleSmsUsedMonth,body:${JSON.stringify(body)}`);
        try {
            body = JSON.parse(body);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const sms = body['result'][0]['sms'];
                        return sms;
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleSmsUsedMonth error:${error}`);
            return { code: '506' };
        }
    },
    //物联卡资费套餐查询
    _buildPackage: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['querycardprodinfo']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['querycardprodinfo'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handlePackage: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handlePackage,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        const prodinfos = body['result'][0]['prodinfos'];
                        return { code: '200', package: prodinfos };
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handlePackage error:${error}`);
            return { code: '506' };
        }
    },
    //物联卡区域位置查询
    _buildLocation: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['location_area']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['location_area'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleLocation: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleLocation,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    return body['result'][0];
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleLocation error:${error}`);
            return { code: '506' };
        }
    },
    _buildSmsStatusReset: function(params) {
        let paramObj = this._buildCommonOptions(params);
        paramObj['ebid'] = config['smsstatusreset']['ebid'];
        paramObj['msisdn'] = params['msisdn'];
        const query_url = config['smsstatusreset'].path;
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                url: url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: querystring.stringify(paramObj)
            };
        return options;
    },
    _handleSmsStatusReset: function(body, params) {
        try {
            body = JSON.parse(body);
            logUtils.wlwlog.info(`Hunan_CMCC _handleSmsStatusReset,body:${JSON.stringify(body)}`);
            if (body['status'] == 0) {
                //0-成功
                if (body['result'] && Array.isArray(body['result'])) {
                    if (body['result'][0]) {
                        return config.smsReset[body['result'][0]['status']];
                    } else {
                        return { code: '505' };
                    }
                } else {
                    return { code: '505' };
                }
            } else {
                return { code: '505' };
            }
        } catch (error) {
            logUtils.wlwlog.error(`Hunan_CMCC _handleSmsStatusReset error:${error}`);
            return { code: '506' };
        }
    }
};
module.exports = Hunan_CMCC;