const path = require('path'),
    moment = require('moment'),
    xml = require('xml'),
    db = require('../../db'),
    RedisKeys = require('../../db/REDIS_KEYS'),
    utils = require('../../util'),
    logUtils = require('../../util/log'),
    iconv = require('iconv-lite'),
    config = require('../config/' + path.basename(__filename));
iconv.skipDecodeWarning = true;


var HUAIAN_CMCC = {
    //构建请求体公共方法
    _buildOperationInOptions: function(process_code, req_type) {
        const operationIn = [
            { process_code: process_code },
            { app_id: config.app_id },
            { access_token: config.access_token },
            { sign: '' },
            { verify_code: '' },
            { req_type: req_type },
            { terminal_id: '' },
            { accept_seq: '' },
            { req_time: moment().format("YYYYMMDDHHmmss") },
            { req_seq: utils.generateRandom(5) + utils.gethostname() },
        ];
        return operationIn;
    },
    //物联网用户基础信息及状态查询接口
    buildMemberInfo: function(params) {
        let operationInArray = this._buildOperationInOptions('cc_qryuserinfo', '01');
        const content = { 'content': [{ ddr_city: config.ddr_city }, { msisdn: params['msisdn'] }] };
        operationInArray = operationInArray.concat(content);
        const xmlobj = [{ operation_in: operationInArray }];
        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

        return {
            url: config.url,
            method: "POST",
            headers: {
                "content-type": "application/xml"
            },
            encoding: null,
            body: body
        };
    },
    handleMemberInfo: async function(options, body, params) {
            try {
                body = iconv.decode(body, 'GBK');
                logUtils.iotlog.info(`query_memberInfo memberInfoOptions:${JSON.stringify(options)},body:${body}`);
                const result = await utils.xml_to_json(body);
                if (result['operation_out']) {
                    const responseBody = result['operation_out']['response'],
                        content = result['operation_out']['content'],
                        msg = responseBody['resp_desc'];
                    if (responseBody['resp_result'] == 0) {
                        //接口成功
                        const msisdn = content['msisdn'],
                            imsi = content['imsi'],
                            iccid = content['iccid'],
                            status = content['status'],
                            username = content['username'],
                            subGroupId = content['subGroupId'],
                            Effdate = moment(content['Effdate']).format("YYYY-MM-DD HH:mm:ss"),
                            Expdate = moment(content['Expdate']).format("YYYY-MM-DD HH:mm:ss");

                        const dbName = 'card',
                            updateAttributes = { msisdn: msisdn, imsi: imsi, iccid: iccid, username: username, lifecycle_status: 4, groupsubid: subGroupId, lifecycle_startTime: Effdate, lifecycle_endTime: Expdate },
                            whereAttributes = { msisdn: msisdn },
                            rediskey = RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn;

                        db.updateDBRecord({ dbName: dbName, updateAttributes: updateAttributes, whereAttributes: whereAttributes, rediskey: rediskey });
                        return { code: '200', result: { msisdn, imsi, iccid, status, username } };
                    } else {
                        //接口异常
                        return { code: '511', result: { msg } };
                    }
                } else {
                    //返回格式错误
                    return { code: '511' };
                }
            } catch (error) {
                console.error(error);
                logUtils.iotlog.info(`query_memberInfo memberInfoOptions:${JSON.stringify(options)},error:${error}`);
                return { code: '511' };
            }
        },
        buildLifeCycle: function(params) {
            let operationInArray = this._buildOperationInOptions('cc_wlw_qrycycle', '01');
            const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
            operationInArray = operationInArray.concat(content);
            const xmlobj = [{ operation_in: operationInArray }];
            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

            let options = {
                url: config.url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/xml',
                },
                encoding: null,
                body: body
            };
            return options;
        },
        handleLifeCycle: async function(options, body, params) {
                const msisdn = params['msisdn'];
                try {
                    body = iconv.decode(body, 'GBK');
                    logUtils.iotlog.info(`handleLifeCycle memberInfoOptions:${JSON.stringify(options)},body:${body}`);
                    let result = await utils.xml_to_json(body);
                    console.log('result', result);
                    if (result['operation_out']) {
                        const responseBody = result['operation_out']['response'],
                            content = result['operation_out']['content'],
                            msg = responseBody['resp_desc'];
                        if (responseBody['resp_result'] == 0) {
                            //接口成功
                            const cycleCode = config.cycle[content['cycle']] ? config.cycle[content['cycle']]['cycleCode'] : '';
                            const dbName = 'card',
                                updateAttributes = { lifecycle_status: cycleCode },
                                whereAttributes = { msisdn: msisdn },
                                rediskey = RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn;

                            db.updateDBRecord({ dbName: dbName, updateAttributes: updateAttributes, whereAttributes: whereAttributes, rediskey: rediskey });
                            return { code: '200', result: cycleCode };
                        } else {
                            //接口异常
                            return { code: '511', result: { msg } };
                        }
                    } else {
                        //返回格式错误
                        return { code: '511' };
                    }
                } catch (error) {
                    console.error(error);
                    logUtils.iotlog.info(`handleLifeCycle memberInfoOptions:${JSON.stringify(options)},error:${error}`);
                    return { code: '511' };
                }
            },
            buildChangeLifiCycle: function(params) {
                let process_code = '';
                switch (params['optType']) {
                    case 1:
                        process_code = 'cc_wlw_suspendtest';
                        break;
                    case 2:
                        process_code = 'cc_wlw_foractive';
                        break;
                    case 3:
                        return;
                        break;
                    default:
                        break;
                }
                let operationInArray = this._buildOperationInOptions(process_code, '02');
                const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
                operationInArray = operationInArray.concat(content);
                const xmlobj = [{ operation_in: operationInArray }];
                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                let options = {
                    url: config.url,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/xml',
                    },
                    encoding: null,
                    body: body
                };
                return options;
            },
            handleChangeLifiCycle: async function(options, body, params) {
                    const msisdn = params['msisdn'];
                    try {
                        body = iconv.decode(body, 'GBK');
                        logUtils.iotlog.info(`handleChangeLifiCycle options:${JSON.stringify(options)},body:${body}`);
                        const result = await utils.xml_to_json(body);
                        console.log('result', result);
                        if (result['operation_out']) {
                            const responseBody = result['operation_out']['response'],
                                content = result['operation_out']['content'],
                                msg = responseBody['resp_desc'];
                            if (responseBody['resp_type'] == 0) {
                                //接口成功，生命周期状态发生变更
                                const orderid = content['orderid'];
                                let lifecycle_status = '';
                                switch (params['optType']) {
                                    case 1:
                                        //1测试期到期处理；2沉默期激活处理；3库存期激活处理
                                        lifecycle_status = 2;
                                        break;
                                    case 2:
                                        lifecycle_status = 4;
                                        break;
                                    case 3:
                                        lifecycle_status = 4;
                                        break;
                                    default:
                                        break;
                                };
                                //更改号卡属性值
                                const dbName = 'card',
                                    updateAttributes = { lifecycle_status: lifecycle_status },
                                    whereAttributes = { msisdn: msisdn },
                                    rediskey = RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn;
                                db.updateDBRecord({ dbName: dbName, updateAttributes: updateAttributes, whereAttributes: whereAttributes, rediskey: rediskey });

                                return { code: '200', result: lifecycle_status };
                            } else {
                                //操作失败
                                return { code: '530' };
                            }
                        } else {
                            //返回格式错误
                            return { code: '511' };
                        }
                    } catch (error) {
                        console.error(error);
                        logUtils.iotlog.info(`handleChangeLifiCycle options:${JSON.stringify(options)},error:${error}`);
                        return { code: '511' };
                    }
                },
                buildSwitchMember: function(params) {

                    let operationInArray = this._buildOperationInOptions('cc_wlw_controlsr', '02');
                    const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }, { oprtype: params['optType'] }, { reason: '6' }] };
                    operationInArray = operationInArray.concat(content);
                    const xmlobj = [{ operation_in: operationInArray }];
                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                    let options = {
                        url: config.url,
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/xml',
                        },
                        encoding: null,
                        body: body
                    };
                    return options;
                },
                handleSwitchMember: async function(options, body, params) {
                        const msisdn = params['msisdn'],
                            switch_staus = params['optType'];
                        try {
                            body = iconv.decode(body, 'GBK');
                            logUtils.iotlog.info(`handleSwitchMember options:${JSON.stringify(options)},body:${body}`);
                            const result = await utils.xml_to_json(body);
                            console.log('result', result);
                            if (result['operation_out']) {
                                const responseBody = result['operation_out']['response'],
                                    content = result['operation_out']['content'],
                                    msg = responseBody['resp_desc'];
                                if (responseBody['resp_type'] == 0) {
                                    //更改号卡属性值
                                    const dbName = 'card',
                                        updateAttributes = { switch_staus: switch_staus },
                                        whereAttributes = { msisdn: msisdn },
                                        rediskey = RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn;
                                    db.updateDBRecord({ dbName: dbName, updateAttributes: updateAttributes, whereAttributes: whereAttributes, rediskey: rediskey });
                                    return { code: '200', result: switch_staus };
                                } else {
                                    //操作失败
                                    return { code: '530' };
                                }
                            } else {
                                //返回格式错误
                                return { code: '511' };
                            }
                        } catch (error) {
                            console.error(error);
                            logUtils.iotlog.info(`handleSwitchMember options:${JSON.stringify(options)},error:${error}`);
                            return { code: '511' };
                        }
                    },
                    buildSwitchService: function(params) {
                        let process_code = '',
                            SUB_SERVICE_STATUS = '';
                        // 22222 位数：语音/短信/GPRS/GPRS-APN1/GPRS-APN2   
                        switch (params['switchType']) {
                            case 1: //1-GPRS;2-短信;3-语音;4-APN
                                process_code = 'cc_userstatuscontrol';
                                if (params['optType'] == 1) {
                                    SUB_SERVICE_STATUS = '22022';
                                } else if (params['optType'] == 2) {
                                    SUB_SERVICE_STATUS = '22122';
                                };
                                break;
                            case 2:
                                process_code = 'cc_userstatuscontrol';
                                if (params['optType'] == 1) {
                                    SUB_SERVICE_STATUS = '20222';
                                } else if (params['optType'] == 2) {
                                    SUB_SERVICE_STATUS = '21222';
                                };
                                break;
                            case 3:
                                process_code = 'cc_userstatuscontrol';
                                if (params['optType'] == 1) {
                                    SUB_SERVICE_STATUS = '02222';
                                } else if (params['optType'] == 2) {
                                    SUB_SERVICE_STATUS = '12222';
                                };
                                break;
                            case 4:
                                break;
                            default:
                                break;
                        };
                        if (process_code && SUB_SERVICE_STATUS) {
                            let operationInArray = this._buildOperationInOptions(process_code, '02');
                            const content = { 'content': [{ ddr_city: config.ddr_city }, { msisdn: params['msisdn'] }, { SUB_SERVICE_STATUS: SUB_SERVICE_STATUS }] };
                            operationInArray = operationInArray.concat(content);
                            const xmlobj = [{ operation_in: operationInArray }];
                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                            let options = {
                                url: config.url,
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/xml',
                                },
                                encoding: null,
                                body: body
                            };
                            return options;
                        } else {
                            return { code: '503' };
                        }
                    },
                    handleSwitchService: async function(options, body, params) {
                            const msisdn = params['msisdn'],
                                switch_staus = params['optType'];
                            try {
                                body = iconv.decode(body, 'GBK');
                                logUtils.iotlog.info(`handleSwitchService options:${JSON.stringify(options)},body:${body}`);
                                const result = await utils.xml_to_json(body);
                                console.log('result', result);
                                if (result['operation_out']) {
                                    const responseBody = result['operation_out']['response'],
                                        content = result['operation_out']['content'],
                                        msg = responseBody['resp_desc'];
                                    if (responseBody['resp_result'] == 0) {
                                        //更改号卡属性值

                                        let switchObj = '';
                                        switch (params['switchType']) {
                                            case 1: //1-GPRS;2-短信;3-语音;4-APN
                                                switchObj = { grps_switch: switch_staus };
                                                break;
                                            case 2:
                                                switchObj = { sms_switch: switch_staus };
                                                break;
                                            case 3:
                                                switchObj = { voice_switch: switch_staus };
                                                break;
                                            case 4:
                                                switchObj = { apn_switch: switch_staus };
                                                break;
                                            default:
                                                break;
                                        };
                                        //更新属性值
                                        const dbName = 'card',
                                            updateAttributes = switchObj,
                                            whereAttributes = { msisdn: msisdn },
                                            rediskey = RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn;
                                        db.updateDBRecord({ dbName: dbName, updateAttributes: updateAttributes, whereAttributes: whereAttributes, rediskey: rediskey });
                                        return { code: '200', result: switch_staus };
                                    } else {
                                        //操作失败
                                        return { code: '530' };
                                    }
                                } else {
                                    //返回格式错误
                                    return { code: '511' };
                                }
                            } catch (error) {
                                console.error(error);
                                logUtils.iotlog.info(`handleSwitchService options:${JSON.stringify(options)},error:${error}`);
                                return { code: '511' };
                            }
                        },

                        //物联网动态流量池实时情况
                        _buildRealPoolGprs: function(params) {
                            let operationInArray = this._buildOperationInOptions('OPEN_QRYINTERNETGRPPOOLGPRS', '01');
                            const content = { 'content': [{ qrytype: '0' }, { eccode: config.ec }, { cycle: moment().format('YYYYMM') }] };
                            operationInArray = operationInArray.concat(content);
                            const xmlobj = [{ operation_in: operationInArray }];
                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                            var options = {
                                url: config.url,
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/xml',
                                },
                                encoding: null,
                                body: body
                            };
                            return options;
                        },
                        _handleRealPoolGprs: async function(body, params) {
                                body = iconv.decode(body, 'GBK');
                                let handlePromise = new Promise((resolve, reject) => {
                                    utils.xml_to_json(body, (err, jsonObj) => {
                                        try {
                                            logUtils.iotlog.info('_handleRealPoolGprs', err, JSON.stringify(jsonObj));
                                            if (err) {
                                                reject(err);
                                            } else {
                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                    const response = jsonObj['operation_out']['response'];
                                                    if (response['resp_type'] == "0") {
                                                        const content = jsonObj['operation_out']['content'],
                                                            resultlist = content['resultlist'];
                                                        if (resultlist) {
                                                            const keys = Object.keys(resultlist);
                                                            const result = keys.map(value => {
                                                                const gprsinfo = value['gprsinfo'];
                                                                console.log('gprsinfo', gprsinfo);
                                                                // gprsinfo = resultlist['gprsinfo'],
                                                                // user_id = gprsinfo['user_id'],
                                                                // max_value = gprsinfo['max_value'],
                                                                // cumulate_value = gprsinfo['cumulate_value'],
                                                                // product_id = gprsinfo['product_id'],
                                                                // product_name = gprsinfo['product_name'];
                                                                resolve(gprsinfo);
                                                            });
                                                        } else {
                                                            resolve({ code: '505' });
                                                        }
                                                    } else {
                                                        //接口错误
                                                        resolve({ code: '505' });
                                                    }
                                                } else {
                                                    //返回格式错误
                                                    resolve({ code: '505' });
                                                }
                                            }
                                        } catch (err) {
                                            logUtils.iotlog.error(`_handleRealPoolGprs,${err}}`);
                                            reject(err);
                                        }
                                    });
                                });
                                return await handlePromise;
                            },
                            // 物联网动态流量池历史情况
                            _buildHisPoolGprs: function(params) {
                                let operationInArray = this._buildOperationInOptions('OPEN_QRYINTERNETGRPPOOLGPRS', '01');
                                const content = { 'content': [{ qrytype: '0' }, { eccode: config.ec }, { cycle: params['time'] }] };
                                operationInArray = operationInArray.concat(content);
                                const xmlobj = [{ operation_in: operationInArray }];
                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                var options = {
                                    url: config.url,
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/xml',
                                    },
                                    encoding: null,
                                    body: body
                                };
                                return options;
                            },
                            _handleHisPoolGprs: async function(body, params) {
                                    body = iconv.decode(body, 'GBK');
                                    let handlePromise = new Promise((resolve, reject) => {
                                        utils.xml_to_json(body, (err, jsonObj) => {
                                            try {
                                                logUtils.iotlog.info('_handleHisPoolGprs', err, JSON.stringify(jsonObj));
                                                if (err) {
                                                    reject(err);
                                                } else {
                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                        const response = jsonObj['operation_out']['response'];
                                                        if (response['resp_type'] == "0") {
                                                            const content = jsonObj['operation_out']['content'],
                                                                resultlist = content['resultlist'];
                                                            if (resultlist) {
                                                                const keys = Object.keys(resultlist);
                                                                const result = keys.map(value => {
                                                                    const gprsinfo = value['gprsinfo'];
                                                                    // gprsinfo = resultlist['gprsinfo'],
                                                                    // user_id = gprsinfo['user_id'],
                                                                    // max_value = gprsinfo['max_value'],
                                                                    // cumulate_value = gprsinfo['cumulate_value'],
                                                                    // product_id = gprsinfo['product_id'],
                                                                    // product_name = gprsinfo['product_name'];
                                                                    resolve(gprsinfo);
                                                                });
                                                            } else {
                                                                resolve({ code: '505' });
                                                            }
                                                        } else {
                                                            //接口错误
                                                            resolve({ code: '505' });
                                                        }
                                                    } else {
                                                        //返回格式错误
                                                        resolve({ code: '505' });
                                                    }
                                                }
                                            } catch (err) {
                                                logUtils.iotlog.error(`_handleHisPoolGprs error:`, err);
                                                reject(err);
                                            }
                                        });
                                    });
                                    return await handlePromise;

                                },
                                //流量池有效成员数量实时查询
                                _buildRealMemberCount: function(params) {
                                    let operationInArray = this._buildOperationInOptions('BOSS_QRYMEMBERCOUNT', '01');
                                    const content = { 'content': [{ groupid: config.groupid }, { groupsubid: config.groupsubid }, { query_day: moment().format("YYYYMMDD") }] };
                                    operationInArray = operationInArray.concat(content);
                                    const xmlobj = [{ operation_in: operationInArray }];
                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                    var options = {
                                        url: config.url,
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/xml',
                                        },
                                        encoding: null,
                                        body: body
                                    };
                                    return options;
                                },
                                _handleRealMemberCount: async function(body, params) {
                                        body = iconv.decode(body, 'GBK');
                                        let handlePromise = new Promise((resolve, reject) => {
                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                try {
                                                    logUtils.iotlog.info('_handleRealMemberCount', err, JSON.stringify(jsonObj));
                                                    if (err) {
                                                        reject(err);
                                                    } else {
                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                            const response = jsonObj['operation_out']['response'];
                                                            if (response['resp_type'] == "0") {
                                                                const content = jsonObj['operation_out']['content'],
                                                                    resultlist = content['resultlist'];
                                                                if (resultlist) {
                                                                    const keys = Object.keys(resultlist);
                                                                    const result = keys.map(value => {
                                                                        const meminfo = resultlist['meminfo'];
                                                                        return meminfo;
                                                                    });
                                                                    resolve(result);
                                                                } else {
                                                                    resolve({ code: '505' });
                                                                }

                                                            } else {
                                                                //接口错误
                                                                resolve({ code: '505' });
                                                            }
                                                        } else {
                                                            //返回格式错误
                                                            resolve({ code: '505' });
                                                        }
                                                    }
                                                } catch (err) {
                                                    logUtils.iotlog.error(`_handleRealMemberCount,error:${err}`);
                                                    reject(err);
                                                }
                                            });
                                        });
                                        return await handlePromise;
                                    },
                                    //流量池有效成员数量历史查询
                                    _buildHisMemberCount: function(params) {
                                        let operationInArray = this._buildOperationInOptions('BOSS_QRYMEMBERCOUNT', '01');
                                        const content = { 'content': [{ groupid: config.groupid }, { groupsubid: config.groupsubid }, { query_day: params['time'] }] };
                                        operationInArray = operationInArray.concat(content);
                                        const xmlobj = [{ operation_in: operationInArray }];
                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                        var options = {
                                            url: config.url,
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/xml',
                                            },
                                            encoding: null,
                                            body: body
                                        };
                                        return options;
                                    },
                                    _handleHisMemberCount: async function(body, params) {
                                            body = iconv.decode(body, 'GBK');
                                            let handlePromise = new Promise((resolve, reject) => {
                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                    try {
                                                        logUtils.iotlog.info('_handleHisMemberCount', err, JSON.stringify(jsonObj));
                                                        if (err) {
                                                            reject(err);
                                                        } else {
                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                const response = jsonObj['operation_out']['response'];
                                                                if (response['resp_type'] == "0") {
                                                                    const content = jsonObj['operation_out']['content'],
                                                                        resultlist = content['resultlist'];
                                                                    if (resultlist) {
                                                                        const keys = Object.keys(resultlist);
                                                                        const result = keys.map(value => {
                                                                            const meminfo = value['meminfo'];
                                                                            return meminfo;
                                                                        });
                                                                        resolve(result);
                                                                    } else {
                                                                        resolve({ code: '505' });
                                                                    }
                                                                } else {
                                                                    //接口错误
                                                                    resolve({ code: '505' });
                                                                }
                                                            } else {
                                                                //返回格式错误
                                                                resolve({ code: '505' });
                                                            }
                                                        }
                                                    } catch (err) {
                                                        logUtils.iotlog.error(`_handleHisMemberCount,error:${err}`);
                                                        reject(err);
                                                    }
                                                });
                                            });
                                            return await handlePromise;
                                        },
                                        //物联网单卡实时流量
                                        _buildRealFlow: function(params) {
                                            let operationInArray = this._buildOperationInOptions('OPEN_QRYINTERNETUSERGPRS', '01');
                                            const content = { 'content': [{ service_number: params['msisdn'] }, { cycle: moment().format("YYYYMM") }] };
                                            operationInArray = operationInArray.concat(content);
                                            const xmlobj = [{ operation_in: operationInArray }];
                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                            var options = {
                                                url: config.url,
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/xml',
                                                },
                                                encoding: null,
                                                body: body
                                            };
                                            return options;
                                        },
                                        _handleRealFlow: async function(body, params) {
                                                body = iconv.decode(body, 'GBK');
                                                let handlePromise = new Promise((resolve, reject) => {
                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                        try {
                                                            logUtils.iotlog.info('_handleRealFlow', err, JSON.stringify(jsonObj));
                                                            if (err) {
                                                                reject(err);
                                                            } else {
                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                    const response = jsonObj['operation_out']['response'];
                                                                    if (response['resp_type'] == "0") {
                                                                        const content = jsonObj['operation_out']['content'],
                                                                            poollist = content['poollist'],
                                                                            prodlist = content['prodlist'],
                                                                            out_gprsflus = content['out_gprsflus'];
                                                                        const poolinfo = poollist['poolinfo'];
                                                                        const prodinfo = prodlist['prodinfo'];
                                                                        let pool_cumulate_value, pool_left_value, prod_cumulate_value, prod_left_value;
                                                                        if (poolinfo) {
                                                                            const pool_cumulate_value = poolinfo['cumulate_value'],
                                                                                pool_left_value = poolinfo['pool_left_value']
                                                                        }
                                                                        if (prodinfo) {
                                                                            const prod_cumulate_value = prodinfo['cumulate_value'],
                                                                                prod_left_value = prodinfo['left_value'];
                                                                        }
                                                                        // resolve(code: '200', gprs: { poolinfo, prodinfo });
                                                                        // console.log('pool_cumulate_value', pool_cumulate_value);
                                                                        // console.log('pool_left_value', pool_left_value);
                                                                        // console.log('prod_cumulate_value', prod_cumulate_value);
                                                                        // console.log('prod_left_value', prod_left_value);
                                                                        // console.log('out_gprsflus', out_gprsflus);
                                                                        // resolve({ pool_cumulate_value: pool_cumulate_value, pool_left_value: pool_left_value, prod_cumulate_value, prod_left_value: prod_left_value, out_gprsflus });
                                                                    } else {
                                                                        //接口错误
                                                                        resolve({ code: '505' });
                                                                    }
                                                                } else {
                                                                    //返回格式错误
                                                                    resolve({ code: '505' });
                                                                }
                                                            }
                                                        } catch (err) {
                                                            logUtils.iotlog.error(`_handleRealFlow,error:${err}`);
                                                            reject(err);
                                                        }
                                                    });
                                                });
                                                return await handlePromise;
                                            },
                                            //物联网单卡历史流量
                                            _buildHisFlow: function(params) {
                                                let operationInArray = this._buildOperationInOptions('OPEN_QRYINTERNETUSERGPRS', '01');
                                                const content = { 'content': [{ service_number: params['msisdn'] }, { cycle: params['time'] }] };
                                                operationInArray = operationInArray.concat(content);
                                                const xmlobj = [{ operation_in: operationInArray }];
                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                                var options = {
                                                    url: config.url,
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/xml',
                                                    },
                                                    encoding: null,
                                                    body: body
                                                };
                                                return options;
                                            },
                                            _handleHisFlow: async function(body, params) {
                                                    body = iconv.decode(body, 'GBK');
                                                    let handlePromise = new Promise((resolve, reject) => {
                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                            try {
                                                                logUtils.iotlog.info('_handleHisFlow', err, JSON.stringify(jsonObj));
                                                                if (err) {
                                                                    reject(err);
                                                                } else {
                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                        const response = jsonObj['operation_out']['response'];
                                                                        if (response['resp_type'] == "0") {
                                                                            const content = jsonObj['operation_out']['content'],
                                                                                poollist = content['poollist'],
                                                                                prodlist = content['prodlist'],
                                                                                out_gprsflus = content['out_gprsflus'];
                                                                            const poolinfo = poollist['poolinfo'];
                                                                            const prodinfo = prodlist['prodinfo'];
                                                                            let pool_cumulate_value, pool_left_value, prod_cumulate_value, prod_left_value;
                                                                            if (poolinfo) {
                                                                                const pool_cumulate_value = poolinfo['cumulate_value'],
                                                                                    pool_left_value = poolinfo['pool_left_value']
                                                                            }
                                                                            if (prodinfo) {
                                                                                const prod_cumulate_value = prodinfo['cumulate_value'],
                                                                                    left_value = prodinfo['left_value'];
                                                                            }
                                                                            resolve({ pool_cumulate_value, pool_left_value, prod_cumulate_value, prod_left_value, out_gprsflus });
                                                                        } else {
                                                                            //接口错误
                                                                            resolve({ code: '505' });
                                                                        }
                                                                    } else {
                                                                        //返回格式错误
                                                                        resolve({ code: '505' });
                                                                    }
                                                                }
                                                            } catch (err) {
                                                                logUtils.iotlog.error(`_handleHisFlow,error:${err}`);
                                                                reject(err);
                                                            }
                                                        });
                                                    });
                                                    return await handlePromise;

                                                },
                                                //物联网语音累积量查询
                                                //暂无权限
                                                _buildVoiceTotal: function(params) {
                                                    let operationInArray = this._buildOperationInOptions('BOSS_wlw_agetfreeitem', '01');
                                                    const content = { 'content': [{ cycle: moment().format("YYYYMM") }, { user_msisdn: params['msisdn'] }, { busi_type: 1 }, { groupid: config.groupid }] };
                                                    operationInArray = operationInArray.concat(content);
                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });

                                                    var options = {
                                                        url: config.url,
                                                        method: 'POST',
                                                        headers: {
                                                            'Content-Type': 'application/xml',
                                                        },
                                                        encoding: null,
                                                        body: body
                                                    };
                                                    return options;
                                                },
                                                _handleVoiceTotal: async function(body, params) {
                                                        body = iconv.decode(body, 'GBK');
                                                        let handlePromise = new Promise((resolve, reject) => {
                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                try {
                                                                    logUtils.iotlog.info('_handleVoiceTotal', err, JSON.stringify(jsonObj));
                                                                    if (err) {
                                                                        reject(err);
                                                                    } else {
                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                            const response = jsonObj['operation_out']['response'];
                                                                            if (response['resp_type'] == "0") {
                                                                                const content = jsonObj['operation_out']['content'],
                                                                                    freeitem_dt = content['freeitem_dt'],
                                                                                    total_value = freeitem_dt['a_freeitem_total_value'],
                                                                                    used_value = freeitem_dt['a_freeitem_value'];
                                                                                resolve({ total_value, used_value });
                                                                            } else {
                                                                                //接口错误
                                                                                resolve({ code: '505' });
                                                                            }
                                                                        } else {
                                                                            //返回格式错误
                                                                            resolve({ code: '505' });
                                                                        }
                                                                    }
                                                                } catch (err) {
                                                                    logUtils.iotlog.error(`_handleVoiceTotal,error:${err}`);
                                                                    reject(err);
                                                                }
                                                            });
                                                        });
                                                        return await handlePromise;
                                                    },
                                                    //物联网短信累积量查询
                                                    //暂无权限
                                                    _buildMessageTotal: function(params) {
                                                        let operationInArray = this._buildOperationInOptions('BOSS_wlw_agetfreeitem', '01');
                                                        const content = { 'content': [{ cycle: moment().format("YYYYMM") }, { user_msisdn: params['msisdn'] }, { busi_type: 2 }, { groupid: '' }] };
                                                        operationInArray = operationInArray.concat(content);
                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                        var options = {
                                                            url: config.url,
                                                            method: 'POST',
                                                            headers: {
                                                                'Content-Type': 'application/xml',
                                                            },
                                                            encoding: null,
                                                            body: body
                                                        };
                                                        return options;
                                                    },
                                                    _handleMessageTotal: async function(body, params) {
                                                            body = iconv.decode(body, 'GBK');
                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                    try {
                                                                        logUtils.iotlog.info('_handleMessageTotal', err, JSON.stringify(jsonObj));
                                                                        if (err) {
                                                                            reject(err);
                                                                        } else {
                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                const response = jsonObj['operation_out']['response'];
                                                                                if (response['resp_type'] == "0") {
                                                                                    const content = jsonObj['operation_out']['content'],
                                                                                        freeitem_dt = content['freeitem_dt'],
                                                                                        total_value = freeitem_dt['a_freeitem_total_value'],
                                                                                        used_value = freeitem_dt['a_freeitem_value'];
                                                                                    resolve({ total_value, used_value });
                                                                                } else {
                                                                                    //接口错误
                                                                                    resolve({ code: '505' });
                                                                                }
                                                                            } else {
                                                                                //返回格式错误
                                                                                resolve({ code: '505' });
                                                                            }
                                                                        }
                                                                    } catch (err) {
                                                                        logUtils.iotlog.error(`_handleMessageTotal,error:${err}`);
                                                                        reject(err);
                                                                    }
                                                                });
                                                            });
                                                            return await handlePromise;

                                                        },
                                                        //用户状态查询
                                                        _buildStatus: function(params) {
                                                            let operationInArray = this._buildOperationInOptions('cc_qryuserinfo', '01');
                                                            const content = { 'content': [{ ddr_city: '12' }, { msisdn: params['msisdn'] }] };
                                                            operationInArray = operationInArray.concat(content);
                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                            var options = {
                                                                url: config.url,
                                                                method: 'POST',
                                                                headers: {
                                                                    'Content-Type': 'application/xml',
                                                                },
                                                                encoding: null,
                                                                body: body
                                                            };
                                                            return options;
                                                        },
                                                        _handleStatus: async function(body, params) {
                                                                body = iconv.decode(body, 'GBK');
                                                                let handlePromise = new Promise((resolve, reject) => {
                                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                                        try {
                                                                            logUtils.iotlog.info('_handleStatus', err, JSON.stringify(jsonObj));
                                                                            if (err) {
                                                                                reject(err);
                                                                            } else {
                                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                    const content = jsonObj['operation_out']['content'];
                                                                                    if (content['ret_code'] == "0") {
                                                                                        const statusCode = config.status[content['status']] ? config.status[content['status']]['code'] : '',
                                                                                            statusDesc = config.status[content['status']] ? config.status[content['status']]['desc'] : '';
                                                                                        resolve({ code: '200', statusCode: statusCode, statusDesc: statusDesc });
                                                                                    } else {
                                                                                        //接口错误
                                                                                        resolve({ code: '505' });
                                                                                    }
                                                                                } else {
                                                                                    //返回格式错误
                                                                                    resolve({ code: '505' });
                                                                                }
                                                                            }
                                                                        } catch (err) {
                                                                            logUtils.iotlog.error(`_handleStatus,error:${err}`);
                                                                            reject(err);
                                                                        }
                                                                    });
                                                                });
                                                                return await handlePromise;
                                                            },
                                                            // 用户基础信息
                                                            _buildCardInfo: function(params) {
                                                                let operationInArray = this._buildOperationInOptions('cc_qryuserinfo', '01');
                                                                const content = { 'content': [{ ddr_city: '12' }, { msisdn: params['msisdn'] }] };
                                                                operationInArray = operationInArray.concat(content);
                                                                const xmlobj = [{ operation_in: operationInArray }];
                                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                var options = {
                                                                    url: config.url,
                                                                    method: 'POST',
                                                                    headers: {
                                                                        'Content-Type': 'application/xml',
                                                                    },
                                                                    encoding: null,
                                                                    body: body
                                                                };
                                                                return options;
                                                            },
                                                            _handleCardInfo: async function(body, params) {
                                                                    body = iconv.decode(body, 'GBK');
                                                                    let handlePromise = new Promise((resolve, reject) => {
                                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                                            try {
                                                                                logUtils.iotlog.info('_handleCardInfo', err, JSON.stringify(jsonObj));
                                                                                if (err) {
                                                                                    reject(err);
                                                                                } else {
                                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                        const content = jsonObj['operation_out']['content'];
                                                                                        if (content['ret_code'] == "0") {
                                                                                            const msisdn = content['msisdn'],
                                                                                                imsi = content['imsi'],
                                                                                                iccid = content['iccid'],
                                                                                                status = config.status[content['status']],
                                                                                                username = content['username'],
                                                                                                brand = content['brand'],
                                                                                                Effdate = content['Effdate'],
                                                                                                Expdate = content['Expdate'];
                                                                                            resolve({ msisdn, imsi, iccid });
                                                                                        } else {
                                                                                            //接口错误
                                                                                            resolve({ code: '505' });
                                                                                        }
                                                                                    } else {
                                                                                        //返回格式错误
                                                                                        resolve({ code: '505' });
                                                                                    }
                                                                                }
                                                                            } catch (err) {
                                                                                logUtils.iotlog.error(`_handleCardInfo,error:${err}`);
                                                                                reject(err);
                                                                            }

                                                                        });
                                                                    });
                                                                    return await handlePromise;
                                                                },

                                                                //生命周期查询接口 
                                                                _buildQrycycle: function(params) {
                                                                    let operationInArray = this._buildOperationInOptions('cc_wlw_qrycycle', '01');
                                                                    const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
                                                                    operationInArray = operationInArray.concat(content);
                                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                    var options = {
                                                                        url: config.url,
                                                                        method: 'POST',
                                                                        headers: {
                                                                            'Content-Type': 'application/xml',
                                                                        },
                                                                        encoding: null,
                                                                        body: body
                                                                    };
                                                                    return options;
                                                                },
                                                                _handleQrycycle: async function(body, params) {
                                                                        body = iconv.decode(body, 'GBK');
                                                                        let handlePromise = new Promise((resolve, reject) => {
                                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                                try {
                                                                                    logUtils.iotlog.info('_handleQrycycle', err, JSON.stringify(jsonObj));
                                                                                    if (err) {
                                                                                        reject(err);
                                                                                    } else {
                                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['content']) {
                                                                                            const content = jsonObj['operation_out']['content'];
                                                                                            if (content['ret_code'] == "0") {
                                                                                                const cycle = config.cycle[content['cycle']];
                                                                                                const cycleCode = config.cycle[content['cycle']] ? config.cycle[content['cycle']]['cycleCode'] : '',
                                                                                                    cycleDesc = config.cycle[content['cycle']] ? config.cycle[content['cycle']]['cycleDesc'] : '';
                                                                                                resolve({ code: '200', cycleCode, cycleDesc });
                                                                                            } else {
                                                                                                //接口错误
                                                                                                resolve({ code: '505' });
                                                                                            }
                                                                                        } else {
                                                                                            //返回格式错误
                                                                                            resolve({ code: '505' });
                                                                                        }
                                                                                    }
                                                                                } catch (err) {
                                                                                    logUtils.iotlog.error(`_handleQrycycle,error:${err}`);
                                                                                    reject(err);
                                                                                }
                                                                            });
                                                                        });
                                                                        return await handlePromise;
                                                                    },
                                                                    //用户状态查询接口
                                                                    _buildQrystatus: function(params) {
                                                                        let operationInArray = this._buildOperationInOptions('cc_wlw_qrystatus', '01');
                                                                        const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
                                                                        operationInArray = operationInArray.concat(content);
                                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                        var options = {
                                                                            url: config.url,
                                                                            method: 'POST',
                                                                            headers: {
                                                                                'Content-Type': 'application/xml',
                                                                            },
                                                                            encoding: null,
                                                                            body: body
                                                                        };
                                                                        return options;
                                                                    },
                                                                    _handleQrystatus: async function(body, params) {
                                                                            body = iconv.decode(body, 'GBK');
                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                    try {
                                                                                        logUtils.iotlog.info('_handleQrystatus', err, JSON.stringify(jsonObj));
                                                                                        if (err) {
                                                                                            reject(err);
                                                                                        } else {
                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                const response = jsonObj['operation_out']['response'];
                                                                                                if (response['resp_type'] == "0") {
                                                                                                    const content = jsonObj['operation_out']['content'],
                                                                                                        ret_code = content['ret_code'],
                                                                                                        ret_desc = content['ret_desc'],
                                                                                                        status = content['status']; //1、正常（1/2）;2、半停（3）;3、全停状态（4/8/9）
                                                                                                    resolve(status);
                                                                                                } else {
                                                                                                    //接口错误
                                                                                                    resolve({ code: '505' });
                                                                                                }
                                                                                            } else {
                                                                                                //返回格式错误
                                                                                                resolve({ code: '505' });
                                                                                            }
                                                                                        }
                                                                                    } catch (err) {
                                                                                        logUtils.iotlog.error(`_handleQrystatus,error:${err}`);
                                                                                        reject(err);
                                                                                    }
                                                                                });
                                                                            });
                                                                            return await handlePromise;
                                                                        },
                                                                        //号码归属集团用户分组查询接口
                                                                        _buildQryuser: function(params) {
                                                                            let operationInArray = this._buildOperationInOptions('cc_wlw_qryuser', '01');
                                                                            const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
                                                                            operationInArray = operationInArray.concat(content);
                                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                            var options = {
                                                                                url: config.url,
                                                                                method: 'POST',
                                                                                headers: {
                                                                                    'Content-Type': 'application/xml',
                                                                                },
                                                                                encoding: null,
                                                                                body: body
                                                                            };
                                                                            return options;
                                                                        },
                                                                        _handleQryuser: async function(body, params) {
                                                                                body = iconv.decode(body, 'GBK');
                                                                                let handlePromise = new Promise((resolve, reject) => {
                                                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                                                        try {
                                                                                            logUtils.iotlog.info('_handleQryuser', err, JSON.stringify(jsonObj));
                                                                                            if (err) {
                                                                                                reject(err);
                                                                                            } else {
                                                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                    const content = jsonObj['operation_out']['content'];
                                                                                                    if (content['ret_code'] == "0") {
                                                                                                        if (content['groupinfo']) {
                                                                                                            const groupinfo = content['groupinfo'];
                                                                                                            resolve(groupinfo);
                                                                                                        } else {
                                                                                                            resolve({ code: '505' });
                                                                                                        }
                                                                                                    } else {
                                                                                                        //接口错误
                                                                                                        resolve({ code: '505' });
                                                                                                    }
                                                                                                } else {
                                                                                                    //返回格式错误
                                                                                                    resolve({ code: '505' });
                                                                                                }
                                                                                            }
                                                                                        } catch (err) {
                                                                                            logUtils.iotlog.error(`_handleQryuser,error:${err}`);
                                                                                            reject(err);
                                                                                        }
                                                                                    });
                                                                                });
                                                                                return await handlePromise;
                                                                            },
                                                                            //受理类执行结果查询 
                                                                            _buildMemorderResult: function(params) {
                                                                                let operationInArray = this._buildOperationInOptions('cc_wlw_memorderresult', '01');
                                                                                const content = { 'content': [{ ddr_city: '12' }, { orderid: params['orderid'] }] };
                                                                                operationInArray = operationInArray.concat(content);
                                                                                const xmlobj = [{ operation_in: operationInArray }];
                                                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                var options = {
                                                                                    url: config.url,
                                                                                    method: 'POST',
                                                                                    headers: {
                                                                                        'Content-Type': 'application/xml',
                                                                                    },
                                                                                    encoding: null,
                                                                                    body: body
                                                                                };
                                                                                return options;
                                                                            },
                                                                            _handleMemorderResult: async function(body, params) {
                                                                                    body = iconv.decode(body, 'GBK');
                                                                                    let handlePromise = new Promise((resolve, reject) => {
                                                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                                                            try {
                                                                                                logUtils.iotlog.info('_handleMemorderResult', err, JSON.stringify(jsonObj));
                                                                                                if (err) {
                                                                                                    reject(err);
                                                                                                } else {
                                                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['content']) {
                                                                                                        const content = jsonObj['operation_out']['content'];
                                                                                                        const result = config.result[content['ret_code']]
                                                                                                        resolve(result);
                                                                                                    } else {
                                                                                                        //返回格式错误
                                                                                                        resolve({ code: '505' });
                                                                                                    }
                                                                                                }
                                                                                            } catch (err) {
                                                                                                logUtils.iotlog.error(`_handleMemorderResult,error:${err}`);
                                                                                                reject(err);
                                                                                            }
                                                                                        });
                                                                                    });
                                                                                    return await handlePromise;
                                                                                },
                                                                                //物联网卡号查询接口
                                                                                _buildQrymsidn: function(params) {
                                                                                    let operationInArray = this._buildOperationInOptions('cc_wlw_qrymsidn', '01');
                                                                                    const content = { 'content': [{ groupid: config.groupid }, { iccid: params['orderid'] }] };
                                                                                    operationInArray = operationInArray.concat(content);
                                                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                    var options = {
                                                                                        url: config.url,
                                                                                        method: 'POST',
                                                                                        headers: {
                                                                                            'Content-Type': 'application/xml',
                                                                                        },
                                                                                        encoding: null,
                                                                                        body: body
                                                                                    };
                                                                                    return options;
                                                                                },
                                                                                _handleQrymsidn: async function(body, params) {
                                                                                        body = iconv.decode(body, 'GBK');
                                                                                        let handlePromise = new Promise((resolve, reject) => {
                                                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                try {
                                                                                                    logUtils.iotlog.info('_handleQrymsidn', err, JSON.stringify(jsonObj));
                                                                                                    if (err) {
                                                                                                        reject(err);
                                                                                                    } else {
                                                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                            const response = jsonObj['operation_out']['response'];
                                                                                                            if (response['resp_type'] == "0") {
                                                                                                                const content = jsonObj['operation_out']['content'],
                                                                                                                    ret_code = content['ret_code'], //0：成功；1：失败;2:处理中
                                                                                                                    ret_desc = content['ret_desc'],
                                                                                                                    msisdn = content['msisdn'];
                                                                                                            } else {
                                                                                                                //接口错误
                                                                                                                resolve({ code: '505' });
                                                                                                            }
                                                                                                        } else {
                                                                                                            //返回格式错误
                                                                                                            resolve({ code: '505' });
                                                                                                        }
                                                                                                    }
                                                                                                } catch (err) {
                                                                                                    console.error('_handleQrymsidn', err);
                                                                                                    logUtils.iotlog.error(`_handleMemorderResult,error:${err}`);
                                                                                                    reject(err);
                                                                                                }
                                                                                            });
                                                                                        });
                                                                                        return await handlePromise;
                                                                                    },
                                                                                    // 查询成员服务状态 上网状态查询
                                                                                    _buildQryGprsService: function(params) {
                                                                                        let operationInArray = this._buildOperationInOptions('cc_wlw_qryservice', '01');
                                                                                        const content = { 'content': [{ ddr_city: '12' }, { groupid: config['groupid'] }, { msisdn: params['msisdn'] }, { service: '3' }] };
                                                                                        operationInArray = operationInArray.concat(content);
                                                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                        var options = {
                                                                                            url: config.url,
                                                                                            method: 'POST',
                                                                                            headers: {
                                                                                                'Content-Type': 'application/xml',
                                                                                            },
                                                                                            encoding: null,
                                                                                            body: body
                                                                                        };
                                                                                        return options;

                                                                                    },
                                                                                    _handleQryGprsService: async function(body, params) {
                                                                                            body = iconv.decode(body, 'GBK');
                                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                    try {
                                                                                                        logUtils.iotlog.info('_handleQryGprsService', err, JSON.stringify(jsonObj));
                                                                                                        if (err) {
                                                                                                            reject(err);
                                                                                                        } else {
                                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                console.log('response')
                                                                                                                const response = jsonObj['operation_out']['response'];
                                                                                                                if (response['resp_type'] == "0") {
                                                                                                                    console.log('resp_type');
                                                                                                                    const servicelist = jsonObj['operation_out']['content']['servicelist'];
                                                                                                                    const serviceKeys = Object.keys(servicelist);
                                                                                                                    let result = [];
                                                                                                                    serviceKeys.forEach(value => {
                                                                                                                        result.push(servicelist[value]);
                                                                                                                    });
                                                                                                                    console.log('result', result);
                                                                                                                    resolve(result);
                                                                                                                } else {
                                                                                                                    //接口错误
                                                                                                                    resolve({ code: '505' });
                                                                                                                }
                                                                                                            } else {
                                                                                                                //返回格式错误
                                                                                                                resolve({ code: '505' });
                                                                                                            }
                                                                                                        }
                                                                                                    } catch (err) {
                                                                                                        logUtils.iotlog.error('_handleQryGprsService', err);
                                                                                                        reject(err);
                                                                                                    }
                                                                                                });
                                                                                            });
                                                                                            return await handlePromise;
                                                                                        },
                                                                                        //查询成员服务状态 短信状态查询
                                                                                        //响应格式有问题，servicelist？？？
                                                                                        _buildQryMessgageService: function(params) {
                                                                                            let operationInArray = this._buildOperationInOptions('cc_wlw_qryservice', '01');
                                                                                            const content = { 'content': [{ ddr_city: '12' }, { groupid: config['groupid'] }, { msisdn: params['msisdn'] }, { service: '2' }] };
                                                                                            operationInArray = operationInArray.concat(content);
                                                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                            var options = {
                                                                                                url: config.url,
                                                                                                method: 'POST',
                                                                                                headers: {
                                                                                                    'Content-Type': 'application/xml',
                                                                                                },
                                                                                                encoding: null,
                                                                                                body: body
                                                                                            };
                                                                                            return options;
                                                                                        },
                                                                                        _handleQryMessgageService: async function(body, params) {
                                                                                                body = iconv.decode(body, 'GBK');
                                                                                                let handlePromise = new Promise((resolve, reject) => {
                                                                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                        try {
                                                                                                            logUtils.iotlog.info('_handleQryMessgageService', err, JSON.stringify(jsonObj));
                                                                                                            if (err) {
                                                                                                                reject(err);
                                                                                                            } else {
                                                                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                    const content = jsonObj['operation_out']['content'];
                                                                                                                    if (content['ret_code'] == "0") {
                                                                                                                        const result = content['ret_code'];
                                                                                                                        resolve(result);
                                                                                                                    } else {
                                                                                                                        //接口错误
                                                                                                                        resolve({ code: '505' });
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    //返回格式错误
                                                                                                                    resolve({ code: '505' });
                                                                                                                }
                                                                                                            }
                                                                                                        } catch (err) {
                                                                                                            logUtils.iotlog.error('_handleQryMessgageService', err);
                                                                                                            reject(err);
                                                                                                        }
                                                                                                    });
                                                                                                });
                                                                                                return await handlePromise;

                                                                                            },
                                                                                            //查询成员服务状态 语音状态查询
                                                                                            _buildQryVoiceService: function(params) {
                                                                                                let operationInArray = this._buildOperationInOptions('cc_wlw_qryservice', '01');
                                                                                                const content = { 'content': [{ ddr_city: '12' }, { groupid: config['groupid'] }, { msisdn: params['msisdn'] }, { service: '1' }] };
                                                                                                operationInArray = operationInArray.concat(content);
                                                                                                const xmlobj = [{ operation_in: operationInArray }];
                                                                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                var options = {
                                                                                                    url: config.url,
                                                                                                    method: 'POST',
                                                                                                    headers: {
                                                                                                        'Content-Type': 'application/xml',
                                                                                                    },
                                                                                                    encoding: null,
                                                                                                    body: body
                                                                                                };
                                                                                                return options;
                                                                                            },
                                                                                            _handleQryVoiceService: async function(body, params) {
                                                                                                    body = iconv.decode(body, 'GBK');
                                                                                                    let handlePromise = new Promise((resolve, reject) => {
                                                                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                            try {
                                                                                                                logUtils.iotlog.info('_handleQryVoiceService', err, JSON.stringify(jsonObj));
                                                                                                                if (err) {
                                                                                                                    reject(err);
                                                                                                                } else {
                                                                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                        const content = jsonObj['operation_out']['content'];
                                                                                                                        if (content['ret_code'] == "0") {
                                                                                                                            const result = content['ret_code'];
                                                                                                                            resolve(result);
                                                                                                                        } else {
                                                                                                                            //接口错误
                                                                                                                            resolve({ code: '505' });
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        //返回格式错误
                                                                                                                        resolve({ code: '505' });
                                                                                                                    }
                                                                                                                }
                                                                                                            } catch (error) {
                                                                                                                logUtils.iotlog.error('_handleQryVoiceService', error);
                                                                                                                reject(error);
                                                                                                            }
                                                                                                        });
                                                                                                    });
                                                                                                    return await handlePromise;
                                                                                                },
                                                                                                _buildPackage: function(params) {

                                                                                                },
                                                                                                //查询 物联网集团成员终端标识获取接口
                                                                                                _buildGetIdentify: function(params) {
                                                                                                    let operationInArray = this._buildOperationInOptions('cc_wlw_getidens', '01');
                                                                                                    const content = { 'content': [{ groupid: config.groupid }, { msisdn: params['msisdn'] }, { tGroupId: '0' }] };
                                                                                                    operationInArray = operationInArray.concat(content);
                                                                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                    var options = {
                                                                                                        url: config.url,
                                                                                                        method: 'POST',
                                                                                                        headers: {
                                                                                                            'Content-Type': 'application/xml',
                                                                                                        },
                                                                                                        encoding: null,
                                                                                                        body: body
                                                                                                    };
                                                                                                    return options;

                                                                                                },
                                                                                                _handleGetIdentify: async function(body, params) {
                                                                                                        body = iconv.decode(body, 'GBK');
                                                                                                        let handlePromise = new Promise((resolve, reject) => {
                                                                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                try {
                                                                                                                    logUtils.iotlog.info('_handleGetIdentify', err, JSON.stringify(jsonObj));
                                                                                                                    if (err) {
                                                                                                                        reject(err);
                                                                                                                    } else {
                                                                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                            const response = jsonObj['operation_out']['response'];
                                                                                                                            if (response['resp_type'] == "0") {
                                                                                                                                const content = jsonObj['operation_out']['content'],
                                                                                                                                    msisdn = content['msisdn'],
                                                                                                                                    terminalGroup = content['terminalGroup'], //0：成功；1：失败;2:处理中
                                                                                                                                    ret_desc = content['ret_desc'],
                                                                                                                                    tGroupId = content['tGroupId'],
                                                                                                                                    marktype = content['marktype'],
                                                                                                                                    markvalue = content['markvalue'];
                                                                                                                            } else {
                                                                                                                                //接口错误
                                                                                                                                resolve({ code: '505' });
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            //返回格式错误
                                                                                                                            resolve({ code: '505' });
                                                                                                                        }
                                                                                                                    }
                                                                                                                } catch (err) {
                                                                                                                    logUtils.iotlog.error('_handleGetIdentify', err);
                                                                                                                    reject(err);
                                                                                                                }
                                                                                                            });
                                                                                                        });
                                                                                                        return await handlePromise;
                                                                                                    },
                                                                                                    // 物联网集团成员APN列表查询接口
                                                                                                    // 暂无权限
                                                                                                    _buildQryApn: function(params) {
                                                                                                        let operationInArray = this._buildOperationInOptions('cc_wlw_qryapn', '01');
                                                                                                        const content = { 'content': [{ msisdn: params['msisdn'] }] };
                                                                                                        operationInArray = operationInArray.concat(content);
                                                                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                        var options = {
                                                                                                            url: config.url,
                                                                                                            method: 'POST',
                                                                                                            headers: {
                                                                                                                'Content-Type': 'application/xml',
                                                                                                            },
                                                                                                            encoding: null,
                                                                                                            body: body
                                                                                                        };
                                                                                                        return options;
                                                                                                    },
                                                                                                    _handleQryApn: async function(body, params) {
                                                                                                            body = iconv.decode(body, 'GBK');
                                                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                    try {
                                                                                                                        logUtils.iotlog.info('_handleQryApn', err, JSON.stringify(jsonObj));
                                                                                                                        if (err) {
                                                                                                                            reject(err);
                                                                                                                        } else {
                                                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                const response = jsonObj['operation_out']['response'];
                                                                                                                                if (response['resp_type'] == "0") {
                                                                                                                                    const content = jsonObj['operation_out']['content'],
                                                                                                                                        ret_code = content['ret_code'], //0：成功；其他：失败
                                                                                                                                        ret_desc = content['ret_desc'],
                                                                                                                                        groupid = content['groupid'],
                                                                                                                                        apnlist = content['apnlist'];
                                                                                                                                } else {
                                                                                                                                    //接口错误
                                                                                                                                    resolve({ code: '505' });
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                //返回格式错误
                                                                                                                                resolve({ code: '505' });
                                                                                                                            }
                                                                                                                        }
                                                                                                                    } catch (err) {
                                                                                                                        logUtils.iotlog.error('_handleQryApn', err);
                                                                                                                        reject(err);
                                                                                                                    }

                                                                                                                });
                                                                                                            });
                                                                                                            return await handlePromise;
                                                                                                        },
                                                                                                        //暂无权限
                                                                                                        _buildQryIden: function(params) {
                                                                                                            let operationInArray = this._buildOperationInOptions('cc_wlw_getidens', '01');
                                                                                                            const content = { 'content': [{ tGroupId: params['tGroupId'] }] };
                                                                                                            operationInArray = operationInArray.concat(content);
                                                                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                            var options = {
                                                                                                                url: config.url,
                                                                                                                method: 'POST',
                                                                                                                headers: {
                                                                                                                    'Content-Type': 'application/xml',
                                                                                                                },
                                                                                                                encoding: null,
                                                                                                                body: body
                                                                                                            };
                                                                                                            return options;
                                                                                                        },
                                                                                                        _handleQryIden: async function(body, params) {
                                                                                                                body = iconv.decode(body, 'GBK');
                                                                                                                let handlePromise = new Promise((resolve, reject) => {
                                                                                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                        try {
                                                                                                                            logUtils.iotlog.info('_handleQryIden', err, JSON.stringify(jsonObj));
                                                                                                                            if (err) {
                                                                                                                                reject(err);
                                                                                                                            } else {
                                                                                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                    const response = jsonObj['operation_out']['response'];
                                                                                                                                    if (response['resp_type'] == "0") {
                                                                                                                                        const content = jsonObj['operation_out']['content'],
                                                                                                                                            terminalGroup = content['terminalGroup'];
                                                                                                                                        resolve(terminalGroup);
                                                                                                                                    } else {
                                                                                                                                        //接口错误
                                                                                                                                        resolve({ code: '505' });
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    //返回格式错误
                                                                                                                                    resolve({ code: '505' });
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } catch (err) {
                                                                                                                            logUtils.iotlog.error('_handleQryIden', err);
                                                                                                                            reject(err);
                                                                                                                        }
                                                                                                                    });
                                                                                                                });
                                                                                                                return await handlePromise;
                                                                                                            },
                                                                                                            _buildQryOffReason: function(params) {
                                                                                                                let operationInArray = this._buildOperationInOptions('cc_wlw_qrySusReason', '01');
                                                                                                                const content = { 'content': [{ msisdn: params['msisdn'] }] };
                                                                                                                operationInArray = operationInArray.concat(content);

                                                                                                                const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                var options = {
                                                                                                                    url: config.url,
                                                                                                                    method: 'POST',
                                                                                                                    headers: {
                                                                                                                        'Content-Type': 'application/xml',
                                                                                                                    },
                                                                                                                    encoding: null,
                                                                                                                    body: body
                                                                                                                };
                                                                                                                return options;
                                                                                                            },
                                                                                                            _handleQryOffReason: async function(body, params) {
                                                                                                                    body = iconv.decode(body, 'GBK');
                                                                                                                    let handlePromise = new Promise((resolve, reject) => {
                                                                                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                            try {
                                                                                                                                logUtils.iotlog.info('_handleQryOffReason', err, JSON.stringify(jsonObj));
                                                                                                                                if (err) {
                                                                                                                                    reject(err);
                                                                                                                                } else {
                                                                                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                        const content = jsonObj['operation_out']['content'];
                                                                                                                                        if (content['ret_code'] == "0") {
                                                                                                                                            const result = content['ret_code'];
                                                                                                                                            resolve(result);
                                                                                                                                        } else {
                                                                                                                                            //接口错误
                                                                                                                                            resolve({ code: '505' });
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        //返回格式错误
                                                                                                                                        resolve({ code: '505' });
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            } catch (err) {
                                                                                                                                logUtils.iotlog.error('_handleQryOffReason', err);
                                                                                                                                reject(err);
                                                                                                                            }
                                                                                                                        });
                                                                                                                    });
                                                                                                                    return await handlePromise;
                                                                                                                },
                                                                                                                //暂无权限
                                                                                                                _buildPackage: function(params) {
                                                                                                                    let operationInArray = this._buildOperationInOptions('cc_wlw_qrygroupuserpackageinfo', '01');
                                                                                                                    const content = { 'content': [{ groupid: config['groupid'] }, { msisdn: params['msisdn'] }, { biz_pkg_qry_scope: params['type'] }] };
                                                                                                                    operationInArray = operationInArray.concat(content);

                                                                                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                    var options = {
                                                                                                                        url: config.url,
                                                                                                                        method: 'POST',
                                                                                                                        headers: {
                                                                                                                            'Content-Type': 'application/xml',
                                                                                                                        },
                                                                                                                        encoding: null,
                                                                                                                        body: body
                                                                                                                    };
                                                                                                                    return options;
                                                                                                                },
                                                                                                                _handlePackage: async function(body, params) {
                                                                                                                        body = iconv.decode(body, 'GBK');
                                                                                                                        let handlePromise = new Promise((resolve, reject) => {
                                                                                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                try {
                                                                                                                                    logUtils.iotlog.info('_handlePackage', err, JSON.stringify(jsonObj));
                                                                                                                                    if (err) {
                                                                                                                                        reject(err);
                                                                                                                                    } else {
                                                                                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                            const content = jsonObj['operation_out']['content'];
                                                                                                                                            if (content['ret_code'] == "0") {
                                                                                                                                                const result = content['ret_code'];
                                                                                                                                                resolve(result);
                                                                                                                                            } else {
                                                                                                                                                //接口错误
                                                                                                                                                resolve({ code: '505' });
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            //返回格式错误
                                                                                                                                            resolve({ code: '505' });
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                } catch (err) {
                                                                                                                                    logUtils.iotlog.error('_handlePackage', err);
                                                                                                                                    reject(err);
                                                                                                                                }
                                                                                                                            });
                                                                                                                        });
                                                                                                                        return await handlePromise;
                                                                                                                    },
                                                                                                                    //————————————————————-——-------受理类操作————————————————————————————
                                                                                                                    //物联网集团成员套餐订购
                                                                                                                    _buildMemberOrder: function(params) {
                                                                                                                        let operationInArray = this._buildOperationInOptions('cc_wlw_memberorder', '02');
                                                                                                                        const prodAttrInfo = [{ attrID: params['attrID'] }, { attrValue: params['attrValue'] }, { oprType: params['oprtype'] }];
                                                                                                                        const memInfo = [{
                                                                                                                                oprTime: moment().format("YYYYMMDDHHmmss")
                                                                                                                            },
                                                                                                                            { oprCode: params['oprtype'] },
                                                                                                                            { msisdn: params['msisdn'] },
                                                                                                                            {
                                                                                                                                prodInfo: [{ prodID: params['prodID'] }, { pkgProdID: params['pkgProdID'] }, { prodInstEffTime: moment().format("YYYYMMDDHHmmss") }, { prodInstExpTime: moment().add(params['time'], 'month').format("YYYYMMDDHHmmss") }, { oprType: params['oprtype'] }]
                                                                                                                            }
                                                                                                                        ];
                                                                                                                        const content = { 'content': [{ ddr_city: '12' }, { grpsubsid: config.groupsubid }, { memInfo: memInfo }] };
                                                                                                                        operationInArray = operationInArray.concat(content);
                                                                                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                        var options = {
                                                                                                                            url: config.url,
                                                                                                                            method: 'POST',
                                                                                                                            headers: {
                                                                                                                                'Content-Type': 'application/xml'
                                                                                                                            },
                                                                                                                            encoding: null,
                                                                                                                            body: body
                                                                                                                        };
                                                                                                                        return options;
                                                                                                                    },
                                                                                                                    _handleMemberOrder: async function(body, params) {
                                                                                                                            body = iconv.decode(body, 'GBK');
                                                                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                    try {
                                                                                                                                        logUtils.iotlog.info('_handleMemberOrder', err, JSON.stringify(jsonObj));
                                                                                                                                        if (err) {
                                                                                                                                            reject(err);
                                                                                                                                        } else {
                                                                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                                const response = jsonObj['operation_out']['response'];
                                                                                                                                                if (response['resp_type'] == "0") {
                                                                                                                                                    const content = jsonObj['operation_out']['content'],
                                                                                                                                                        ret_code = content['ret_code'], //0 成功 1 失败 2 处理中
                                                                                                                                                        ret_msg = content['ret_msg'];
                                                                                                                                                } else {
                                                                                                                                                    //接口错误
                                                                                                                                                    resolve({ code: '505' });
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                //返回格式错误
                                                                                                                                                resolve({ code: '505' });
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    } catch (err) {
                                                                                                                                        logUtils.iotlog.error('_handleMessageTotal', err);
                                                                                                                                        reject(err);
                                                                                                                                    }
                                                                                                                                });
                                                                                                                            });
                                                                                                                            return await handlePromise;
                                                                                                                        },
                                                                                                                        //物联网集团成员停复机接口
                                                                                                                        _buildControlsr: function(params) {
                                                                                                                            let operationInArray = this._buildOperationInOptions('cc_wlw_controlsr', '02');
                                                                                                                            const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }, { oprtype: params['oprtype'] }, { reason: params['reason'] }] };
                                                                                                                            operationInArray = operationInArray.concat(content);
                                                                                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                            var options = {
                                                                                                                                url: config.url,
                                                                                                                                method: 'POST',
                                                                                                                                headers: {
                                                                                                                                    'Content-Type': 'application/xml',
                                                                                                                                },
                                                                                                                                encoding: null,
                                                                                                                                body: body
                                                                                                                            };
                                                                                                                            return options;
                                                                                                                        },
                                                                                                                        _handleControlsr: async function(body, params) {
                                                                                                                                body = iconv.decode(body, 'GBK');
                                                                                                                                let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                    utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                        try {
                                                                                                                                            logUtils.iotlog.info('_handleControlsr', err, JSON.stringify(jsonObj));
                                                                                                                                            if (err) {
                                                                                                                                                reject(err);
                                                                                                                                            } else {
                                                                                                                                                if (jsonObj['operation_out'] && jsonObj['operation_out']['content']) {

                                                                                                                                                    const content = jsonObj['operation_out']['content'];
                                                                                                                                                    if (content['ret_code'] == '0') { //0：成功；1：失败
                                                                                                                                                        const orderid = content['orderid'];
                                                                                                                                                        resolve(orderid);
                                                                                                                                                    } else {
                                                                                                                                                        resolve({ code: '505' });
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    //返回格式错误
                                                                                                                                                    resolve({ code: '505' });
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        } catch (err) {
                                                                                                                                            logUtils.iotlog.error('_handleMessageTotal', err);
                                                                                                                                            reject(err);
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                });
                                                                                                                                return await handlePromise;
                                                                                                                            },
                                                                                                                            //测试期强制结束接口
                                                                                                                            _buildSuspendTest: function(params) {
                                                                                                                                let operationInArray = this._buildOperationInOptions('cc_wlw_suspendtest', '02');
                                                                                                                                const content = { 'content': [{ ddr_city: '12' }, { orderid: params['orderid'] }] };
                                                                                                                                operationInArray = operationInArray.concat(content);
                                                                                                                                const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                                const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                                var options = {
                                                                                                                                    url: config.url,
                                                                                                                                    method: 'POST',
                                                                                                                                    headers: {
                                                                                                                                        'Content-Type': 'application/xml',
                                                                                                                                    },
                                                                                                                                    encoding: null,
                                                                                                                                    body: body
                                                                                                                                };
                                                                                                                                return options;
                                                                                                                            },
                                                                                                                            _handleSuspendTest: async function(body, params) {
                                                                                                                                    body = iconv.decode(body, 'GBK');
                                                                                                                                    let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                        utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                            try {
                                                                                                                                                logUtils.iotlog.info('_handleSuspendTest', err, JSON.stringify(jsonObj));
                                                                                                                                                if (err) {
                                                                                                                                                    reject(err);
                                                                                                                                                } else {
                                                                                                                                                    if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                                        const response = jsonObj['operation_out']['response'];
                                                                                                                                                        if (response['resp_type'] == "0") {
                                                                                                                                                            const content = jsonObj['operation_out']['content'],
                                                                                                                                                                ret_code = content['ret_code'], //0：成功；1：失败
                                                                                                                                                                ret_msg = content['ret_msg'];
                                                                                                                                                        } else {
                                                                                                                                                            //接口错误
                                                                                                                                                            resolve({ code: '505' });
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        //返回格式错误
                                                                                                                                                        resolve({ code: '505' });
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } catch (err) {
                                                                                                                                                logUtils.iotlog.error('_handleMessageTotal', err);
                                                                                                                                                reject(err);
                                                                                                                                            }
                                                                                                                                        });
                                                                                                                                    });
                                                                                                                                    return await handlePromise;
                                                                                                                                },
                                                                                                                                //沉默期强制激活接口
                                                                                                                                _buildForactive: function(params) {
                                                                                                                                    let operationInArray = this._buildOperationInOptions('cc_wlw_foractive', '02');
                                                                                                                                    const content = { 'content': [{ groupid: config.groupid }, { telnum: params['msisdn'] }] };
                                                                                                                                    operationInArray = operationInArray.concat(content);
                                                                                                                                    const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                                    const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                                    var options = {
                                                                                                                                        url: config.url,
                                                                                                                                        method: 'POST',
                                                                                                                                        headers: {
                                                                                                                                            'Content-Type': 'application/xml',
                                                                                                                                        },
                                                                                                                                        encoding: null,
                                                                                                                                        body: body
                                                                                                                                    };
                                                                                                                                    return options;
                                                                                                                                },
                                                                                                                                _handleForactive: async function(body, params) {
                                                                                                                                        body = iconv.decode(body, 'GBK');
                                                                                                                                        let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                            utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                                try {
                                                                                                                                                    logUtils.iotlog.info('_handleForactive', err, JSON.stringify(jsonObj));
                                                                                                                                                    if (err) {
                                                                                                                                                        reject(err);
                                                                                                                                                    } else {
                                                                                                                                                        if (jsonObj['operation_out'] && jsonObj['operation_out']['content']) {
                                                                                                                                                            const content = jsonObj['operation_out']['content'];
                                                                                                                                                            if (content['ret_code'] == "0") {
                                                                                                                                                                resolve(content['ret_code']);
                                                                                                                                                            } else {
                                                                                                                                                                //接口错误
                                                                                                                                                                resolve({ code: '505' });
                                                                                                                                                            }
                                                                                                                                                        } else {
                                                                                                                                                            //返回格式错误
                                                                                                                                                            resolve({ code: '505' });
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } catch (err) {
                                                                                                                                                    logUtils.iotlog.error('_handleForactive', err);
                                                                                                                                                    reject(err);
                                                                                                                                                }
                                                                                                                                            });
                                                                                                                                        });
                                                                                                                                        return await handlePromise;
                                                                                                                                    },
                                                                                                                                    //APN 通道暂停恢复接口
                                                                                                                                    //暂无权限
                                                                                                                                    _buildApnStatus: function(params) {
                                                                                                                                        let operationInArray = this._buildOperationInOptions('cc_wlw_chgsevstatus', '02');
                                                                                                                                        const content = { 'content': [{ groupid: config.groupid }, { msisdn: params['msisdn'] }, { prodid: params['prodid'] }, { oprtype: params['oprtype'] }] };
                                                                                                                                        operationInArray = operationInArray.concat(content);
                                                                                                                                        const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                                        const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                                        var options = {
                                                                                                                                            url: config.url,
                                                                                                                                            method: 'POST',
                                                                                                                                            headers: {
                                                                                                                                                'Content-Type': 'application/xml',
                                                                                                                                            },
                                                                                                                                            encoding: null,
                                                                                                                                            body: body
                                                                                                                                        };
                                                                                                                                        return options;
                                                                                                                                    },
                                                                                                                                    _handleApnStatus: async function(body, params) {
                                                                                                                                            body = iconv.decode(body, 'GBK');
                                                                                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                                    try {
                                                                                                                                                        logUtils.iotlog.info('_handleForactive', err, JSON.stringify(jsonObj));
                                                                                                                                                        if (err) {
                                                                                                                                                            reject(err);
                                                                                                                                                        } else {
                                                                                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['content']) {
                                                                                                                                                                const content = jsonObj['operation_out']['content'];
                                                                                                                                                                if (content['ret_code'] == "0") {
                                                                                                                                                                    const result = content['ret_code'];
                                                                                                                                                                    resolve(result);
                                                                                                                                                                } else {
                                                                                                                                                                    //接口错误
                                                                                                                                                                    resolve({ code: '505' });
                                                                                                                                                                }
                                                                                                                                                            } else {
                                                                                                                                                                //返回格式错误
                                                                                                                                                                resolve({ code: '505' });
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    } catch (err) {
                                                                                                                                                        logUtils.iotlog.error('_handleForactive', err);
                                                                                                                                                        reject(err);
                                                                                                                                                    }
                                                                                                                                                });
                                                                                                                                            });
                                                                                                                                            return await handlePromise;
                                                                                                                                        },

                                                                                                                                        //物联网客户单卡预警阀值设置接口
                                                                                                                                        _buildCardAlarmSetting: function(params) {
                                                                                                                                            let operationInArray = this._buildOperationInOptions('iot_wlw_custcardalarmset', '02');
                                                                                                                                            const content = { 'content': [{ cfg_name: '' }, { groupid: config.groupid }, { groupsubsid: config.groupsubsid }, { telnum: params['msisdn'] }, { apn: params['apn'] }, { attr_value: '90' }, { attr_type: '1' }, { alarm_type: params['type'] }, { push_type: '3' }, { operator_type: params['oprtype'] }, { cfg_id: params['cfg_id'] }, { sms_detail: '' }, { email_detail: '' }, { send_time_constr: '' }] };
                                                                                                                                            operationInArray = operationInArray.concat(content);
                                                                                                                                            const xmlobj = [{ operation_in: operationInArray }];
                                                                                                                                            const body = xml(xmlobj, { declaration: { encoding: 'GBK' } });
                                                                                                                                            var options = {
                                                                                                                                                url: config.url,
                                                                                                                                                method: 'POST',
                                                                                                                                                headers: {
                                                                                                                                                    'Content-Type': 'application/xml',
                                                                                                                                                },
                                                                                                                                                encoding: null,
                                                                                                                                                body: body
                                                                                                                                            };
                                                                                                                                            return options;

                                                                                                                                        },
                                                                                                                                        _handleCardAlarmSetting: async function(body, params) {
                                                                                                                                            body = iconv.decode(body, 'GBK');
                                                                                                                                            let handlePromise = new Promise((resolve, reject) => {
                                                                                                                                                utils.xml_to_json(body, (err, jsonObj) => {
                                                                                                                                                    try {
                                                                                                                                                        logUtils.iotlog.info('_handleCardAlarmSetting', err, JSON.stringify(jsonObj));
                                                                                                                                                        if (err) {
                                                                                                                                                            reject(err);
                                                                                                                                                        } else {
                                                                                                                                                            if (jsonObj['operation_out'] && jsonObj['operation_out']['response']) {
                                                                                                                                                                const response = jsonObj['operation_out']['response'];
                                                                                                                                                                if (response['resp_type'] == "0") {
                                                                                                                                                                    const content = jsonObj['operation_out']['content'],
                                                                                                                                                                        ret_code = content['ret_code'],
                                                                                                                                                                        ret_desc = content['ret_desc'],
                                                                                                                                                                        cfg_id = content['cfg_id'];
                                                                                                                                                                } else {
                                                                                                                                                                    //接口错误
                                                                                                                                                                    resolve({ code: '505' });
                                                                                                                                                                }
                                                                                                                                                            } else {
                                                                                                                                                                //返回格式错误
                                                                                                                                                                resolve({ code: '505' });
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    } catch (err) {
                                                                                                                                                        logUtils.iotlog.error('_handleCardAlarmSetting', err);
                                                                                                                                                        reject(err);
                                                                                                                                                    }
                                                                                                                                                });
                                                                                                                                            });
                                                                                                                                            return await handlePromise;
                                                                                                                                        }
};
module.exports = HUAIAN_CMCC;