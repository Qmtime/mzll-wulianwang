const path = require('path'),
    moment = require('moment'),
    utils = require('../../utils'),
    config = require('../config/' + path.basename(__filename));

class Beijing_CMCC {
    //用户当月GPRS查询
    buildPostOptioins(params) {
        const paramObj = {
            appid: config.appid,
            transid: config.appid + moment().format('YYYYMMDDHHMISS') + parseInt(Math.random() * Math.pow(10, 8)),
            ebid: config.ebid,
            msisdn: params['msisdn']
        };
        params['query_date'] ? paramObj['query_date'] = moment(params['query_date']).format('YYYYMMDD') : moment().format('YYYYMMDD');
        paramObj['token'] = utils.sha256base64(paramObj['appid'], config.httpClientPwd, paramObj['transid']);
        let query_url = '';
        switch (params['query_type']) {
            case 'gprsusedinfosingle':
                query_url = config.gprsusedinfosinglePath;
                break;
            case 'batchsmsusedbydate':
                query_url = config.batchsmsusedbydatePath;
                break;
            case 'batchgprsusedbydate':
                query_url = config.batchgprsusedbydatePath;
                break;
            case 'balancerealsingle':
                query_url = config.balancerealsinglePath;
                break;
            case 'smsusedinfosingle':
                query_url = config.smsusedinfosinglePath;
                break;
            case 'groupuserinfo':
                query_url = config.groupuserinfoPath;
                break;
            case 'smsusedbydate':
                query_url = config.smsusedbydatePath;
                break;
            case 'gprsrealtimeinfo':
                query_url = config.gprsrealtimeinfoPath;
                break;
            case 'gprsrealsingle':
                query_url = config.gprsrealsinglePath;
                break;
            case 'userstatusrealsingle':
                query_url = config.userstatusrealsinglePath;
                break;
            case 'cardinfo':
                query_url = config.cardinfoPath;
                break;
            case 'onandoffrealsingle':
                query_url = config.onandoffrealsinglePath;
                break;
            case 'multiapninfo':
                query_url = config.multiapninfoPath;
                break;
            case 'smsstatusreset':
                query_url = config.smsstatusresetPath;
                break;
            case 'location_slis':
                query_url = config.location_slisPath;
                break;
            case 'sendsmscustom':
                query_url = config.sendsmscustomPath;
                break;
            default:
                break;
        };
        const url = `http://${config.host}:${config.port}${query_url}`,
            options = {
                method: 'POST',
                data: paramObj,
                dataType: 'json'
            };
        return { url: url, options: options };
    }


}

module.exports = new Beijing_CMCC();