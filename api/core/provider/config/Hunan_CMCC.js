module.exports = {
    // host: '183.230.96.66',
    // port: '8087',
    host: '111.10.45.200', //沙箱
    port: '7000', //沙箱
    // appid: 'UHRH9C0SCLN4FR',
    // httpClientPwd: 'QMSG428hn'
    appid: 'A3CI4FV',
    httpClientPwd: 'SXHJ837bj',
    gprsrealsingle: {
        path: '/v2/gprsrealsingle',
        ebid: '0001000000008'
    },
    userstatusrealsingle: {
        path: '/v2/userstatusrealsingle',
        ebid: '0001000000009'
    },
    cardinfo: {
        path: '/v2/cardinfo',
        ebid: '0001000000010'
    },
    gprsusedinfosingle: {
        path: '/v2/gprsusedinfosingle',
        ebid: '0001000000012'
    },
    onandoffrealsingle: {
        path: '/v2/onandoffrealsingle',
        ebid: '0001000000025'
    },
    querysmsopenstatus: {
        path: '/v2/querysmsopenstatus',
        ebid: '0001000000429'
    },
    querygprsopenstatus: {
        path: '/v2/querygprsopenstatus',
        ebid: '0001000000430'
    },
    queryapnopenstatus: {
        path: '/v2/queryapnopenstatus',
        ebid: '0001000000431'
    },
    querycardlifecycle: {
        path: '/v2/querycardlifecycle',
        ebid: '0001000000432'
    },
    useropenservice: {
        path: '/v2/useropenservice',
        ebid: '0001000000447'
    },
    // querycardopentime: {
    //     path: '/v2/querycardopentime',
    //     ebid:''
    // },
    groupuserinfo: {
        path: '/v2/groupuserinfo',
        ebid: '0001000000039'
    },
    arrearageuserinfo: {
        path: '/v2/arrearageuserinfo',
        ebid: '0001000000328'
    },
    querycardcount: {
        path: '/v2/querycardcount',
        ebid: '0001000000417'
    },
    queryabnormalcardcount: {
        path: '/v2/queryabnormalcardcount',
        ebid: '0001000000427'
    },
    querygprsonlinecardcount: {
        path: '/v2/querygprsonlinecardcount',
        ebid: '0001000000428'
    },
    // bjquerygroupuserchangenum: {
    //     path: '/v2/bjquerygroupuserchangenum',
    //     ebid:''
    // },
    balancerealsingle: {
        path: '/v2/balancerealsingle',
        ebid: '0001000000035'
    },
    batchsmsusedbydate: {
        path: '/v2/batchsmsusedbydate',
        ebid: '0001000000026'
    },
    batchgprsusedbydate: {
        path: '/v2/batchgprsusedbydate',
        ebid: '0001000000027'
    },
    smsusedinfosingle: {
        path: '/v2/smsusedinfosingle',
        ebid: '0001000000036'
    },
    smsusedbydate: {
        path: '/v2/smsusedbydate',
        ebid: '0001000000040'
    },
    gprsrealtimeinfo: {
        path: '/v2/gprsrealtimeinfo',
        ebid: '0001000000083'
    },
    gprsusedinfosinglebydate: {
        path: '/v2/gprsusedinfosinglebydate',
        ebid: '0001000000407'
    },
    querycardprodinfo: {
        path: '/v2/querycardprodinfo',
        ebid: '0001000000264'
    },
    location_area: {
        path: '/v2/location_area',
        ebid: '0001000000459'
    },
    smsstatusreset: {
        path: '/v2/smsstatusreset',
        ebid: '0001000000034'
    },
    status: {
        '00': { code: '0', desc: '正常' },
        '01': { code: '1', desc: '单向停机' },
        '02': { code: '2', desc: '停机' },
        '03': { code: '3', desc: '预销号' },
        '04': { code: '4', desc: '销号' },
        '05': { code: '5', desc: '过户' },
        "06": { code: '6', desc: '休眠' },
        '07': { code: '7', desc: '待激活' },
        '99': { code: '8', desc: '号码不存在' }
    },
    cycle: {
        '00': { cycleCode: '0', cycleDesc: '正式期' },
        '01': { cycleCode: '1', cycleDesc: '测试期' },
        '02': { cycleCode: '2', cycleDesc: '沉默期' },
        '03': { cycleCode: '3', cycleDesc: '其他' }
    },
    onstatus: {
        '0': '关机',
        '1': '开机'
    },
    smsReset: {
        '0': '重置成功',
        '100': '短信重置刷新失败'
    }
};