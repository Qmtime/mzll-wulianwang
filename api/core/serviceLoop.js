"use strict"
const env = process.env.NODE_ENV || require('./package.json').NODE_ENV;

console.log("Start Core Service in [", env, "] mode.");

process.configure = require('./config')(env);
const db = require('./db/index');

try {
    db.loadParameters((err, result) => {
    	require('./service');
    });
} catch (err) {
    console.error(err);
}