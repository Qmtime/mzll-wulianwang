"use strict";
const path = require('path'),
    utils = require(path.join('../../', 'util')),
    logUtils = require(path.join('../../', 'util', 'log')),
    moment = require('moment'),
    request = require('request-promise'),
    async = require('async'),
        db = require('../../db'),
        RedisKeys = require('../../db/REDIS_KEYS'),
        objectSort = require('object-sort');



/**
 * 对单个号卡停开机操作
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function switch_member(params) {
    const msisdn = params['msisdn'],
        transid = params['transid'],
        appkey = params['appkey'],
        optType = params['optType'],
        optTime = moment().format("YYYY-MM-DD HH:mm:ss");
    try {
        params['apiMethod'] = 'switch_member';

        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数
        if (checkApiParamsResult['code'] == '200') {
            const cardResult = await db.query_memberInfo(params);
            //缓存不存在，从运营商侧取值
            if (cardResult['code'] == '200') {
                const cardInfo = cardResult['cardInfo'];
                if (cardInfo['provider_code']) {
                    const providerCode = cardInfo['provider_code'];
                    const providerApi = require(`../../provider/obj/${providerCode}`);
                    const options = providerApi.buildSwitchMember(params);
                    const queryResult = await request(options);
                    let result = await providerApi.handleSwitchMember(options, queryResult, params);
                    if (result['code'] == '200') {
                        //记录变更操作日志
                        const rediskey = RedisKeys.REDIS_KEY_PRE_SWITCH_MEMBER_KEY,
                            redisvalue = { appkey: appkey, transid: transid, msisdns: msisdn, optType: optType, optTime: optTime, finishTime: moment().format("YYYY-MM-DD HH:mm:ss"), order_status: 2, order_no: utils.generateRandom(5) + utils.gethostname() };
                        db.recordLogRecord({ rediskey, redisvalue });
                    }
                    return result;
                } else {
                    logUtils.iotlog.info(`switch_member missing provider:`, msisdn);
                    return { code: '501', transid: transid };
                }
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        logUtils.iotlog.info(`switch_member error:`, err);
        return { code: '511' };
    }
}
/**
 * 查询批量号卡信息查询
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function switch_batchMember(params) {
    const msisdn = params['msisdn'],
        transid = params['transid'],
        appkey = params['appkey'],
        optType = params['optType'],
        optTime = moment().format("YYYY-MM-DD HH:mm:ss");
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'switch_batchMember';
        const checkApiParamsResult = await db.checkApiParams(params);

        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商
            let resultList = [];
            const msisdns = params['msisdns'].split(',');
            for (let msisdn of msisdns) {
                params['msisdn'] = msisdn;
                const cardResult = await db.query_memberInfo(params);
                //缓存不存在，从运营商侧取值
                if (cardResult['code'] == '200') {
                    const cardInfo = cardResult['cardInfo'];
                    if (cardInfo['provider_code']) {
                        const providerCode = cardInfo['provider_code'];
                        const providerApi = require(`../../provider/obj/${providerCode}`);
                        const options = providerApi.buildSwitchMember(params);
                        const queryResult = await request(options);
                        let result = await providerApi.handleSwitchMember(options, queryResult, params);
                        if (result['code'] == '200') {
                            //解析成功
                            const switch_staus = result['result'];
                            const obj = { msisdn: msisdn, switch_staus: switch_staus }
                            resultList.push(obj);
                        }
                    }
                }
            }
            await Promise.all(msisdns);
            return { code: '200', result: resultList };
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`switch_batchMember error:`, err);
        return { code: '511' };
    }
}