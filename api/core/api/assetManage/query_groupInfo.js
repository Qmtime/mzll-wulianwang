"use strict";
const path = require('path'),
    utils = require(path.join('../../', 'util')),
    logUtils = require(path.join('../../', 'util', 'log')),
    moment = require('moment'),
    request = require('request-promise'),
    async = require('async'),
        db = require('../../db'),
        objectSort = require('object-sort');

/**
 * 集团客户信息查询
 * @param  {[type]} params [description]
 * @return {[type]} [description]
 */
export async function query_groupInfo(params){
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'query_groupInfo';
        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商

            const providerInfo = await db.matchProviderByMember(params);
            if (providerInfo['code'] == '200') {

            } else {
                return providerInfo;
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`matchProvider error:`, err);
        return { code: '511' };
    }
}
