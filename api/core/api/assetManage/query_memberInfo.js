"use strict";
const path = require('path'),
    utils = require(path.join('../../', 'util')),
    logUtils = require(path.join('../../', 'util', 'log')),
    moment = require('moment'),
    request = require('request-promise'),
    async = require('async'),
        db = require('../../db'),
        objectSort = require('object-sort');



/**
 * 查询单个号卡信息查询
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_memberInfo(params) {
    try {
        params['apiMethod'] = 'query_memberInfo';
        const msisdn = params['msisdn'],
            transid = params['transid'];

        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数
        if (checkApiParamsResult['code'] == '200') {
            //判断redis缓存中是否存在，如果存在直接读取缓存，如果不存在则溯源到运营商
            const existResult = await db.cachedRecordExist(params);
            if (existResult['code'] == '200') {
                if (existResult['cardInfo']) {
                    //存在缓存记录
                    return existResult;
                } else {
                    const cardResult = await db.query_memberInfo(params);
                    //缓存不存在，从运营商侧取值
                    if (cardResult['code'] == '200') {
                        const cardInfo = cardResult['cardInfo'];
                        if (cardInfo['provider_code']) {
                            const providerCode = cardInfo['provider_code'];
                            const providerApi = require(`../../provider/obj/${providerCode}`);
                            const memberInfoOptions = providerApi.buildMemberInfo(params);
                            const queryResult = await request(memberInfoOptions);
                            let result = await providerApi.handleMemberInfo(memberInfoOptions, queryResult, params);
                            return result;
                        } else {
                            logUtils.iotlog.info(`query_memberInfo missing provider:`, msisdn);
                            return { code: '501', transid: transid };
                        }
                    } else {
                        return cardResult;
                    }
                }
            } else {
                return existResult;
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        logUtils.iotlog.info(`query_memberInfo error:`, err);
        return { code: '511' };
    }
}
/**
 * 查询批量号卡信息查询
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_batchMemberInfo(params) {
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'query_batchMemberInfo';
        const checkApiParamsResult = await db.checkApiParams(params);

        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商
            let batchMemberInfo = [];
            const msisdns = params['msisdns'].split(',');
            for (let msisdn of msisdns) {
                params['msisdn'] = msisdn;
                //判断redis缓存中是否存在，如果存在直接读取缓存，如果不存在则溯源到运营商
                const existResult = await db.cachedRecordExist(params);

                if (existResult['code'] == '200') {
                    if (existResult['cardInfo']) {
                        //记录存在缓存中
                        batchMemberInfo.push(existResult['cardInfo']);
                    } else {
                        const cardResult = await db.query_memberInfo(params);
                        //缓存不存在，从运营商侧取值
                        if (cardResult['code'] == '200') {
                            const cardInfo = cardResult['cardInfo'];
                            if (cardInfo['provider_code']) {
                                const providerCode = cardInfo['provider_code'];
                                const providerApi = require(`../../provider/obj/${providerCode}`);
                                const memberInfoOptions = providerApi.buildMemberInfo(params);
                                const queryResult = await request(memberInfoOptions);
                                let memberInfo = await providerApi.handleMemberInfo(memberInfoOptions, queryResult, params);
                                if (memberInfo['code'] == '200') {
                                    //解析成功
                                    const memberInfoObj = memberInfo['result'];
                                    batchMemberInfo.push(memberInfoObj);
                                }
                            }
                        }
                    }
                }
            }
            await Promise.all(msisdns);
            console.log('result', batchMemberInfo);
            return { code: '200', result: batchMemberInfo };
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`query_batchMemberInfo error:`, err);
        return { code: '511' };
    }
}