"use strict";
const path = require('path'),
    utils = require(path.join('../../', 'util')),
    logUtils = require(path.join('../../', 'util', 'log')),
    moment = require('moment'),
    request = require('request-promise'),
    async = require('async'),
        db = require('../../db'),
        objectSort = require('object-sort');



/**
 * 查询单个号卡进入正使用期的时间
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_activeTime(params) {
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'query_activeTime';
        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商
            const cardResult = await db.query_memberInfo(params);

            if (cardResult['code'] == '200') {
                const cardInfo = cardResult['cardInfo'];
                if (cardInfo['provider_code']) {
                    const providerCode = cardInfo['provider_code'];
                    const providerApi = require(`../../provider/obj/${providerCode}`);
                    const actimeTimeOptions = providerApi.buildActiveTime(params);
                    const queryResult = await request(actimeTimeOptions);
                    let result = await providerApi.handleActiveTime(actimeTimeOptions, queryResult, params);
                    return result;
                } else {
                    logUtils.iotlog.info(`query_activeTime missing provider:`, msisdn);
                    return { code: '501', transid: transid };
                }
            } else {
                return cardResult;
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`query_activeTime error:`, err);
        return { code: '511' };
    }
}
/**
 * 查询分批号卡进入正使用期的时间
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_batchActiveTime(params) {
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'query_batchActiveTime';
        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商

            const providerInfo = await db.matchProviderByMember(params);
            if (providerInfo['code'] == '200') {

            } else {
                return providerInfo;
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`query_batchActiveTime error:`, err);
        return { code: '511' };
    }
}