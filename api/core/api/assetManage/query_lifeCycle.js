"use strict";
const path = require('path'),
    utils = require(path.join('../../', 'util')),
    logUtils = require(path.join('../../', 'util', 'log')),
    moment = require('moment'),
    request = require('request-promise'),
    async = require('async'),
        db = require('../../db'),
        objectSort = require('object-sort');



/**
 * 查询单个号卡当前生命周期状态
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_lifeCycle(params) {
    try {
        params['apiMethod'] = 'query_lifeCycle';
        const msisdn = params['msisdn'],
            transid = params['transid'];

        const checkApiParamsResult = await db.checkApiParams(params);
        //校验参数
        if (checkApiParamsResult['code'] == '200') {
            const cardResult = await db.query_memberInfo(params);
            //缓存不存在，从运营商侧取值
            if (cardResult['code'] == '200') {
                const cardInfo = cardResult['cardInfo'];
                if (cardInfo['provider_code']) {
                    const providerCode = cardInfo['provider_code'];
                    const providerApi = require(`../../provider/obj/${providerCode}`);
                    const lifeCycleOptions = providerApi.buildLifeCycle(params);
                    const queryResult = await request(lifeCycleOptions);
                    let result = await providerApi.handleLifeCycle(lifeCycleOptions, queryResult, params);
                    return result;
                } else {
                    logUtils.iotlog.info(`query_lifeCycle missing provider:`, msisdn);
                    return { code: '501', transid: transid };
                }
            }
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        logUtils.iotlog.info(`query_lifeCycle error:`, err);
        return { code: '511' };
    }
}
/**
 * 查询批量号卡信息查询
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
export async function query_batchLifeCycle(params) {
    //校验参数
    //查找对应的运营商
    try {
        params['apiMethod'] = 'query_batchLifeCycle';
        const checkApiParamsResult = await db.checkApiParams(params);

        //校验参数通过
        if (checkApiParamsResult['code'] == '200') {
            //通过号卡查找对应运营商
            let resultList = [];
            const msisdns = params['msisdns'].split(',');
            for (let msisdn of msisdns) {
                params['msisdn'] = msisdn;
                const cardResult = await db.query_memberInfo(params);
                //缓存不存在，从运营商侧取值
                if (cardResult['code'] == '200') {
                    const cardInfo = cardResult['cardInfo'];
                    if (cardInfo['provider_code']) {
                        const providerCode = cardInfo['provider_code'];
                        const providerApi = require(`../../provider/obj/${providerCode}`);
                        const lifeCycleOptions = providerApi.buildLifeCycle(params);
                        const queryResult = await request(lifeCycleOptions);
                        let memberInfo = await providerApi.handleLifeCycle(lifeCycleOptions, queryResult, params);
                        if (memberInfo['code'] == '200') {
                            //解析成功
                            const lifeCicle = memberInfo['result'];
                            const obj = {msisdn:msisdn,lifeCicle:lifeCicle}
                            resultList.push(obj);
                        }
                    }
                }
            }
            await Promise.all(msisdns);
            return { code: '200', result: resultList };
        } else {
            return checkApiParamsResult;
        }
    } catch (err) {
        console.log(err);
        logUtils.iotlog.info(`query_batchLifeCycle error:`, err);
        return { code: '511' };
    }
}