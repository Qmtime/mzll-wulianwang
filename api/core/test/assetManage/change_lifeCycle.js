"use strict";
var util = require('../../util');
const moment = require('moment'),
    querystring = require('querystring'),
    objectSort = require('object-sort'),
    request = require('request'),
    path = require('path');

const appsecret = '57b7f08849be6bf28115a36c14515cc0';
let order = {
    msisdn: '1064708733469',
    appkey: 'g3xJN80mbi6XlKnv',
    transid: moment().format('YYYYMMDDHHmmssSSS'),
    optType: 2 //1测试期到期处理；2沉默期激活处理；3库存期激活处理
};
order = objectSort(order);
let signStr = '';
for (let key in order) {
    if (order.hasOwnProperty(key)) {
        signStr += key + order[key];
    }
};

console.log('signStr', signStr + appsecret);
order['sign'] = util.md5(signStr + appsecret);

var options = {
    url: 'http://123.57.147.172:3055/iot/assetManage/change_lifeCycle',
    // url: 'http://127.0.0.1:3055/iot/assetManage/change_lifeCycle',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(order)
};

request(options, (error, response, body) => {
    console.log(error, body);
});