/**
 * Created by BoZhang on 16/3/17.
 */
var moment = require('moment'),
    path = require('path'),
    redis = require('./redisdb'),
    RedisKeys = require('./REDIS_KEYS'),
    bluebird = require('bluebird'),
    logUtils = require('../util/log');



const db = require(path.join(__dirname, '..', '..', '..', 'model-public', 'sequelize')).init(process.env.NODE_ENV);

const sequelize = db.sequelize,
    replySMS = db.replySMS,
    SMSOrder = db.SMSOrder;

// replySMS.sync();
// SMSOrder.sync();

exports.generate_SMS_Order = function(mobile, content, callback) {
    "use strict";
    var smsOrder = {
        sms_mobile: mobile,
        sms_contents: content,
        sms_send_time: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    // 判断该手机号码之前是否有未完成的短信数据
    redis.getAsync(RedisKeys.REDIS_KEY_PRE_SMS + mobile).then(oldSmsOrderStr => {
        var oldSmsOrder = JSON.parse(oldSmsOrderStr);

        // 如之前的短信未持久化的短信, 需要存放到数据库
        var order = SMSOrder.build(oldSmsOrder);
        order.save().catch(reason => {
            logUtils.smslog.error(' SMSdb => generate_SMS_Order -> save mysql [',oldSmsOrder,'] err ', reason);
        });

        // 替换掉redis中的旧的短信数据
        var smsOrderStr = JSON.stringify(smsOrder);
        redis.setAsync(RedisKeys.REDIS_KEY_PRE_SMS + mobile, smsOrderStr).then(smsOrder => {
            callback(null, smsOrder);
        }).catch(reason => {
            logUtils.smslog.error(' SMSdb => generate_SMS_Order -> set [',RedisKeys.REDIS_KEY_PRE_SMS + mobile,'] to redis ' + smsOrderStr + ' err ', reason);
            callback(reason, null);
        });

    }).catch(reason => {
        logUtils.smslog.error(' SMSdb => generate_SMS_Order -> get [',RedisKeys.REDIS_KEY_PRE_SMS + mobile,'] from redis err ', reason);
        callback(reason, null);
    });
};

exports.generate_SMS_OrderPO = function(mobile, providerOrder, content, sendStatus, callback) {
    "use strict";
    var smsOrder = {
        sms_mobile: mobile,
        sms_provider_order: providerOrder,
        sms_contents: content,
        sms_send_status: sendStatus,
        sms_send_time: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    var smsOrderStr = JSON.stringify(smsOrder);
    redis.setAsync(RedisKeys.REDIS_KEY_PRE_SMS + providerOrder, smsOrderStr).then(smsOrder => {
        callback(null, smsOrder);
    }).catch(reason => {
        logUtils.smslog.error(' SMSdb => generate_SMS_OrderPO -> set [',RedisKeys.REDIS_KEY_PRE_SMS + providerOrder,'] to redis ' + smsOrderStr + ' err ', reason);
        callback(reason, null);
    });
};

exports.update_SMS_OrderPO = function(providerOrder, recvtime, reportStatus, callback) {
    "use strict";
    redis.getAsync(RedisKeys.REDIS_KEY_PRE_SMS + providerOrder).then(smsOrderStr => {
        var smsOrder = JSON.parse(smsOrderStr);
        if (smsOrder) {
            smsOrder.sms_recvtime = recvtime;
            smsOrder.sms_report_status = reportStatus;
            smsOrderStr = JSON.stringify(smsOrder);
            redis.setAsync(RedisKeys.REDIS_KEY_PRE_SMS + providerOrder, smsOrderStr).then(result => {
                callback(null, result);
            }).catch(reason => {
                logUtils.smslog.error(' SMSdb => update_SMS_OrderPO -> update [',providerOrder,'] to redis ' + smsOrderStr + ' err ', reason);
                callback(reason, null);
            });
        } else {
            logUtils.smslog.error(' SMSdb => update_SMS_OrderPO -> update [',providerOrder,'] to redis ' + smsOrderStr + ' err NULL in Redis');
            callback('null stored', null);
        }
    });
};

exports.update_SMS_Order = function(mobile, providerOrder, recvtime, sendStatus, reportStatus, callback) {
    "use strict";
    redis.getAsync(RedisKeys.REDIS_KEY_PRE_SMS + mobile).then(smsOrderStr => {
        var smsOrder = JSON.parse(smsOrderStr);
        if (!smsOrder.sms_provider_order) {
            // redis 中存在的短信数据 无 订单号,则表示此处更新为请求发送短信的同步返回结果调用
            smsOrder.sms_provider_order = providerOrder;
            smsOrder.sms_send_status = sendStatus;
        } else if (smsOrder.sms_provider_order === providerOrder) {
            // redis 中存在的短信数据有订单号且 相等 ,则表示此处更新为 先前 请求发送短信成功后notfiy回调,此前短信数据在Redis中
            smsOrder.sms_recvtime = recvtime;
            smsOrder.sms_report_status = reportStatus;
        } else {
            // redis 中存在的短信数据有订单号且 不等 ,则表示此处更新为 曾经 请求发送短信成功后notfiy回调,此前短信数据在数据库中
            var options = {
                where: {
                    SMS_provider_order: providerOrder
                }
            };
            SMSOrder.findOne(options).then(order => {
                order.sms_recvtime = recvtime;
                order.sms_report_status = reportStatus;
                order.save();
            }).catch(reason => {
                logUtils.smslog.error(' SMSdb => update_SMS_Order -> sql findOne [',options,'] in mysql err ', reason);
            });
        }

        var newSmsOrderStr = JSON.stringify(smsOrder);
        redis.setAsync(RedisKeys.REDIS_KEY_PRE_SMS + mobile, newSmsOrderStr).then(smsOrderStr => {
            callback(null, smsOrder);
        }).catch(reason => {
            logUtils.smslog.error(' SMSdb => update_SMS_Order -> set [',RedisKeys.REDIS_KEY_PRE_SMS + mobile,'] to redis ' + newSmsOrderStr + ' err ', reason);
            callback(reason, null);
        });

    }).catch(reason => {
        logUtils.smslog.error(' SMSdb => update_SMS_Order -> get [',RedisKeys.REDIS_KEY_PRE_SMS + mobile,'] from redis err ', reason);
        callback(reason, null);
    });
};

exports.get_SMS_Order = function(mobile, callback) {
    "use strict";
    redis.getAsync(RedisKeys.REDIS_KEY_PRE_SMS + mobile).then(smsOrder => {
        callback(null, smsOrder);
    }).catch(reason => {
        callback(reason, null);
    });
};

exports.save_SMS_Reply = function(sms, callback) {
    "use strict";
    replySMS.create(sms).then(reply => {
        callback(null, reply);
    }).catch(reason => {
        logUtils.smslog.error(' SMSdb => save_SMS_Reply -> sql create [',sms,'] in mysql err ', reason);
        callback(reason, null);
    });
};
