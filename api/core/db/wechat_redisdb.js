"use strict";

var Redis = require("redis");
var bluebird = require('bluebird');
const cluster = require('cluster');
var db = require('./index');
const logUtils = require('../util/log');

const options = {
    host: process.configure.redis.host,
    port: process.configure.redis.port,
    db: 0,
    retry_strategy: function(options) {
        if (options.total_retry_time > 1000 * 30) {
            const content = "最高警报：Core API的Redis服务器断开，重连30秒无效，请立即处理！";
            // End reconnecting after a specific timeout and flush all commands with a individual error
            if (process.env.NODE_ENV.indexOf('prod') && cluster.isMaster) {
                const warnTitle = 'redis连接错误',
                    warnContent = 'redis连接错误，请立刻恢复';
                db.wechatWarn(warnTitle, warnContent);
            }
            // 重连40次无效，系统将退出让pm2将其重启，其为了解决阿里云中redis服务器断开连接，必须客户端重启才能连接上的问题。
            process.exit(0);
        }
        if (options.error && options.error.code === 'ECONNREFUSED') {
            // End reconnecting on a specific error and flush all commands with a individual error
            const content = "最高警报：Core API的Redis服务器连接被拒绝！";
            if (process.env.NODE_ENV.indexOf('prod') && cluster.isMaster) {
                const warnTitle = 'redis连接中端',
                    warnContent = 'redis连接中断，请立刻恢复';
                db.wechatWarn(warnTitle, warnContent);
            }
            return new Error('The server refused the connection');
        }
        if (options.times_connected > 10) {
            // End reconnecting with built in errorgit
            return undefined;
        }
        // reconnect after
        console.info(' --------------- reconnect to server --------options.total_retry_time ------- ', options.total_retry_time);
        console.info(' --------------- reconnect to server ~~~~~~~~options.times_connected ~~~~~~~ ', options.times_connected);
        return Math.min(options.attempt * 100, 750);
    }
};

const redis = Redis.createClient(options);
if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'stage') {
    redis.auth(process.configure.redis.auth);
}


bluebird.promisifyAll(Redis.RedisClient.prototype);
bluebird.promisifyAll(Redis.Multi.prototype);

redis.on("error", function(err) {
    console.error(err);
});




module.exports = redis;