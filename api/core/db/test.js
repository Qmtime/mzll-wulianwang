var path = require('path'),
    moment = require('moment');
var db = require(path.join(__dirname, '..', '..', '..', 'model-public', 'sequelize')).init('stage');
var customer = db.customer,
    order = db.productOrder,
    sequelize = db.sequelize;

const step02Str = 'SELECT COUNT(*) as "inchargeNum",CASE `vendor` WHEN 1 THEN "移动" WHEN 2 THEN "联通" ELSE "电信" END as "vendor", phone_area,product_size,product_using_scope,product_id FROM `productorder`  WHERE order_status in (1,2) and request_time>=? and DATE_ADD(request_time,INTERVAL ? MINUTE) < NOW() and `productorder`.`product_id` in (SELECT `customerproduct`.`product_id` FROM `customerproduct` WHERE `customerproduct`.`customer_id` =? and `product_deleted` =0) GROUP BY  vendor, phone_area,product_size,product_using_scope';

sequelize.query(step02Str, { replacements: [moment().subtract(3, 'hours').format('YYYY-MM-DD HH:mm:ss'), 10, 443], type: sequelize.QueryTypes.SELECT }).then(orders => {
    const filterOrders = orders.filter(filterOrder => {
        return filterOrder['inchargeNum'] >= 1;
    });
    const offProducts = filterOrders.map(mapOrder => {
        const mapKey = mapOrder['vendor'] + '-' + mapOrder['phone_area'].split('-')[1] + '-' + mapOrder['product_size'] + '-' + mapOrder['product_using_scope'];
        const mapObj = {};
        mapObj[mapKey] = mapOrder['product_id'];
        return JSON.stringify(mapObj);
    });
    console.log('step02-offProducts', offProducts);
    const step02PassList = filterOrders.map(mapOrder => { return mapOrder['product_id'] + '' });
}).catch(err => { console.error(err) });