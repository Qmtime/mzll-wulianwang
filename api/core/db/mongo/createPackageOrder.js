"use strict";
var moment = require('moment');
var path = require('path');
var logUtils = require('../../util/log');


const mongo = require(path.join(__dirname, '..', '..', '..', 'model-public', 'mongo')).init(process.env.NODE_ENV);

const createPackageOrder = mongo['createPackageOrder'];

function createPackageOrderModel() {
    this.createPackageOrder = createPackageOrder;
};

createPackageOrderModel.prototype.insert = function(orderObj) {
    var orderOBJ = new this.createPackageOrder(orderObj);
    var error = orderOBJ.validateSync();
    if (error) {
        console.error(`createPackageOrderModel,In, ${error}`);
        logUtils.didi_mongolog.error('createPackageOrder,In ', error);
    }
    this.createPackageOrder.create(orderOBJ).catch(err => {
        console.error(`createPackageOrderModel,In, ${err}`);
        logUtils.didi_mongolog.error('createPackageOrder,In ', err);
    });
};

createPackageOrderModel.prototype.findOne = function(params, callback) {
    createPackageOrder.findOne(params).then(data => {
        if (data) {
            callback(null, data);
        } else {
            callback(null, null);
        }
    }).catch(err => {
        console.error(`createPackageOrderModel,findOne, ${err}`);
        logUtils.didi_mongolog.error('createPackageOrderModel,findOne ', err);
        callback(err, null);
    });
}

module.exports = createPackageOrderModel;