"use strict";
var moment = require('moment');
var path = require('path');
var logUtils = require('../../util/log');


const mongo = require(path.join(__dirname, '..', '..', '..', 'model-public', 'mongo')).init(process.env.NODE_ENV);

const createCardOrder = mongo['createCardOrder'];

function createCardOrderModel() {
    this.createCardOrder = createCardOrder;
};

createCardOrderModel.prototype.insert = function(orderObj) {
    var orderOBJ = new this.createCardOrder(orderObj);
    var error = orderOBJ.validateSync();
    if (error) {
        console.error(`createCardOrder,In, ${error}`);
        logUtils.didi_mongolog.error('createCardOrder,In ', error);
    }
    this.createCardOrder.create(orderOBJ).catch(err => {
        console.error(`createCardOrder,In, ${err}`);
        logUtils.didi_mongolog.error('createCardOrder,In ', err);
    });
};

createCardOrderModel.prototype.findOne = function(params, callback) {
    createCardOrder.findOne(params).then(data => {
        if (data) {
            callback(null, data);
        } else {
            callback(null, null);
        }
    }).catch(err => {
        console.error(`createCardOrderModel,findOne, ${err}`);
        logUtils.didi_mongolog.error('createCardOrderModel,findOne ', err);
        callback(err, null);
    });
}

module.exports = createCardOrderModel;