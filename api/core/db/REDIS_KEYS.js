module.exports = {

    REDIS_KEY_PRE_PARAMETER:'wlw_parameter',//系统参数
    REDIS_KEY_PRE_REALFLOW:'wlw_realflow',//实时流量
    REDIS_KEY_PRE_CUSTOMER_APPKEY:'iotcusKey_',//客户信息 db:5
    REDIS_KEY_PRE_CARD_KEY:'iotcardKey_',//号卡信息
    REDIS_KEY_PRE_CHANGE_LIFECYCLE_KEY:'iotChangeLifecycleKey_',//改变生命周期信息
    REDIS_KEY_PRE_SWITCH_MEMBER_KEY:'iotSwitchMemberKey_',//停复机信息
    REDIS_KEY_PRE_SWITCH_SERVICE_KEY:'iotSwitchServiceKey_',//服务开停信息

};