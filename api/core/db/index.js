'use strict';
const moment = require('moment'),
    path = require('path'),
    RedisKeys = require('./REDIS_KEYS'),
    async = require('async'),
        requestPromise = require('request-promise');

const redis = require("./redisdb"),
    wechat_api = require('../util/wechat_api'),
    SMSHelper = require('../util/SMSHelper'),
    wechatConfig = require('../config/wechat')(process.env.NODE_ENV),
    utils = require('../util'),
    _ = require('lodash'),
    objectSort = require('object-sort'),
    logUtils = require('../util/log');

const smsHelper = new SMSHelper();

const HOST_SERVER = utils.gethostname();

const db = require(path.join(__dirname, '..', '..', 'model-public', 'sequelize')).init(process.env.NODE_ENV),
    sequelize = db.sequelize,
    Admin = db['admin'],
    Apn = db['apn'],
    Card = db['card'],
    ChangeLifeCycle = db['changeLifeCycle'],
    Customer = db['customer'],
    CustomerProduct = db['customerProduct'],
    PoolMember = db['poolMember'],
    Product = db['product'],
    ProductOrder = db['productOrder'],
    ProductUsed = db['productUsed'],
    Provider = db['provider'],
    Role = db['role'],
    SetAlarm = db['setAlarm'],
    SwitchService = db['switchService'];

var lock = require("redis-lock")(redis);
const LOCK_CUSTOMER_FREEZE_PHONE_BALANCE_PREFIX = 'lock_cust_membership_freezed_balance_',
    LOCK_CUSTOMER_MEMBERSHIP_BALANCE_PREFIX = 'lock_cust_membership_balance_',
    LOCK_ORDER_PREFIX = 'orderno_',
    LOCK_CHECKEXTNO_ = 'lock_checkZhuanShuExtno_',
    LOCK_CHECKPACKAGEEXTNO_ = 'lock_checkZhuanshuPackageExtno_';

/**
 * 校验入参合法性
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
exports.checkApiParams = async function(params) {
    try {
        const sign = params['sign'],
            transid = params['transid'];
        params = objectSort(params);
        let parmstr = '',
            filterParams = [],
            Unencrypt = [],
            lengthLimit, //检验的接口参数
            checkIp = false;
        switch (params['apiMethod']) {
            case 'query_groupInfo':
                filterParams = ['appkey'];
                break;
            case 'query_memberInfo':
                filterParams = ['appkey', 'msisdn', 'transid'];
                break;
            case 'query_batchMemberInfo':
                filterParams = ['appkey', 'msisdns', 'transid'];
                break;
            case 'query_activeTime':
                filterParams = ['appkey', 'msisdn', 'transid'];
                break;
            case 'query_lifeCycle':
                filterParams = ['appkey', 'msisdn', 'transid'];
                break;
            case 'query_batchLifeCycle':
                filterParams = ['appkey', 'msisdns', 'transid'];
                break;
            case 'change_lifeCycle':
                filterParams = ['appkey', 'msisdn', 'transid', 'optType'];
                break;
            case 'change_batchLifiCycle':
                filterParams = ['appkey', 'msisdns', 'transid', 'optType'];
                break;
            case 'switch_member':
                filterParams = ['appkey', 'msisdn', 'transid', 'optType'];
                break;
            case 'switch_batchMember':
                filterParams = ['appkey', 'msisdns', 'transid', 'optType'];
                break;
            case 'switch_service':
                filterParams = ['appkey', 'msisdn', 'transid', 'switchType', 'optType'];
                break;
            default:
                break;
        }

        //验证必要参数
        for (var property of filterParams) {
            const mustpropertyFlag = params.hasOwnProperty(property);
            if (!mustpropertyFlag) {
                //不满足参数要求
                const obj = { code: '507', transid: transid };
                return obj;
            } else if (lengthLimit && lengthLimit[property] && params[property].length != lengthLimit[property]) {
                return { code: '509', transid: transid };
            }
        }
        //验证签名
        const filterKeys = Object.keys(params).filter(value => { return filterParams.indexOf(value) > -1 });
        for (let i of filterKeys) {
            if (Unencrypt.indexOf(i) == -1) {
                if (params.hasOwnProperty(i)) {
                    parmstr += i + params[i];
                }
            }
        };
        let customerResult = await _getCustomerByAppkey(params['appkey']);
        if (customerResult['code'] == '200') {
            const customerObj = customerResult['customer'];
            const localSign = utils.md5(parmstr + customerObj['appsecret']);
            if (localSign === sign) {
                //验证IP
                if (checkIp) {
                    if ((customerObj.auth_ip && customerObj.auth_ip.indexOf(params['clientip']) > -1) || params['appkey'] === 'hVHGa1CO6bcvjDIg') {
                        return { code: '200', customerObj };
                    } else {
                        logUtils.iotlog.info(`checkParams apiMethod:${params['apiMethod']},transid:${transid},auth_ip:${customerObj.auth_ip},clientip:${params['clientip']}`);
                        const obj = { code: '514', transid: transid };
                        return obj;
                    }
                } else {
                    //接口无需验证IP
                    return customerResult;
                }
            } else {
                logUtils.iotlog.info(`checkParams apiMethod:${params['apiMethod']},transid:${transid},parmstr:${parmstr},sign:${sign}`);
                const obj = { code: '502', transid: transid };
                return obj;
            }
        } else {
            return customerResult;
        }
    } catch (error) {
        logUtils.iotlog.error(`checkApiParams error:`, error.stack);
        return { code: '511', transid: transid };
    }
};
/**
 * 请求记录是否在redis缓存
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
exports.cachedRecordExist = async function(params) {
    const msisdn = params['msisdn'],
        transid = params['transid'];
    try {
        let cardRecord = await redis.hgetallAsync(RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn);
        if (cardRecord) {
            cardRecord = cardRecord;
            return { code: '200', cardInfo: cardRecord };
        } else {
            return { code: '200', cardInfo: false };
        }
    } catch (error) {
        logUtils.iotlog.info(`cachedRecordExist error:`, error);
        return { code: '511', transid: transid };
    }
}
/**
 * 根据号卡匹配运营商
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
exports.query_memberInfo = async function(params) {
    const msisdn = params['msisdn'],
        transid = params['transid'];
    try {
        let cardInfo = await Card.findOne({
            where: { msisdn: msisdn }
        });
        if (cardInfo) {
            cardInfo = cardInfo.toJSON();
            const hashObj = utils.filterObjectNullProperty(cardInfo);
            await redis.hmset(RedisKeys.REDIS_KEY_PRE_CARD_KEY + msisdn, hashObj);
            return { code: '200', cardInfo };
        } else {
            const obj = { code: '501', transid: transid };
            return obj;
        }
    } catch (error) {
        logUtils.iotlog.info(`query_memberInfo error:`, error);
        return { code: '511', transid: transid };
    }
};

async function _getCustomerByAppkey(appkey) {
    try {
        let data = await redis.getAsync(RedisKeys.REDIS_KEY_PRE_CUSTOMER_APPKEY + appkey),
            customer;
        if (data) {
            customer = JSON.parse(data);
            return { code: '200', customer };
        } else {
            data = await Customer.findOne({ where: { appkey: appkey } });
            if (data) {
                customer = data.toJSON();
                const record = await redis.setAsync(RedisKeys.REDIS_KEY_PRE_CUSTOMER_APPKEY + customer['appkey'], JSON.stringify(customer));
                return { code: '200', customer };
            } else {
                return { code: '515', appkey: appkey };
            }
        }
    } catch (error) {
        logUtils.iotlog.error(`_getCustomerByAppkey error:`, error);
        return { code: '511' };
    }
}
/**
 * 创建/更改db记录值和redis记录值
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
exports.updateDBRecord = async function(params) {
    try {
        const dbName = params['dbName'],
            DB = db[dbName],
            updateAttributes = params['updateAttributes'],
            whereAttributes = params['whereAttributes'],
            rediskey = params['rediskey']

        const upsertdata = await DB.update(updateAttributes, { where: whereAttributes });
        console.log('updateAttributes', updateAttributes);
        const redisdata = await redis.hmset(rediskey, updateAttributes);
    } catch (error) {
        console.error(error);
    }
}
/**
 * 记录查询/操作 成功日志
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
exports.recordLogRecord = async function(params) {
    try {
        const rediskey = params['rediskey'],
            redisvalue = params['redisvalue'];
        console.log('rediskey', rediskey);
        console.log('redisvalue', redisvalue);
        const redisdata = await redis.rpushAsync(rediskey, JSON.stringify(redisvalue));
    } catch (error) {
        console.error(error);
    }
}