module.exports = function(sequelize, DataTypes) {
    const Apn = sequelize.define('apn', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        apn_name:{
            type: DataTypes.STRING(32),
            comment: 'apn名称'
        },
        apn_status:{
            type: DataTypes.INTEGER(3),
            comment: 'apn状态：1-关；2-开'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            Apn.belongsTo(models.card);
        }
    });
    return Apn;
};