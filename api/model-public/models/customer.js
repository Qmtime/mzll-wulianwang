module.exports = function(sequelize, DataTypes) {
    const Customer = sequelize.define('customer', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        username: { type: DataTypes.STRING(40), comment: '客户用户名' },
        password: { type: DataTypes.STRING(40), comment: '客户密码' },
        appkey: { type: DataTypes.STRING(40), unique: true, comment: '客户appkey' },
        appsecret: { type: DataTypes.STRING(40), comment: '客户appsecret' },
        email: { type: DataTypes.STRING(40), comment: '客户邮箱' },
        mobile: { type: DataTypes.STRING(40), comment: '客户电话' },
        compname: { type: DataTypes.STRING(40), comment: '客户名称' },
        address: { type: DataTypes.STRING(256), comment: '客户地址' },
        status: { type: DataTypes.INTEGER(3), defaultValue: 0, comment: "客户使用状态(0:使用中,1:已停用)" },
        notify_path: {
            type: DataTypes.STRING(256),
            comment: '回调地址'
        },
        auth_ip: {
            type: DataTypes.STRING(1510),
            comment: 'ip白名单列表'
        },
        balance: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '余额，单位分'
        },
        balanceWarn: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '余额预警值，单位分'

        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            Customer.hasMany(models.card);
            Customer.hasMany(models.productOrder);
            Customer.hasMany(models.setAlarm);
            Customer.hasMany(models.switchService);
            Customer.hasMany(models.changeLifeCycle);
            Customer.hasMany(models.customerProduct);
        }
    });
    return Customer;
};