module.exports = function(sequelize, DataTypes) {
    const SetAlarm = sequelize.define('setAlarm', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        appkey: {
            type: DataTypes.STRING(32),
            comment: 'appkey名称'
        },
        transid: {
            type: DataTypes.STRING(60),
            comment: '事务编码'
        },
        msisdns: {
            type: DataTypes.STRING(),
            comment: '号码列表'
        },
        attr_type: {
            type: DataTypes.INTEGER(3),
            comment: '阀值类型:1-绝对值；2-百分比'
        },
        attr_value: {
            type: DataTypes.STRING(32),
            comment: '阀值'
        },
        alarm_type: {
            type: DataTypes.INTEGER(3),
            comment: '告警类型:1-单卡流量告警；2-流量池告警；3-机卡分离告警；5-单卡套餐内使用量告警'
        },
        optTime: {
            type: DataTypes.DATE,
            comment: '操作时间'
        },
        finishTime: {
            type: DataTypes.DATE,
            comment: '完成时间'
        },
        order_status: {
            type: DataTypes.INTEGER(3),
            comment: '订单状态标识'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            SetAlarm.belongsTo(models.provider);
            SetAlarm.belongsTo(models.customer);
            SetAlarm.belongsTo(models.card);
        }
    });
    return SetAlarm;
};