module.exports = function(sequelize, DataTypes) {
    const Admin = sequelize.define('admin', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        username: { type: DataTypes.STRING(40), comment: '用户名' },
        password: { type: DataTypes.STRING(40), comment: '密码' },
        status: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            defaultValue: 0,
            comment: "账号状态(0:使用中,1:已停用)"
        },
        mobile: {
            type: DataTypes.STRING(15),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            Admin.hasMany(models.customerProduct);
            Admin.belongsTo(models.role);
        }
    });
    return Admin;
};