module.exports = function(sequelize, DataTypes) {
    const SwitchService = sequelize.define('switchService', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        appkey: {
            type: DataTypes.STRING(32),
            comment: 'appkey名称'
        },
        transid: {
            type: DataTypes.STRING(60),
            comment: '事务编码'
        },
        msisdns: {
            type: DataTypes.STRING(),
            comment: '号码列表'
        },
        optType: {
            type: DataTypes.INTEGER(3),
            comment: '操作类型：1X-GPRS;2X-短信;3X-语音;4X-APN;5-开关机，6-停复机 X:1-关；2-开'
        },
        optTime: {
            type: DataTypes.DATE,
            comment: '操作时间'
        },
        finishTime: {
            type: DataTypes.DATE,
            comment: '完成时间'
        },
        order_status: {
            type: DataTypes.INTEGER(3),
            comment: '订单状态标识'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            SwitchService.belongsTo(models.provider);
            SwitchService.belongsTo(models.customer);
            SwitchService.belongsTo(models.card);
        }
    });
    return SwitchService;
};