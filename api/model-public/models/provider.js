module.exports = function(sequelize, DataTypes) {
    const Provider = sequelize.define('provider', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        compname: { type: DataTypes.STRING(16), comment: '运营商名称' },
        code: { type: DataTypes.STRING(16), comment: '运营商编码', unique: true },
        location: { type: DataTypes.STRING(60), comment: '省市归属地' },
        vendor: { type: DataTypes.INTEGER(3), comment: '供应商：1:移动;2:联通;3:电信' },
        status: { type: DataTypes.INTEGER(3), comment: '0：使用中；1：已停用', defaultValue: 0 },
        balance: {
            type: DataTypes.FLOAT(20, 4),
            defaultValue: 0.0000,
            comment: '在上游余额'
        },
        balanceWarn: {
            type: DataTypes.FLOAT(20, 4),
            defaultValue: 0.0000,
            comment: '在上游余额预警值'
        },
    }, {
        freezeTableName: true,
        associate: function(models) {
            Provider.hasMany(models.product);
            Provider.hasMany(models.card);
            Provider.hasMany(models.productOrder);
            Provider.hasMany(models.setAlarm);
            Provider.hasMany(models.switchService);
            Provider.hasMany(models.changeLifeCycle);
        }
    });
    return Provider;
};