module.exports = function(sequelize, DataTypes) {
    const Card = sequelize.define('card', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        msisdn: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: '号码'
        },
        imsi: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: 'IMSI国际移动用户识别码'
        },
        iccid: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: 'ICCID集成电路卡识别码'
        },
        username: {
            type: DataTypes.STRING(30),
            comment: '用户名称'
        },
        location: {
            type: DataTypes.STRING(60),
            comment: '省市归属地'
        },
        vendor: {
            type: DataTypes.INTEGER(3),
            comment: '供应商：1:移动;2:联通;3:电信'
        },
        balance: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '余额，单位分'
        },
        open_time: {
            type: DataTypes.DATE,
            comment: '开户日期'
        },
        active_time: {
            type: DataTypes.DATE,
            comment: '激活日期'
        },
        grps_switch: {
            type: DataTypes.INTEGER(3),
            comment: "终端开关机状态：1-关；2-开"
        },
        sms_switch: {
            type: DataTypes.INTEGER(3),
            comment: "终端开关机状态：1-关；2-开"
        },
        voice_switch: {
            type: DataTypes.INTEGER(3),
            comment: "终端开关机状态：1-关；2-开"
        },
        apn_switch: {
            type: DataTypes.INTEGER(3),
            comment: "终端开关机状态：1-关；2-开"
        },
        terminal_status: {
            type: DataTypes.INTEGER(3),
            comment: "终端开关机状态：1-关；2-开"
        },
        switch_staus: {
            type: DataTypes.INTEGER(3),
            comment: "停复机状态：1-停机；2-复机"
        },
        gprs_status: {
            type: DataTypes.INTEGER(3),
            comment: "在线状态：1-关；2-开"
        },
        ip: {
            type: DataTypes.STRING(30),
            comment: "号卡IP地址"
        },
        lat: {
            type: DataTypes.STRING(30),
            comment: "纬度位置"
        },
        lon: {
            type: DataTypes.STRING(30),
            comment: "经度位置"
        },
        rat: {
            type: DataTypes.INTEGER(3),
            comment: "1表示3G;2表示2G;6表示4G及其他"
        },
        lifecycle_status: {
            type: DataTypes.INTEGER(3),
            comment: "生命周期：1-测试期;2-沉默期;3-库存期;4-正使用;5停机;6销户;7预约销户"
        },
        lifecycle_startTime: {
            type: DataTypes.DATE,
            comment: '生命周期日期'
        },
        lifecycle_endTime: {
            type: DataTypes.DATE,
            comment: '生命周期预计结束时间'
        },
        provider_code: {
            type: DataTypes.STRING(40),
            comment: '运营商编码'
        },
        groupsubid: {
            type: DataTypes.STRING(32),
            comment: '分组ID'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            Card.hasMany(models.productUsed);
            Card.hasMany(models.productOrder);
            Card.hasMany(models.setAlarm);
            Card.hasMany(models.switchService);
            Card.hasMany(models.changeLifeCycle);
            Card.hasMany(models.apn);
            Card.belongsTo(models.provider);
            Card.belongsTo(models.customer);
        }
    });
    return Card;
};