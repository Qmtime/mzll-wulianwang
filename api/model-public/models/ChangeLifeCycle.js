module.exports = function(sequelize, DataTypes) {
    const ChangeLifeCycle = sequelize.define('changeLifeCycle', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        appkey: {
            type: DataTypes.STRING(32),
            comment: 'appkey名称'
        },
        transid: {
            type: DataTypes.STRING(60),
            comment: '事务编码'
        },
        msisdns: {
            type: DataTypes.STRING(),
            comment: '号码列表'
        },
        optType: {
            type: DataTypes.INTEGER(3),
            comment: '操作类型：1-测试期到期处理；2-沉默期激活处理；3-库存期激活处理'
        },
        optTime: {
            type: DataTypes.DATE,
            comment: '操作时间'
        },
        finishTime: {
            type: DataTypes.DATE,
            comment: '完成时间'
        },
        order_status: {
            type: DataTypes.INTEGER(3),
            comment: '订单状态标识'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
            ChangeLifeCycle.belongsTo(models.provider);
            ChangeLifeCycle.belongsTo(models.customer);
            ChangeLifeCycle.belongsTo(models.card);
        }
    });
    return ChangeLifeCycle;
};