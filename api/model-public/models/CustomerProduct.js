function CustomerProduct(sequelize, DataTypes) {
    var CustomerProduct = sequelize.define('customerProduct', {
        'id': {
            primaryKey: true,
            type: DataTypes.INTEGER(20),
            allowNull: false,
            autoIncrement: true
        },
        'discount': {
            type: DataTypes.FLOAT(4, 4),
            comment: '客户折扣'
        },
        'status': {
            type: DataTypes.INTEGER(3),
            comment: '0:启用，1:停用',
            defaultValue: 0
        },

    }, {
        freezeTableName: true,
        associate: function(models) {
            CustomerProduct.belongsTo(models.customer);
            CustomerProduct.belongsTo(models.product);
            CustomerProduct.belongsTo(models.admin);
        }
    });
    return CustomerProduct;
}
module.exports = CustomerProduct;