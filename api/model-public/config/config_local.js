module.exports = {
    dbname: 'wulianwang', // 数据库名
    user: 'management', // 用户名
    passwd: 'QM-timemg', // 用户密码
    config: {
        'dialect': 'mysql', // 数据库使用mysql
        'host': 'rdslyxkt27rjjnogi1q4.mysql.rds.aliyuncs.com', // 数据库服务器ip
        'port': 3306, // 数据库服务器端口
        'define': {
            // 字段以下划线（_）来分割（默认是驼峰命名风格）
            'underscored': true
        },
        pool: {
            max: 1,
            min: 0,
            idle: 10000
        },
        timezone: '+08:00',
        logging: true,
        benchmark: true
    },
    mongo: {
        qmsg: 'localhost:27017/qmsg',
        qmsgLog: 'localhost:27017/qmsg-log'
    }

};