module.exports = {
    dbname: 'wulianwang', // 数据库名
    user: 'management', // 用户名
    passwd: 'QM-timemg', // 用户密码
    config: {
        'dialect': 'mysql', // 数据库使用mysql
        'host': 'rdslyxkt27rjjnogi1q4269.mysql.rds.aliyuncs.com', // 数据库服务器ip
        'port': 3306, // 数据库服务器端口
        'define': {
            // 字段以下划线（_）来分割（默认是驼峰命名风格）
            'underscored': true
        },
        pool: {
            max: 1,
            min: 0,
            idle: 10000
        },
        timezone: '+08:00',
        logging: true,
        benchmark: true
    },
    mongo: {
        qmsg: 'mongodb://root:QMtime_mongodb2016@dds-2ze7e2a02754db841530.mongodb.rds.aliyuncs.com:3717,dds-2ze7e2a02754db842445.mongodb.rds.aliyuncs.com:3717/qmsg?replicaSet=mgset-1764777',
        qmsgLog: 'mongodb://root:QMtime_mongodb2016@dds-2ze7e2a02754db841530.mongodb.rds.aliyuncs.com:3717,dds-2ze7e2a02754db842445.mongodb.rds.aliyuncs.com:3717/qmsg-log?replicaSet=mgset-1764777',
        qmsg_options: { server: { poolSize: 2 }, replset: { poolSize: 2 } },
        qmsgLog_options: { server: { poolSize: 3 }, replset: { poolSize: 3 } }
    }
};