module.exports = {
    dbname: 'wulianwang', // 数据库名
    config: {
        'dialect': 'mysql', // 数据库使用mysql
        'port': 3306, // 数据库服务器端口
        'define': {
            // 字段以下划线（_）来分割（默认是驼峰命名风格）
            'underscored': true
        },
        replication: {
            read: [
                { host: 'rr-2ze6s6q222q8480oo981.mysql.rds.aliyuncs.com', username: 'prod_mng', password: 'QM-timemg2016' }
            ],
            write: {
                'host': 'rdsko7x2f31ur3su9s6h888.mysql.rds.aliyuncs.com',
                username: 'prod_mng',
                password: 'QM-timemg2016'
            }
        },
        pool: {
            max: 30,
            min: 0,
            idle: 10000
        },
        timezone: '+08:00',
        logging: false
    },
    mongo: {
        qmsg: 'mongodb://root:QMtime_mongodb2016@dds-2ze7e2a02754db841530.mongodb.rds.aliyuncs.com:3717,dds-2ze7e2a02754db842445.mongodb.rds.aliyuncs.com:3717/qmsg?replicaSet=mgset-1764777',
        qmsgLog: 'mongodb://root:QMtime_mongodb2016@dds-2ze7e2a02754db841530.mongodb.rds.aliyuncs.com:3717,dds-2ze7e2a02754db842445.mongodb.rds.aliyuncs.com:3717/qmsg-log?replicaSet=mgset-1764777',
        qmsg_options: { server: { poolSize: 2 }, replset: { poolSize: 2 } },
        qmsgLog_options: { server: { poolSize: 3 }, replset: { poolSize: 3 } }
    }
};