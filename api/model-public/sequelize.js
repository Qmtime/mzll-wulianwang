var Sequelize = require('sequelize');

function init(env) {
    "use strict";
    var dbconfig = require('./config')(env);

    var dbname =dbconfig.dbname;
    var username = dbconfig.user;
    var passwd = dbconfig.passwd;
    var config = dbconfig.config;
    var sequelize = new Sequelize(
        dbname,
        username,
        passwd,
        config
    );
    dbconfig = null;
    process.setMaxListeners(0);
    return require('./_sync')(sequelize, Sequelize);
}

exports.init = init;