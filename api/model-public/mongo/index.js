var createCardOrder = require('./createCardOrder');
var createPackageOrder = require('./createPackageOrder');

var mongoose = require('mongoose');
var ObjectID = require('mongodb').ObjectID;
var async = require('async');
mongoose.Promise = require('bluebird');

var mongo;
module.exports = {
    init: function(env) {
        if (!!mongo) {
            return mongo;
        } else {
            var Schema = mongoose.Schema;
            var dbconfig = require('../config')(env);
            var qmsg = mongoose.createConnection(dbconfig['mongo']['qmsg'], dbconfig['mongo']['qmsg_options']);
            var qmsgLog = mongoose.createConnection(dbconfig['mongo']['qmsgLog'], dbconfig['mongo']['qmsg_options']);
            qmsg.on('error', function(err) {
                console.log('[mongoose] Error connecting to: ' + dbconfig['mongo']['qmsg'] + '. ' + err);
            });
            qmsg.once('open', function(err) {
                console.log('[mongoose] Successfully connected to: ' + dbconfig['mongo']['qmsg']);
            });
            qmsgLog.on('error', function(err) {
                console.log('[mongoose log] Error connecting to: ' + dbconfig['mongo']['qmsgLog'] + '. ' + err);
            });
            qmsgLog.once('open', function(err) {
                console.log('[mongoose log] Successfully connected to: ' + dbconfig['mongo']['qmsgLog']);
            });
            mongo = {
                // qmsg 中对应的集合
                createCardOrder: createCardOrder(qmsg, Schema),
                createPackageOrder: createPackageOrder(qmsg, Schema),
            };
            return mongo;
        }
    }
};