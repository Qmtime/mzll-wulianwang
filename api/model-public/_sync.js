const async = require('async');
const cluster = require('cluster');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');

function DBSyncer(sequelize,DataTypes) {
    "use strict";
    let instance;

    // 重新构造函数
    DBSyncer = function DBSyncer() {
        return instance;
    };

    // 后期处理原型属性
    DBSyncer.prototype = this;

    // 实例
    instance = new DBSyncer();

    // 重设构造函数指针
    instance.constructor = DBSyncer;

    // 单例中只允许运行一次处理
    if (!this.packager) {
        this.packager = {
            sequelize: sequelize
        };
    }
    this._init(sequelize,DataTypes);

    if (cluster.isMaster) {
        this._sync(sequelize);
    }
}

DBSyncer.prototype._init = function(sequelize,DataTypes) {
    "use strict";
    console.log(' DBSyncer - _init');
    var _self = this;
    fs.readdirSync(path.join(__dirname,'models'))
        .forEach(function (file) {
            const model = sequelize.import(path.join('./models', file));
            _self.packager[model.name] = model;
        });
    Object.keys(this.packager).forEach(function (modelName) {
        if (_self.packager[modelName].options.hasOwnProperty('associate')) {
            _self.packager[modelName].options.associate(_self.packager)
        }
    });

    _.extend({
        sequelize: sequelize,
        Sequelize: DataTypes
    }, _self.packager);
};

DBSyncer.prototype._sync = function(sequelize) {
    "use strict";
    console.log(' DBSyncer - _sync');
    // var pkger = this.packager;
    var _self = this;
    Object.keys(_self.packager).forEach(function (modelName) {
        if (_self.packager.hasOwnProperty(modelName)) {
            _self.packager[modelName].sync();
        }
    });
};

module.exports = function(sequelize,DataTypes) {
    "use strict";
    const dbSyncer = new DBSyncer(sequelize,DataTypes);
    return dbSyncer.packager;
};
