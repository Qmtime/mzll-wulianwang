const API_VERSION = 'v1';
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
const requestApi = require('request-promise');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const CardModel = db['Card'];
const CardOrderModel = db['CardOrder'];
const DeliveryModel = db['Delivery'];
const CustomerModel = db['Customer'];

class CARDORDER {
    static async cardorderlist(ctx) {
        try {
            //查询申请号码订单信息
            const params = ctx.request.body;
            const whereOptions = {};
            switch (params['order_status']) {
                case '1':
                    //申请中
                    whereOptions['order_status'] = { $in: [1, 10] };
                    break;
                case '2':
                    //申请成功
                    whereOptions['order_status'] = 4;
                    break;
                case '3':
                    //申请失败
                    whereOptions['order_status'] = { $in: [0, 5, 6] };
                    break;
                case '4':
                    //申请异常，待人工确认
                    whereOptions['order_status'] = 9;
                    break;
                case '5':
                    whereOptions['order_status'] = 2;
                    break;
                case '6':
                    whereOptions['order_status'] = 3;
                    break;
                default:
                    break;
            };

            if (params['searchInput'].trim()) {
                switch (params['selectSearch']) {
                    case '1':
                        whereOptions['customer_order_no'] = params['searchInput'].trim();
                        break;
                    case '2':
                        whereOptions['order_no'] = params['searchInput'].trim();
                        break;
                    case '3':
                        whereOptions['phone'] = params['searchInput'].trim();
                        break;
                    default:
                        break;
                };
            }
            params['delivery_label'] ? whereOptions['delivery_label'] = params['delivery_label'] : null;
            params['activated_state'] ? whereOptions['activated_state'] = params['activated_state'] : null;
            params['province'] ? whereOptions['province'] = { $in: params['province'] } : null;
            params['phoneNum'] ? whereOptions['phoneNum'] = params['phoneNum'] : null;
            params['request_time'] = { $gte: params['request_time'] };
            if (params['start_time'] && params['end_time']) {
                const startTime = moment(params['start_time']).format('YYYY-MM-DD HH:mm:ss'),
                    endTime = moment(params['end_time']).format('YYYY-MM-DD HH:mm:ss');
                whereOptions['request_time'] = { $gte: startTime, $lte: endTime };
            } else {
                whereOptions['request_time'] = { $gte: moment().startOf('day').format('YYYY-MM-DD HH:mm:ss'), $lte: moment().format('YYYY-MM-DD HH:mm:ss') };
            }

            const limit = parseInt(params.page_size),
                offset = params.page_size * (params.page_num - 1);

            const cardOrderOptions = {
                where: whereOptions,
                offset: offset,
                limit: limit,
                include: [{ model: DeliveryModel }, { model: CardModel }],
                order: [
                    ['request_time', 'DESC']
                ]
            };
            let cardorders = await CardOrderModel.findAndCountAll(cardOrderOptions);

            let count = cardorders.count,
                list = cardorders.rows;
            if (list.length > 0) {
                list = list.map(async value => {
                    value = value.toJSON();
                    value['request_time'] = moment(value['request_time']).format("YYYY-MM-DD HH:mm:ss");
                    if (value['order_status'] == 4) {
                        value['activated_state'] = commonConfig.activatedState[value['activated_state']] || '未激活';
                    } else {
                        value['activated_state'] = null;
                    }
                    value['order_status'] = commonConfig.cardOrdStatus[value['order_status']] || '审核中';
                    value['delivery_label'] = commonConfig.deliveryLabel[value['delivery_label']];
                    value['provider_discount'] = Number(value['provider_discount']).toFixed(2);
                    value['customer_discount'] = Number(value['customer_discount']).toFixed(2);
                    if (value['delivery']) {
                        value['delivery'] = value['delivery']['delivery_info'];
                    }
                    //查询申请套餐信息
                    const cardOptions = { where: { id: value['card_id'] } };
                    const cardInfo = await CardModel.findOne(cardOptions);
                    if (cardInfo) {
                        value['product_name'] = cardInfo['product_name'];
                    }
                    return value;
                });
                ctx.body = {
                    code: RCODE['00000'].code,
                    codeDes: RCODE['00000'].codeDes,
                    data: { count: count, list: await Promise.all(list) }
                }
            } else {
                ctx.body = {
                    code: RCODE['00000'].code,
                    codeDes: RCODE['00000'].codeDes,
                    data: { count: count, list: await Promise.all(list) }
                }
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('cardorderlist', err));
            ctx.throw(err);
        }
    }
    static async queryCardDelivery(ctx) {
        try {
            const params = ctx.request.body;
            const customer_order_no = params['customer_order_no'];
            const obj = {
                customer_order_no: customer_order_no,
                appkey: commonConfig.api.appkey
            };
            obj['sign'] = util.md5(util.sortObj(obj) + commonConfig.api.appsecret);
            const options = {
                url: commonConfig.api.deliveryUrl,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(obj)
            };
            console.log('options',options);
            let deliverObj = await requestApi(options);
            deliverObj = JSON.parse(deliverObj);
            const deliverInfo = deliverObj['data'];
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: deliverInfo
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('queryCardDelivery', err));
            ctx.throw(err);
        }
    }
    static async cardshowTotal(ctx) {
        try {
            const params = ctx.request.body;
            const whereOptions = {};

            if (params['start_time'] && params['end_time']) {
                const startTime = moment(params['start_time']).format('YYYY-MM-DD HH:mm:ss'),
                    endTime = moment(params['end_time']).format('YYYY-MM-DD HH:mm:ss');
                whereOptions['request_time'] = { $gte: startTime, $lte: endTime };
            } else {
                whereOptions['request_time'] = { $gte: moment().startOf('day').format('YYYY-MM-DD HH:mm:ss'), $lte: moment().format('YYYY-MM-DD HH:mm:ss') };
            }
            const cardOrderOptions = {
                where: whereOptions,
                include: [{ model: DeliveryModel }]
            };
            let cardorders = await CardOrderModel.findAndCountAll(cardOrderOptions);

            let count = cardorders.count,
                list = cardorders.rows,
                applySuccess = list.filter(value => value.order_status == '4'),
                applyFailure = list.filter(value => value.order_status == '0' || value.order_status == '5' || value.order_status == '6'),
                ctivateSuccess = list.filter(value => value.activated_state == '2'), //1:未激活;2:已激活
                ctivateFailure = list.filter(value => value.activated_state == '1'),
                sales = applySuccess.length > 0 ? applySuccess.map(value => { return Number(value['customer_fee']) }).reduce((accumulator, currentValue) => { return accumulator + currentValue }).toFixed(3) : 0,
                costs = applySuccess.length > 0 ? applySuccess.map(value => { return Number(value['provider_fee']) }).reduce((accumulator, currentValue) => { return accumulator + currentValue }).toFixed(3) : 0,
                profits = (sales - costs).toFixed(2),
                profitRation = costs != 0 ? (profits / costs * 100).toFixed(2) : 0;
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: {
                    data: count,
                    applySuccess: applySuccess.length,
                    applyFailure: applyFailure.length,
                    ctivateSuccess: ctivateSuccess.length,
                    ctivateFailure: ctivateFailure.length,
                    sales: sales,
                    costs: costs,
                    profits: profits,
                    profitRation: profitRation
                }
            }
        } catch (err) {
            console.error(err);
            errorlog.error(util.formatErrorLog('cardshowTotal', err));
            ctx.throw(err);
        }
    }
    static async pushToCustomer(ctx) {
        try {
            const params = ctx.request.body;
            const customer_order_no = params['customer_order_no'];
            //查找订单信息
            const cardOrderOptions = {
                where: { customer_order_no: customer_order_no },
                include: [{ model: CustomerModel }]
            };
            const orderObj = await CardOrderModel.findOne(cardOrderOptions);
            const custoemrInfo = orderObj['customer'],
                callback_path = custoemrInfo['callback_path'],
                appsecret = custoemrInfo['appsecret'],
                code = commonConfig.status_code.cardOrder[orderObj['order_status']],
                info = commonConfig.cardOrdStatus[orderObj['order_status']];

            const callbackObj = {
                customer_order_no: customer_order_no,
                order_no: orderObj['order_no'],
                code: code,
                delivery_label: orderObj['delivery_label'],
                info: info
            };
            callbackObj['sign'] = util.md5(util.sortObj(callbackObj) + appsecret);
            let options = {
                url: callback_path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(callbackObj)
            }
            let responseInfo = await requestApi(options);
            serverlog.info(`pushToCustomer,options:${options},result:${responseInfo}`);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
            }
            // if (responseInfo == 1) {
            //     ctx.body = {
            //         code: RCODE['00000'].code,
            //         codeDes: RCODE['00000'].codeDes,
            //     }
            // } else {
            //     ctx.body = {
            //         code: RCODE['00001'].code,
            //         codeDes: RCODE['00001'].codeDes,
            //     }
            // }
        } catch (err) {
            errorlog.error(util.formatErrorLog('pushToCustomer', err));
            ctx.throw(err);
        }
    }
}
module.exports = CARDORDER;