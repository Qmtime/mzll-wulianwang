const API_VERSION = 'v1';
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录
const util = require('../../lib/util');

const db = require('../../models');
const Admin = db['Admin'];
const Customer = db['Customer'];
const Role = db['Role'];


class ADMIN {
    static async findAdminByName(name) {
        try {
            const result = await Admin.findOne({ where: { username: name },include:[{ attributes: ['authorities'], model: Role }] });
            if (result) {
                return result;
            } else {
                return null;
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('findAdminByName', err));
            this.throwError(err.status, err.message, err.code);
        }
    }

    static throwError(status,message,code,err){
        let error = err ? err : new Error(message);
        error.status = status || 500;
        error.code = code || '50000';
        throw error;
    }
}
module.exports = ADMIN;