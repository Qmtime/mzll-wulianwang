const API_VERSION = 'v1';
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
const _ = require('lodash');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const ProviderModel = db['Provider'];
const ProductModel = db['Product'];
const CustomerProductModel = db['CustomerProduct'];

class PRODUCT {
    static async getProducts(ctx) {
        try {
            const params = ctx.request.body;
            const offset = params['offset'],
                limit = params['limit'],
                provider = params['provider'],
                province = params['province'],
                status = params['status'],
                searchInput = params['searchInput'] ? params['searchInput'].trim() : '',
                selectSearch = params['selectSearch'];

            const whereOptions = {},
                includeWhereOptions = {};

            if (searchInput) {
                switch (selectSearch) {
                    case '1':
                        whereOptions['product_code'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    case '2':
                        whereOptions['product_name'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    default:
                        break;
                };
            }
            if (province && province.length > 0) {
                whereOptions['province'] = { $in: province };
            }
            if (status) {
                whereOptions['status'] = status;
            }
            if (provider) {
                includeWhereOptions['id'] = provider;
            }

            const productOptions = {
                where: whereOptions,
                include: [{ model: ProviderModel, where: includeWhereOptions }]
            };
            if (offset) {
                productOptions['offset'] = offset;
            }
            if (limit) {
                productOptions['limit'] = limit;
            }
            let result = await ProductModel.findAndCountAll(productOptions);
            let count = result.count,
                list = result.rows;
            if (list.length > 0) {
                list = list.map(value => {
                    value = value.toJSON();
                    const productObj = Object.assign({}, value);
                    productObj['provider_compname'] = value['provider'] ? value['provider']['compname'] : '';
                    productObj['status'] = commonConfig.statusCode[value['status']];
                    productObj['product_time_label'] = value['product_time'];
                    productObj['product_time'] = commonConfig.productTime[value['product_time']];
                    productObj['effective_type_label'] = value['effective_type'];
                    productObj['effective_type'] = commonConfig.effectiveType[value['effective_type']];
                    productObj['product_type_label'] = value['product_type'];
                    productObj['product_type'] = commonConfig.productType[value['product_type']];
                    return productObj;
                });
            }

            let productNameList = [],
                productTimeList = [],
                effectiveTypeList = [],
                productTypeList = [];

            list.forEach(value => {
                //产品名称
                let productNameObj = {};
                productNameObj['value'] = value['product_name'];
                productNameObj['label'] = value['product_name'];
                productNameList.push(productNameObj);
                //时效
                const productTimeObj = {};
                productTimeObj['value'] = value['product_time_label'];
                productTimeObj['label'] = commonConfig.productTime[value['product_time_label']];
                productTimeList.push(productTimeObj);
                //生效类型
                const effectiveTypeObj = {};
                effectiveTypeObj['value'] = value['effective_type'];
                effectiveTypeObj['label'] = commonConfig.effectiveType[value['effective_type_label']];
                effectiveTypeList.push(effectiveTypeObj);
                //套餐包类型
                const productTypeObj = {};
                productTypeObj['value'] = value['product_type'];
                productTypeObj['label'] = commonConfig.productType[value['product_type_label']];
                productTypeList.push(productTypeObj);

            });
            productNameList = _.uniqBy(productNameList, 'value');
            productTypeList = _.uniqBy(productTypeList, 'value');
            productTimeList = _.uniqBy(productTimeList, 'value');
            effectiveTypeList = _.uniqBy(effectiveTypeList, 'value');
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: { count: count, list: list, productNameList, productTimeList, effectiveTypeList, productTypeList }
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('getproducts', err));
            ctx.throw(err);
        }
    }
    static async addProduct(ctx) {
        try {
            const params = ctx.request.body;

            const product_name = params['product_name'],
                product_code = params['product_code'],
                product_description = params['product_description'],
                product_time = params['product_time'],
                effective_type = params['effective_type'],
                provider = params['provider'],
                province = params['province'],
                vendor = params['vendor'],
                price = params['price'],
                provider_discount = params['provider_discount'],
                agent_provider_discount = params['agent_provider_discount'],
                provider_code = params['provider_code'];

            const options = {
                product_name: product_name,
                product_code: product_code,
                product_description: product_description,
                product_time: product_time,
                effective_type: effective_type,
                province: province,
                vendor: vendor,
                price: price,
                provider_discount: provider_discount,
                agent_provider_discount: agent_provider_discount,
                provider_id: provider,
                provider_code: provider_code
            }
            const result = await ProductModel.create(options);

            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: result[0]
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('addCard', err));
            ctx.throw(err);
        }
    }
    static async editProduct(ctx) {
        try {
            const params = ctx.request.body;
            const product_name = params['product_name'],
                status = params['status'],
                product_description = params['product_description'],
                product_time = params['product_time'],
                effective_type = params['effective_type'],
                price = params['price'],
                provider_discount = params['provider_discount'],
                agent_provider_discount = params['agent_provider_discount'];

            let options = {
                id: params['id'],
                product_name: product_name,
                product_description: product_description,
                product_time: product_time,
                effective_type: effective_type,
                status: status,
                price: price,
                provider_discount: provider_discount,
                agent_provider_discount: agent_provider_discount
            };
            switch (status) {
                case 'true':
                    options['status'] = 0;
                    break;
                case 'false':
                    options['status'] = 1;
                    break;
                default:
                    break;
            }
            const result = await ProductModel.upsert(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('editProduct', err));
            ctx.throw(err);
        }
    }
    static async dispatchProduct(ctx) {
        try {
            const params = ctx.request.body;
            const options = {
                product_id: params['product_id'],
                customer_id: params['customer_id'],
                admin_id: params['admin_id'],
                discount: params['discount']
            }
            const result = await CustomerProductModel.upsert(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('dispatchProduct', err));
            ctx.throw(err);
        }
    }
}
module.exports = PRODUCT;