const API_VERSION = 'v1';
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const CardModel = db['Card'];
const ProviderModel = db['Provider'];
const CustomerCardModel = db['CustomerCard'];

class CARD {
    static async getCards(ctx) {
        try {
            const params = ctx.request.body;
            const offset = params['offset'],
                limit = params['limit'],
                provider = params['provider'],
                province = params['province'],
                status = params['status'],
                searchInput = params['searchInput'].trim(),
                selectSearch = params['selectSearch'];

            const whereOptions = {},
                includeWhereOptions = {};

            if (searchInput) {
                switch (selectSearch) {
                    case '1':
                        whereOptions['product_code'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    case '2':
                        whereOptions['product_name'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    default:
                        break;
                };
            }
            if (province && province.length > 0) {
                whereOptions['province'] = { $in: province };
            }
            if (status) {
                whereOptions['status'] = status;
            }
            if (provider) {
                includeWhereOptions['id'] = provider;
            }
            const options = {
                where: whereOptions,
                offset: offset,
                limit: limit,
                include: [{ model: ProviderModel, where: includeWhereOptions }]
            };
            const result = await CardModel.findAndCountAll(options);
            let count = result.count,
                list = result.rows;
            if (list.length > 0) {
                list = list.map(value => {
                    value = value.toJSON();
                    value['provider_compname'] = value['provider'] ? value['provider']['compname'] : '';
                    value['status'] = commonConfig.statusCode[value['status']];
                    return value;
                });
            }
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: { count: count, list: list }
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('getCustomers', err));
            ctx.throw(err);
        }
    }
    static async editCard(ctx) {
        try {
            const params = ctx.request.body;

            const product_name = params['product_name'],
                status = params['status'],
                product_description = params['product_description'],
                price = params['price'],
                provider_discount = params['provider_discount'],
                agent_provider_discount = params['agent_provider_discount'];

            let options = {
                id: params['id'],
                product_name: product_name,
                product_description: product_description,
                price: price,
                provider_discount: provider_discount,
                agent_provider_discount: agent_provider_discount
            };
            switch (status) {
                case 'true':
                    options['status'] = 0;
                    break;
                case 'false':
                    options['status'] = 1;
                    break;
                default:
                    break;
            }
            const result = await CardModel.upsert(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('editCard', err));
            ctx.throw(err);
        }
    }
    static async addCard(ctx) {
        try {
            const params = ctx.request.body;

            const product_name = params['product_name'],
                product_code = params['product_code'],
                product_description = params['product_description'],
                provider = params['provider'],
                province = params['province'],
                vendor = params['vendor'],
                price = params['price'],
                provider_discount = params['provider_discount'],
                agent_provider_discount = params['agent_provider_discount'],
                provider_list = params['provider_list'],
                provider_code = params['provider_code'];

            const options = {
                product_name: product_name,
                product_code: product_code,
                product_description: product_description,
                provider_code: provider_code,
                province: province,
                vendor: vendor,
                price: price,
                provider_discount: provider_discount,
                agent_provider_discount: agent_provider_discount,
                provider_id: provider
            }
            const result = await CardModel.create(options);

            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: result[0]
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('addCard', err));
            ctx.throw(err);
        }
    }
    static async dispatchCard(ctx) {
        try {
            const params = ctx.request.body;
            const options = {
                card_id: params['card_id'],
                customer_id: params['customer_id'],
                admin_id: params['admin_id'],
                discount: params['discount']
            }

            const result = await CustomerCardModel.upsert(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('dispatchCard', err));
            ctx.throw(err);
        }
    }
}
module.exports = CARD;