const commonConfig = require('../../config');
const API_VERSION = commonConfig['API_VERSION'];
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const Admin = db['Admin'];
const Customer = db['Customer'];
const Role = db['Role'];

const SIGN_KEY = commonConfig['SIGN_KEY'];

const redis = require('../../redis');
const REDIS_KEY = require('../../config/redisKey.config');
redis.select(2);

class Auth {

    /**models
     * @api {get} /auth 获取Token
     * @apiName   GetAuth
     * @apiGroup  1.1 鉴权
     *
     * @apiDescription 获取后续API请求的JWT token(身份验证令牌)
     *
     *   Token是拇指平台接口唯一调用凭据,所有接口调用都需要Token,Token保存至少保留512个字符空间,有效期为2小时,需定时获取刷新.
     *
     * @apiParam   {String{18}} appid           注册应用时由拇指平台分发的ID.
     * @apiParam   {String{32}} appsecret       用户唯一凭证密钥.
     * @apiHeader  [Accept=application/json]    application/json, application/xml, text/yaml, text/plain.
     * @apiSuccess token                        接口凭证
     * @apiSuccess expires_in                   凭证有效时间，单位：秒
     */
    static async getAuth(ctx) {
        const params = ctx.request.body;

        if (!params.username || !params.password) {
            ctx.throw(200, '', { code: '00003' });
        }
        try {
            let AdminObj = await Admin.findOne({
                where: { 'username': params.username },
                include: [{ attributes: ['authorities'], model: Role }]
            });
            let CustomerObj = await Customer.findOne({ where: { username: params.username } });
            if (!AdminObj && !CustomerObj) {
                ctx.throw(200, '', { code: '00006' });
            }
            const UserInfo = AdminObj ? AdminObj.toJSON() : CustomerObj.toJSON();

            const match = util.md5(UserInfo['password']) === params.password;
            if (!match) ctx.throw(200, '', { code: '00007' });
            const payload = {
                id: UserInfo.id,
                name: UserInfo.username
            };
            const token = jwt.sign(payload, SIGN_KEY, { expiresIn: '24h' });

            const dataObj = {
                id: UserInfo.id,
                username: UserInfo['username'],
                status: UserInfo['status'],
                token: token
            };
            ctx.cookies.set('token', token, { httpOnly: true, expires: new Date(moment().add(1, 'h')) });
            ctx.body = { code: RCODE['00000'].code, codeDes: RCODE['00000'].codeDes, data: dataObj };
        } catch (e) {
            errorlog.error(util.formatErrorLog({ model: 'getAuth', body: params }, e));
            ctx.throw(e);
        }
    }
}

module.exports = Auth;