const API_VERSION = 'v1';
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const CustomerModel = db['Customer'];

class CUSTOMER {
    static async findCustomerByName(name) {
        try {
            const result = await CustomerModel.findOne({ where: { username: name } });
            if (result) {
                return result;
            } else {
                return null;
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('findCustomerById', err));
            this.throwError(err.status, err.message, err.code);
        }
    }

    static throwError(status, message, code, err) {
        let error = err ? err : new Error(message);
        error.status = status || 500;
        error.code = code || '50000';
        throw error;
    }

    static async getCustomers(ctx) {
        try {
            const params = ctx.request.body;
            console.log('params', params);
            const offset = params['offset'],
                limit = params['limit'],
                status = params['status'],
                searchInput = params['searchInput'],
                selectSearch = params['selectSearch'];

            const whereOptions = {};
            if (searchInput) {
                switch (selectSearch) {
                    case '1':
                        whereOptions['compname'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    case '2':
                        whereOptions['username'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    case '3':
                        whereOptions['appkey'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    default:
                        break;
                }
            }
            if (status) {
                whereOptions['status'] = status;
            }
            console.log('whereOptions', whereOptions);
            const options = {
                where: whereOptions,
                attributes: { exclude: ['appsecret', 'password'] },
                offset: offset,
                limit: limit
            };
            const result = await CustomerModel.findAndCountAll(options);
            let count = result.count,
                list = result.rows;
            if (list.length > 0) {
                list = list.map(value => {
                    value = value.toJSON();
                    value['status'] = commonConfig.statusCode[value['status']];
                    return value;
                });
            }
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: { count: count, list: list }
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('getCustomers', err));
            ctx.throw(err);
        }
    }
    static async addCustomer(ctx) {
        try {
            const params = ctx.request.body;
            console.log('params', params);

            const compname = params['compname'],
                username = params['username'],
                password = params['password'],
                appkey = params['appkey'],
                email = params['email'],
                mobile = params['mobile'],
                address = params['address'],
                status = params['status'],
                callback_path = params['callback_path'],
                notify_path = params['notify_path'],
                auth_ip = params['auth_ip'];

            const options = {
                compname: compname,
                username: username,
                password: password,
                appkey: appkey,
                email: email,
                mobile: mobile,
                address: address,
                status: status,
                callback_path: callback_path,
                notify_path: notify_path,
                auth_ip: auth_ip
            }
            options['appkey'] = util.random(16);
            options['appsecret'] = util.random(32);
            const result = await CustomerModel.create(options);

            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: result[0]
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('addCustomer', err));
            ctx.throw(err);
        }
    }
    static async editCustomer(ctx) {
        try {
            const params = ctx.request.body;
            let options = {
                id: params['id'],
                compname: params['compname'],
                username: params['username'],
                appkey: params['appkey'],
                email: params['email'],
                mobile: params['mobile'],
                address: params['address'],
                callback_path: params['callback_path'],
                notify_path: params['notify_path'],
                auth_ip: params['auth_ip']
            };
            switch (params['status']) {
                case 'true':
                    options['status'] = 0;
                    break;
                case 'false':
                    options['status'] = 1;
                    break;
                default:
                    break;
            }
            const result = await CustomerModel.upsert(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('editCustomer', err));
            ctx.throw(err);
        }
    }
}
module.exports = CUSTOMER;