const API_VERSION = 'v1';
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const OrderModel = db['Order'];
const productModel = db['Product'];

class parckageOrder {
    static async orderlist(ctx) {
        try {
            //查询订单信息
            const params = ctx.request.body;
            const whereOptions = {};
            params['customer'] ? whereOptions['customer_id'] = { $in: params['customer'] } : null;
            params['province'] ? whereOptions['province'] = { $in: params['province'] } : null;
            params['phone'] ? whereOptions['phone'] = params['phone'] : null;
            params['request_time'] = { $gte: params['request_time'] };

            const limit = parseInt(params.page_size),
                offset = params.page_size * (params.page_num - 1);

            if (params['start_time'] && params['end_time']) {
                const startTime = moment(params['start_time']).format('YYYY-MM-DD HH:mm:ss'),
                    endTime = moment(params['end_time']).format('YYYY-MM-DD HH:mm:ss');
                whereOptions['request_time'] = { $gte: startTime, $lte: endTime };
            } else {
                whereOptions['request_time'] = { $gte: moment().startOf('day').format('YYYY-MM-DD HH:mm:ss'), $lte: moment().format('YYYY-MM-DD HH:mm:ss') };
            }
            switch (params['order_status']) {
                case '1':
                    whereOptions['order_status'] = 1;
                    break;
                case '2':
                    whereOptions['order_status'] = 4;
                    break;
                case '3':
                    whereOptions['order_status'] = { $in: [0, 5, 6] };
                    break;
                case '4':
                    whereOptions['order_status'] = 9;
                    break;
                case '5':
                    whereOptions['order_status'] = 2;
                    break;
                case '6':
                    whereOptions['order_status'] = 3;
                    break;
                default:
                    break;
            }
            if (params['searchInput'].trim()) {
                switch (params['selectSearch']) {
                    case '1':
                        whereOptions['customer_order_no'] = params['searchInput'].trim();
                        break;
                    case '2':
                        whereOptions['order_no'] = params['searchInput'].trim();
                        break;
                    case '3':
                        whereOptions['phone'] = params['searchInput'].trim();
                        break;
                    default:
                        break;
                };
            }
            const includeOptions = {};
            params['product_name'] ? includeOptions['product_name'] = { $in: params['product_name'] } : null;
            params['product_time'] ? includeOptions['product_time'] = params['product_time'] : null;
            params['effective_type'] ? includeOptions['effective_type'] = params['effective_type'] : null;
            params['product_type'] ? includeOptions['product_type'] = params['product_type'] : null;

            const orderOptions = {
                attributes: { exclude: ['vendor_order_no', 'msg', 'callback_flag', 'appkey', 'created_at', 'updated_at', 'agent_provider_id'] },
                where: whereOptions,
                offset: offset,
                limit: limit,
                include: [{ model: productModel, where: includeOptions }],
                order: [
                    ['request_time', 'DESC']
                ]
            };
            let orders = await OrderModel.findAndCountAll(orderOptions);
            let count = orders.count,
                list = orders.rows;
            list = list.map(async value => {
                value = value.toJSON();
                value['request_time'] = moment(value['request_time']).format("YYYY-MM-DD HH:mm:ss");
                value['order_status'] = commonConfig.packageOrdStatus[value['order_status']];

                const productInfo = value['product'];
                if (productInfo) {
                    value['product_time'] = commonConfig.productTime[productInfo['product_time']];
                    value['effective_type'] = commonConfig.effectiveType[productInfo['effective_type']];
                    value['product_type'] = commonConfig.productType[productInfo['product_type']];
                }
                return value;
            });
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: { count: count, list: await Promise.all(list) }
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('orderlist', err));
            ctx.throw(err);
        }
    }
    static async ordershowTotal(ctx) {
        try {
            const params = ctx.request.body;
            const whereOptions = {};

            if (params['start_time'] && params['end_time']) {
                const startTime = moment(params['start_time']).format('YYYY-MM-DD HH:mm:ss'),
                    endTime = moment(params['end_time']).format('YYYY-MM-DD HH:mm:ss');
                whereOptions['request_time'] = { $gte: startTime, $lte: endTime };
            } else {
                whereOptions['request_time'] = { $gte: moment().startOf('day').format('YYYY-MM-DD HH:mm:ss'), $lte: moment().format('YYYY-MM-DD HH:mm:ss') };
            }
            const orderOptions = {
                where: whereOptions,
                include: [{ model: productModel }]
            };
            let orders = await OrderModel.findAndCountAll(orderOptions);

            let count = orders.count,
                list = orders.rows,
                success = list.filter(list => list.order_status == '4'),
                failed = list.filter(list => list.order_status == '0' || list.order_status == '5' || list.order_status == '6'),
                inCharge = list.filter(list => list.order_status == '1'),
                sales = success.length > 0 ? success.map(value => { return Number(value['customer_fee']) }).reduce((accumulator, currentValue) => { return accumulator + currentValue }).toFixed(3) : 0,
                costs = success.length > 0 ? success.map(value => { return Number(value['provider_fee']) }).reduce((accumulator, currentValue) => { return accumulator + currentValue }).toFixed(3) : 0,
                profits = (sales - costs).toFixed(2),
                profitRation = costs != 0 ? (profits / costs * 100).toFixed(2) : 0;
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: {
                    total: count,
                    success: success.length,
                    failed: failed.length,
                    inCharge: inCharge.length,
                    sales: sales,
                    costs: costs,
                    profits: profits,
                    profitRation: profitRation
                }
            }
        } catch (err) {
            console.error(err);
            errorlog.error(util.formatErrorLog('ordershowTotal', err));
            ctx.throw(err);
        }
    }
}
module.exports = parckageOrder;