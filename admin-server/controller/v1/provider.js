const API_VERSION = 'v1';
const moment = require('moment');
const commonConfig = require('../../config');
const RCODE = require(`../../config/${API_VERSION}/response.code`);
const util = require('../../lib/util');
const requestApi = require('request-promise');
import { serverlog, errorlog } from '../../lib/logger.lib'; //日记记录

const db = require('../../models');
const providerModel = db[''];

const CardModel = db['Card'];
const CardOrderModel = db['CardOrder'];
const DeliveryModel = db['Delivery'];
const ProviderModel = db['Provider'];

class PROVIDER {
    static async getProviders(ctx) {
        try {
            const params = ctx.request.body;
            const offset = params['offset'],
                limit = params['limit'],
                status = params['status'],
                province = params['province'],
                searchInput = params['searchInput'],
                selectSearch = params['selectSearch'];

            const whereOptions = {};
            if (searchInput) {
                switch (selectSearch) {
                    case '1':
                        whereOptions['compname'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    case '2':
                        whereOptions['code'] = { $or: [{ $like: searchInput + '%' }, { $like: '%' + searchInput }, { $like: '%' + searchInput + '%' }] };
                        break;
                    default:
                        break;
                }
            }
            if (status) {
                whereOptions['status'] = status;
            }
            if (province) {
                whereOptions['province'] = province;
            }
            const options = {
                where: whereOptions,
                offset: offset,
                limit: limit
            };
            const result = await ProviderModel.findAndCountAll(options);
            let count = result.count,
                list = result.rows;
            if (list.length > 0) {
                list = list.map(value => {
                    value = value.toJSON();
                    value['vendor'] = commonConfig.vendorCode[value['vendor']];
                    value['status'] = commonConfig.statusCode[value['status']];
                    return value;
                });
            }
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: { count: count, list: list }
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('getProviders', err));
            ctx.throw(err);
        }
    }
    static async addProvider(ctx) {
        try {
            const params = ctx.request.body;
            const compname = params['compname'],
                code = params['code'],
                province = params['province'],
                vendor = params['vendor'];
            const options = {
                where: {
                    compname: compname,
                    code: code,
                    province: province,
                    vendor: vendor
                }
            }
            const result = await ProviderModel.findOrCreate(options);
            ctx.body = {
                code: RCODE['00000'].code,
                codeDes: RCODE['00000'].codeDes,
                data: result[0]
            };
        } catch (err) {
            errorlog.error(util.formatErrorLog('addProvider', err));
            ctx.throw(err);
        }
    }
    static async editProvider(ctx) {
        try {
            const params = ctx.request.body;
            let compname = params['compname'],
                code = params['code'],
                status = params['status'],
                province = params['province'],
                id = params['id'];
            
            if (compname && code && status) {
                switch (status) {
                    case 'true':
                        status = 0;
                        break;
                    case 'false':
                        status = 1;
                        break;
                    default:
                        break;
                }

                const result = await ProviderModel.upsert({ compname: compname, status: status, code: code, province: province}, { where: { id: id } });
                ctx.body = {
                    code: RCODE['00000'].code,
                    codeDes: RCODE['00000'].codeDes
                };
            } else {
                ctx.body = {
                    code: RCODE['00002'].code,
                    codeDes: RCODE['00002'].codeDes
                };
            }
        } catch (err) {
            errorlog.error(util.formatErrorLog('editProvider', err));
            ctx.throw(err);
        }
    }
}
module.exports = PROVIDER;