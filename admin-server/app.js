/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* API app - RESTful API for API interface and/or ajax functions.                                 */
/*                                                                                                */
/* The API provides GET / POST / PATCH / DELETE methods on a variety of resources.                */
/*                                                                                                */
/* 2xx responses honour the request Accept type (json/xml/yaml/text) for the response body;       */
/* 4xx/5xx responses provide a simple text message in the body.                                   */
/*                                                                                                */
/* A GET on a collection which returns no results returns a 204 / No Content response.            */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

'use strict';
const commonConfig = require(`./config`);
const API_VERSION = commonConfig['API_VERSION'];
const Koa = require('koa'); // Koa framework
const cors = require('koa2-cors');
const body = require('koa-body'); // body parser
const compress = require('koa-compress'); // HTTP compression
const session = require('koa-session'); // session for flash messages
const debug = require('debug')('app'); // small debugging utility
const jwt = require('jsonwebtoken'); // JSON Web Token implementation
const xmlify = require('xmlify'); // JS object to XML
const yaml = require('js-yaml'); // JS object to YAML
const _trim = require('lodash').trim;
const moment = require('moment');
const utils = require('./lib/util');
const RCODE = require(`./config/${API_VERSION}/response.code`);
const Admin = require(`./controller/${API_VERSION}/admin`);
const Customer = require(`./controller/${API_VERSION}/customer`);

const SIGN_KEY = commonConfig['SIGN_KEY'];
import { serverlog, errorlog } from './lib/logger.lib'; //日记记录

require('dotenv').config(); // loads environment variables from .env file (if available - eg dev env)

const app = new Koa(); // API app


app.use(cors({
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'x-access-token'],
}));

/* set up middleware which will be applied to each request - - - - - - - - - - - - - - - - - - -  */

app.use(async(ctx, next) => {
    //响应开始时间
    const start = new Date();
    //响应间隔时间
    let ms;
    try {
        //开始进入到下一个中间件
        await next();
        ms = new Date() - start;
        ctx.set('X-Response-Time', ms + 'ms');
        //记录响应日志
        serverlog.info(utils.formatResLog(ctx, ms));
    } catch (error) {
        ms = new Date() - start;
        //记录异常日志
        errorlog.error(utils.formatErrorLog('access', error, true, ctx, ms));
    }
});



// HTTP compression
app.use(compress({}));

// parse request body into ctx.request.body
app.use(body());

// session for flash messages (uses signed session cookies, with no server storage)
app.use(session(app)); // note koa-session@3.4.0 is v1 middleware which generates deprecation notice


// sometimes useful to be able to track each request...
app.use(async function(ctx, next) {
    debug(ctx.method + ' ' + ctx.url);
    await next();
});

// logging
// const access = { type: 'rotating-file', path: './logs/api-access.log', level: 'trace', period: '1d', count: 4 };
// const error  = { type: 'rotating-file', path: './logs/api-error.log',  level: 'error', period: '1d', count: 4 };
// const logger = bunyan.createLogger({ name: 'api', streams: [ access, error ] });
// app.use(koaLogger(logger, {}));



// content negotiation: api will respond with json, xml, or yaml
app.use(async function contentNegotiation(ctx, next) {
    await next();

    if (!ctx.body) return; // no content to return

    // check Accept header for preferred response type
    const type = ctx.accepts('json', 'xml', 'yaml', 'text');
    switch (type) {
        case 'json':
        default:
            delete ctx.body.root; // xml root element
            break; // ... koa takes care of type
        case 'xml':
            ctx.type = type;
            const root = ctx.body.root; // xml root element
            delete ctx.body.root;
            ctx.body = xmlify(ctx.body, root);
            break;
        case 'yaml':
        case 'text':
            delete ctx.body.root; // xml root element
            ctx.type = 'yaml';
            ctx.body = yaml.dump(ctx.body);
            break;
        case false:
            ctx.throw(200, '', { code: '00002' }); // "Not acceptable" - can't furnish whatever was requested
            break;
    }
});


// handle thrown or uncaught exceptions anywhere down the line
app.use(async function handleStatus(ctx, next) {
    const start = new Date();
    let ms;
    try {
        await next();
    } catch (e) {
        ms = new Date() - start;
        //记录异常日志
        errorlog.error(utils.formatErrorLog('handleStatus', e, true, ctx, ms));
        ctx.response.status = e.statusCode || e.status || 500;

        ctx.body = RCODE[e.code];
    }
});

app.use(require('./routes/auth'));
app.use(async function verifyJwt(ctx, next) {
    const start = new Date();
    let ms;
    if (!ctx.headers['authorization']) ctx.throw(200, '', { code: '00003' });
    const [scheme, token] = ctx.headers['authorization'].split(' ');
    if (scheme != 'Bearer') ctx.throw(417);
    try {
        const payload = jwt.verify(token, SIGN_KEY);
        const customer = await Customer.findCustomerByName(payload.name);
        const admin = await Admin.findAdminByName(payload.name);
        ctx.state.user = customer || admin;
        if (!ctx.state.user) return ctx.throw(200, '', { code: '00003' });
    } catch (e) {
        ms = new Date() - start;
        errorlog.error(utils.formatErrorLog('verifyJwt', e, true, ctx, ms));
        if (e.message == 'invalid signature' || e.message == 'jwt expired' || e.message == 'jwt malformed') return ctx.throw(200, e.message, { code: '00003' });
        ctx.throw(e);
    }
    await next();
});

app.use(require(`./routes`));

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */


/* create server - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */


module.exports = app;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */