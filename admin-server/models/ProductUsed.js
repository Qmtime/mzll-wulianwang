module.exports = function(sequelize, DataTypes) {
    const productUsed = sequelize.define('productUsed', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        product_code: {
            type: DataTypes.STRING(32),
            comment: '产品编码'
        },
        product_name: {
            type: DataTypes.STRING(32),
            comment: '产品名称'
        },
        product_capacity: {
            type: DataTypes.INTEGER(10),
            comment: '产品容量'
        },
        product_used: {
            type: DataTypes.INTEGER(10),
            comment: '产品已用量'
        },
        product_left: {
            type: DataTypes.INTEGER(10),
            comment: '产品剩余量'
        },
        extra_total_fee: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '超出套餐外费用'
        },
        date: {
            type: DataTypes.DATE,
            comment: '日/月使用情况日期/月份'
        },
        date_type: {
            type: DataTypes.INTEGER(3),
            comment: '统计周期类型：1:日统计;2:月统计'
        },
        product_ype: {
            type: DataTypes.INTEGER(3),
            comment: '产品类型：1-流量;2-短信;3-语音;4-流量池'
        },
        order_no: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: '奇妙物联流水号'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {}
    });
    return productUsed;
};