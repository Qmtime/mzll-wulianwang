module.exports = function(sequelize, DataTypes) {
    const PoolMember = sequelize.define('poolMember', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        pool_code: {
            type: DataTypes.STRING(32),
            unique: true,
            comment: '流量池编码'
        },
        pool_name: {
            type: DataTypes.STRING(32),
            comment: '流量池名称'
        },
        member_count: {
            type: DataTypes.INTEGER(10),
            comment: '流量池用户量'
        },
        date: {
            type: DataTypes.DATE,
            comment: '日/月统计量日期/月份'
        },
        type: {
            type: DataTypes.INTEGER(3),
            comment: '类型：1:日统计;2:月统计'
        },
        order_no: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: '奇妙物联流水号'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {}
    });
    return PoolMember;
};