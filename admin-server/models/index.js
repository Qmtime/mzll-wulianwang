const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const _ = require('lodash');
const CONFIG = require('../config/base.config');

const env = process.env.NODE_ENV || "development";

const config = {
    dialect: 'mysql',
    port: CONFIG.mysql.port,
    define: {
        // 字段以下划线（_）来分割（默认是驼峰命名风格）
        'underscored': true
    },
    replication: {
        read: [
            { host: CONFIG.mysql.readHost, username: CONFIG.mysql.username, password: CONFIG.mysql.password }
        ],
        write: {
            host: CONFIG.mysql.writeHost,
            username: CONFIG.mysql.username,
            password: CONFIG.mysql.password
        }
    },
    pool: {
        max: 30,
        min: 0,
        idle: 10000
    },
    timezone: '+08:00',
    logging: CONFIG.mysqlDebug
};

const sequelize = new Sequelize(CONFIG.mysql.database, CONFIG.mysql.username, CONFIG.mysql.password, config);


const Admin = sequelize.import('./Admin'),
    Role = sequelize.import('./Role'),
    Card = sequelize.import('./Card'),
    Customer = sequelize.import('./Customer'),
    Provider = sequelize.import('./Provider'),
    Apn = sequelize.import('./Apn'),
    Product = sequelize.import('./Product'),
    CustomerProduct = sequelize.import('./CustomerProduct'),
    //用量统计
    ProductUsed = sequelize.import('./ProductUsed'),
    PoolMember = sequelize.import('./PoolMember'),
    //操作记录
    ProductOrder = sequelize.import('./ProductOrder'),
    SetAlarm = sequelize.import('./SetAlarm'),
    SwitchService = sequelize.import('./SwitchService'),
    ChangeLifeCycle = sequelize.import('./ChangeLifeCycle');

//运营商
Provider.hasMany(Product);
Provider.hasMany(Card);
Provider.hasMany(ProductOrder);
Provider.hasMany(SetAlarm);
Provider.hasMany(SwitchService);
Provider.hasMany(ChangeLifeCycle);

//客户
Customer.hasMany(Card);
Customer.hasMany(ProductOrder);
Customer.hasMany(SetAlarm);
Customer.hasMany(SwitchService);
Customer.hasMany(ChangeLifeCycle);
Customer.hasMany(CustomerProduct);

//管理员
Admin.hasMany(CustomerProduct);

Card.hasMany(ProductUsed);
Card.hasMany(ProductOrder);
Card.hasMany(SetAlarm);
Card.hasMany(SwitchService);
Card.hasMany(ChangeLifeCycle);
Card.hasMany(Apn);

//产品
Product.hasMany(ProductOrder);
Product.hasMany(CustomerProduct);
Product.hasMany(ProductUsed);
Product.hasMany(PoolMember);

Role.hasMany(Admin);

//------
Admin.belongsTo(Role);

Apn.belongsTo(Card);

ProductUsed.belongsTo(Product);
ProductUsed.belongsTo(Card);

PoolMember.belongsTo(Product);

CustomerProduct.belongsTo(Customer);
CustomerProduct.belongsTo(Product);
CustomerProduct.belongsTo(Admin);

Card.belongsTo(Provider);
Card.belongsTo(Customer);

Product.belongsTo(Provider);

ProductOrder.belongsTo(Provider);
ProductOrder.belongsTo(Customer);
ProductOrder.belongsTo(Card);
ProductOrder.belongsTo(Product);

SetAlarm.belongsTo(Provider);
SetAlarm.belongsTo(Customer);
SetAlarm.belongsTo(Card);

SwitchService.belongsTo(Provider);
SwitchService.belongsTo(Customer);
SwitchService.belongsTo(Card);

ChangeLifeCycle.belongsTo(Provider);
ChangeLifeCycle.belongsTo(Customer);
ChangeLifeCycle.belongsTo(Card);



sequelize.sync({ force: false });
const db = { Admin, Role, Card, Customer, Provider, Apn, Product, CustomerProduct, ProductUsed, PoolMember, ProductOrder, SetAlarm, SwitchService, ChangeLifeCycle };

module.exports = db;