module.exports = function(sequelize, DataTypes) {
    const Product = sequelize.define('product', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        product_code: {
            type: DataTypes.STRING(32),
            unique: true,
            comment: '产品编码'
        },
        product_name: {
            type: DataTypes.STRING(32),
            comment: '产品名称'
        },
        product_io_id: {
            type: DataTypes.STRING(32),
            comment: '运营商产品编码'
        },
        product_capacity: {
            type: DataTypes.INTEGER(10),
            comment: '产品容量'
        },
        product_price: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '月费用，单位分'
        },
        type: {
            type: DataTypes.INTEGER(3),
            comment: '产品类型：1:流量;2:短信;3:语音,4:流量池'
        },
        extra_unit_fee: {
            type: DataTypes.FLOAT(40, 4),
            defaultValue: 0.0000,
            comment: '超出套餐外费用/单位'
        },
        provider_code: {
            type: DataTypes.STRING(40),
            comment: '运营商编码'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {}
    });
    return Product;
};