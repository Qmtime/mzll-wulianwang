module.exports = function(sequelize, DataTypes) {
    const ProductOrder = sequelize.define('productOrder', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        appkey: {
            type: DataTypes.STRING(32),
            comment: 'appkey名称'
        },
        transid: {
            type: DataTypes.STRING(60),
            comment: '事务编码'
        },
        order_no: {
            type: DataTypes.STRING(60),
            unique: true,
            comment: '奇妙物联流水号'
        },
        vendor_order_no: {
            type: DataTypes.STRING(40),
            comment: '运营商订单'
        },
        order_status: {
            type: DataTypes.INTEGER(3),
            comment: '订单状态标识'
        },
        msisdns: {
            type: DataTypes.STRING(),
            comment: '号码列表'
        },
        newPackage: {
            type: DataTypes.STRING(60),
            comment: '目的套餐编码'

        },
        oldPackage: {
            type: DataTypes.STRING(60),
            comment: '原始套餐编码'
        },
        newproduct_name: {
            type: DataTypes.STRING(60),
            comment: '目的套餐名称'
        },
        oldproduct_name: {
            type: DataTypes.STRING(60),
            comment: '原始套餐名称'
        },
        provider_code: {
            type: DataTypes.STRING(40),
            comment: '供应商编码'
        },
        provider_compname: {
            type: DataTypes.STRING(32),
            comment: '供应商名称'
        },
        customer_compname: {
            type: DataTypes.STRING(32),
            comment: '客户名称'
        },
        location: {
            type: DataTypes.STRING(60),
            comment: '号码归属地省市'
        },
        actionType: {
            type: DataTypes.INTEGER(3),
            comment: '办理类型：1-套餐订购;2-套餐退订;3-套餐修改'
        },
        effectiveMode: {
            type: DataTypes.INTEGER(3),
            comment: '生效类型：1-立即生失效；2-下月生失效'
        },
        expDate: {
            type: DataTypes.DATE,
            comment: '有效期, 以自然月为单位，为空默认长期有效'
        },
        request_time: {
            type: DataTypes.DATE,
            comment: '操作时间'
        },
        charge_time: {
            type: DataTypes.DATE,
            comment: '完成时间'
        },
        price: {
            type: DataTypes.DECIMAL(10, 2),
            comment: '套餐原价'
        },
        provider_discount: {
            type: DataTypes.DECIMAL(4, 4),
            comment: '供应商折扣（成本折扣）'
        },
        customer_discount: {
            type: DataTypes.DECIMAL(4, 4),
            comment: '客户折扣'
        },
        provider_fee: {
            type: DataTypes.DECIMAL(10, 2),
            comment: '成本价格'
        },
        customer_fee: {
            type: DataTypes.DECIMAL(10, 2),
            comment: '成本价格'
        },
        vendor: {
            type: DataTypes.INTEGER(3),
            comment: '运营商：1:移动;2:联通;3:电信'
        }
    }, {
        freezeTableName: true,
        associate: function(models) {}
    });
    return ProductOrder;
};