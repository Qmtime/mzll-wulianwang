module.exports = function (sequelize, DataTypes) {
    const Role = sequelize.define('role', {
        id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true, allowNull: false }, //主键
        name: {
            type: DataTypes.STRING(120)
        },
        description: {
            type: DataTypes.STRING(256)
        },
        authorities: {
            type: DataTypes.STRING()
        }
    }, {
        freezeTableName: true,
        associate: function(models) {
        }
    });
    return Role;
};

