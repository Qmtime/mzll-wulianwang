/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/*  Route to handle all							                                                  */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

'use strict';
const commonConfig = require('../config');
const API_VERSION = commonConfig['API_VERSION'];
const router = require('koa-router')(); // router middleware for koa
const auth = require(`../controller/${API_VERSION}/auth`);

router.post(`/${API_VERSION}/auth`, auth.getAuth);

module.exports = router.middleware();