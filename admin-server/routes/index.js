/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/*  Route to handle all							                                                  */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

'use strict';
const commonConfig = require('../config');
const API_VERSION = commonConfig['API_VERSION'];
const router = require('koa-router')(); // router middleware for koa
const auth = require(`../controller/${API_VERSION}/auth`);
const admin = require(`../controller/${API_VERSION}/admin`);
const card = require(`../controller/${API_VERSION}/card`);
const cardOrder = require(`../controller/${API_VERSION}/cardOrder`);
const customer = require(`../controller/${API_VERSION}/customer`);
const order = require(`../controller/${API_VERSION}/order`);
const product = require(`../controller/${API_VERSION}/product`);
const provider = require(`../controller/${API_VERSION}/provider`);

router.post(`/${API_VERSION}/auth`, auth.getAuth);
router.post(`/${API_VERSION}/orderlist`, order.orderlist);
router.post(`/${API_VERSION}/ordershowTotal`, order.ordershowTotal);
router.post(`/${API_VERSION}/cardorderlist`, cardOrder.cardorderlist);
router.post(`/${API_VERSION}/cardshowTotal`, cardOrder.cardshowTotal);
router.post(`/${API_VERSION}/queryCardDelivery`, cardOrder.queryCardDelivery);
router.post(`/${API_VERSION}/pushToCustomer`, cardOrder.pushToCustomer);
router.post(`/${API_VERSION}/getProviders`, provider.getProviders);
router.post(`/${API_VERSION}/addProvider`, provider.addProvider);
router.post(`/${API_VERSION}/editProvider`, provider.editProvider);
router.post(`/${API_VERSION}/getCustomers`, customer.getCustomers);
router.post(`/${API_VERSION}/addCustomer`, customer.addCustomer);
router.post(`/${API_VERSION}/editCustomer`, customer.editCustomer);
router.post(`/${API_VERSION}/getCards`, card.getCards);
router.post(`/${API_VERSION}/editCard`, card.editCard);
router.post(`/${API_VERSION}/addCard`, card.addCard);
router.post(`/${API_VERSION}/dispatchCard`, card.dispatchCard);
router.post(`/${API_VERSION}/getProducts`, product.getProducts);
router.post(`/${API_VERSION}/editProduct`, product.editProduct);
router.post(`/${API_VERSION}/addProduct`, product.addProduct);
router.post(`/${API_VERSION}/dispatchProduct`, product.dispatchProduct);



module.exports = router.middleware();