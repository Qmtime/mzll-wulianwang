module.exports = {
    API_VERSION: 'v1',
    SIGN_KEY: 'didizhuanshu_Q+HD}2uEydqaJ7BD>7wzUrvPVmy]TM7wDZZUt24?FzCxsu)P89Xjg;23Mg?jA]U@',
    api: {
        deliveryUrl: 'https://zhuanshuapi.mzboss.com/query_cardDelivery',
        appkey: 'hVHGa1CO6bcvjDIg',
        appsecret: '1e548f3bd57d14303001093ae3e37774'
    },
    packageCode: {
        'YY': '银桔语音',
        'JY': '金桔语音',
        'YL': '银桔流量',
        'JL': '金桔流量',
        'YT': '银桔套餐',
        'JT': '金桔套餐'
    },
    provinceCode: {
        '北京市': 'BJ'
    },
    vendorCode: {
        '1': '移动',
        '2': '联通',
        '3': '电信'
    },
    vendorMapping: {
        '移动': '1',
        '联通': '2',
        '电信': '3'
    },
    statusCode: {
        '0': '启用',
        '1': '停用'
    },
    productTime: {
        1: '月包',
        2: '季包',
        3: '半年包'
    },
    effectiveType: {
        1: '立即生效',
        2: '次月生效'
    },
    productType:{
        1:'语音包',
        2:'流量包',
        3:'组合包'
    },
    cardOrdStatus: {
        0: '申请失败',
        1: '审核中',
        2: '客户失败缓存中',
        3: '运营商失败缓存中',
        4: '申请成功',
        5: '申请失败',
        6: '申请失败',
        7: '取消中',
        8: '已取消',
        9: '异常待确认',
        10: '审核中'
    },
    packageOrdStatus: {
        0: '订购失败',
        1: '订购中',
        2: '客户失败缓存中',
        3: '运营商失败缓存中',
        4: '订购成功',
        5: '订购失败',
        6: '订购失败',
        7: '取消中',
        8: '已取消',
        9: '异常待确认'
    },
    status_code: {
        cardOrder: {
            0: '530',
            1: '201',
            2: '201',
            3: '201',
            4: '200',
            5: '530',
            6: '430',
            7: '517',
            8: '518',
            9: '519',
            10: '201'
        },
        packageOrder: {
            0: '530',
            1: '201',
            2: '201',
            3: '201',
            4: '200',
            5: '530',
            6: '430',
            7: '517',
            8: '518',
            9: '519'
        }
    },
    deliveryLabel: {
        1: '待发货',
        2: '发货失败',
        3: '已发货',
        4: '配送中',
        5: '已签收'
    },
    activatedState: {
        1: '未激活',
        2: '已激活'
    },
};