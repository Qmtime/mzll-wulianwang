module.exports = {
    appenders: {
        server: {
            "type": 'dateFile',
            'filename': '/home/MZLL/logs/zhuanshu/admin_server/server',
            "pattern": "-yyyy-MM-dd",
            "alwaysIncludePattern": true,
        },
        error: {
            "type": 'dateFile',
            'filename': '/home/MZLL/logs/zhuanshu/admin_server/error',
            "pattern": "-yyyy-MM-dd",
            "alwaysIncludePattern": true,
        }
    },
    categories: {
        default: { appenders: ['server', 'error'], level: 'all' }
    }
};
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */