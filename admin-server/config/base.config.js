const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
	NODE_ENV: Joi.string()
		.allow(['development', 'production', 'test', 'provision'])
		.default('development'),
	PORT: Joi.number()
		.default(5300),
	MYSQL_DEBUG:Joi.boolean()
		.when('NODE_ENV', {
			is: Joi.string().equal('production'),
			then: Joi.boolean().default(false),
			otherwise: Joi.boolean().default(true)
		}),
	MYSQL_HOST:Joi.string().required()
		.description('Mysql DB host url'),
	MYSQL_READHOST:Joi.string().required()
		.description('Mysql READDB host url'),
	MYSQL_WRITEHOST:Joi.string().required()
		.description('Mysql WRITEDB host url'),
	MYSQL_PORT:Joi.number()
		.default(3306),
	MYSQL_USERNAME:Joi.string().required()
		.description('Mysql DB username'),
	MYSQL_PASSWORD:Joi.string().required()
		.description('Mysql DB password'),
	MYSQL_DATABASE:Joi.string().required()
		.description('Mysql DB datatable'),
	MONGOOSE_DEBUG: Joi.boolean()
		.when('NODE_ENV', {
			is: Joi.string().equal('development'),
			then: Joi.boolean().default(true),
			otherwise: Joi.boolean().default(false)
		}),
	MONGO_HOST: Joi.string().required()
		.description('Mongo DB host url'),
	MONGO_PORT: Joi.number()
		.default(27017),
	REDIS_HOST: Joi.string().required()
		.description('Redis DB host url'),
	REDIS_PORT: Joi.number().required()
		.description('Redis DB port'),
	REDIS_AUTH: Joi.string().empty('')
		.description('Redis DB auth'),
	DB_INDEX:   Joi.string().required()
		.description('Redis DB index')
}).unknown()
	.required();

const {error, value: envVars} = Joi.validate(process.env, envVarsSchema);

if (error) {
	throw new Error(`Config validation error: ${error.message}`);
}

const config = {
	env: envVars.NODE_ENV,
	port: envVars.PORT,
	mysqlDebug: envVars.MYSQL_DEBUG,
	mysql:{
		host: envVars.MYSQL_HOST,
		port: envVars.MYSQL_PORT,
		username: envVars.MYSQL_USERNAME,
		password: envVars.MYSQL_PASSWORD,
		database: envVars.MYSQL_DATABASE,
		readHost:envVars.MYSQL_READHOST,
		writeHost:envVars.MYSQL_WRITEHOST
	},
	mongooseDebug: envVars.MONGOOSE_DEBUG,
	mongo: {
		host: envVars.MONGO_HOST,
		port: envVars.MONGO_PORT
	},
	redis:{
		host:envVars.REDIS_HOST,
		port: envVars.REDIS_PORT,
		auth: envVars.REDIS_AUTH
	}
};

module.exports = config;
