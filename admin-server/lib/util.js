/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/*  Util of assorted useful functions                                                          */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

'use strict';
const crypto = require('crypto');
const moment = require('moment');
const objectSort = require('object-sort');
const Util = {};

/**
 * Log or notify unhandled exception.
 *
 * @param method
 * @param e
 */
Util.logException = function(method, e) {
    // could eg save to log file or e-mail developer
    console.error('UNHANDLED EXCEPTION', method, e.stack===undefined?e.message:e.stack);
};

/**
 * Create a random string
 *
 * @param len          string length
 * @param radix        radix < CHARS.length
 * @returns {string}
 */
Util.randomString = function(len, radix) {
    var CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    var chars = CHARS,
        uuid = [],
        i;
    radix = radix || chars.length;
    if (len) {
        for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
    } else {
        var r;
        uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
        uuid[14] = '4';
        for (i = 0; i < 36; i++) {
            if (!uuid[i]) {
                r = 0 | Math.random() * 16;
                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
            }
        }
    }
    return uuid.join('');
};


/**
 * 判断是否为空
 * @param str
 * @returns {string}
 */
Util.isNull = function(str){
    return (String(str) == "" || str === null || str == 'null' || str == 'undefined') ? true : false;
};

/**
 * 生成num位订单号
 * @return {[type]} [description]
 */
Util.generateRandom = function(num) {
    return moment().format('YYMMDDHHmmssSSS') + Math.floor(Math.random() * Math.pow(10, num));
};

/**
 * 生成md5加密
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
Util.md5 = function(str) {
    var md5 = crypto.createHash('md5').update(str).digest('hex');
    return md5;
};

/**
 * 手机号码验证
 * @param mobile
 */
Util.isMobile = function(mobile){
    var patrn = new RegExp("^(0|86|17951)?(13[0-9]|15[012356789]|17[0135678]|18[0-9]|14[579|19[89]|166)[0-9]{8}$");
    return patrn.test(mobile);
};

/**
 * 空对象判断
 * @param obj
 * @returns {boolean}
 */
Util.isEmptyObject = function(obj) {
    for (var key in obj)
        return !1;
    return !0
};

/**
 * 对象排序
 * @param obj       Object
 * @param reverse   是否倒叙
 * @returns {{}}
 */
Util.sortObj = function(signobj) {
    signobj = objectSort(signobj);
    var parmstr = '';
    for (var i in signobj) {
        parmstr += i + signobj[i];
    }
    return parmstr;
};

/**
 * 异常抛出
 * @param status
 * @param message
 * @param stack
 */
Util.throwError = function(status,message,stack){
    let err = new Error(message);
    err.status = status || 500;
    err.stack = stack || '';
    throw err;
};

/**
 * 格式化请求日志
 * @param ctx
 * @param time
 * @returns {string}
 */
Util.formatReqLog = function(ctx, time){
    var logText = '';
    var method = ctx.method;
    //访问方法
    logText += "request method: " + method + "\n";

    //请求原始地址
    logText += "request originalUrl:  " + ctx.originalUrl + "\n";

    //客户端ip
    logText += "request client ip:  " + ctx.ip + "\n";

    //请求参数
    if (method === 'GET') {
        logText += "request query:  " + JSON.stringify(ctx.request.query) + "\n";
    } else {
        logText += "request body: " + "\n" + JSON.stringify(ctx.request.body) + "\n";
    }

    //服务器响应时间
    logText += "response time: " + time + "\n";

    return logText;
};

/**
 * 格式化响应日志
 * @param ctx
 * @param time
 * @returns {String}
 */
Util.formatResLog = function (ctx, time) {
    var logText = '';

    //响应日志开始
    logText += "\n" + "*************** response log start ***************" + "\n";

    //添加请求日志
    logText += this.formatReqLog(ctx, time);

    //响应状态码
    logText += "response status: " + ctx.status + "\n";

    //响应内容
    logText += "response body: " + "\n" + JSON.stringify(ctx.body) + "\n";

    //响应日志结束
    logText += "*************** response log end ***************" + "\n";

    return logText;
};

/**
 * 格式化错误日志
 * @param info
 * @param err
 * @param isreq
 * @param ctx
 * @param time
 * @returns {string}
 */
Util.formatErrorLog = function (info,err,isreq,ctx,time) {
    var logText = '';

    //错误信息开始
    logText += "\n" + "*************** error log start ***************" + "\n";
    //错误信息
	if(typeof info === 'string'){
		logText += "err info: " + JSON.stringify(info) + "\n";
	}else if(typeof info === 'object'){
		for(let item in info){
			logText += `${item}:${JSON.stringify(info[item])}\n`;
		}
	}
    //添加请求日志
    if(isreq) logText += this.formatReqLog(ctx, time);

    //错误名称
    logText += "err name: " + err.name + "\n";
    //错误信息
    logText += "err message: " + err.message + "\n";
    //错误详情
    logText += "err stack: " + err.stack + "\n";

    //错误信息结束
    logText += "*************** error log end ***************" + "\n";

    return logText;
};

/**
 * 格式化信息日志
 * @param info
 * @returns {string}
 */
Util.formatInfoLog = function(info){
    var logText = '';
    //信息开始
    logText += "\n" + "*************** log start ***************" + "\n";

    //信息
	if(typeof info === 'string'){
		logText += "info: " + JSON.stringify(info) + "\n";
	}else if(typeof info === 'object'){
		for(let item in info){
			logText += `${item}:${JSON.stringify(info[item])}\n`;
		}
	}

    //金额信息结束
    logText += "*************** log end ***************" + "\n";

    return logText;
};
Util.random = function(length) {
    return _randomStr(length);
};
function _randomStr(size) {
    const TEMPLATE = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const MAX = TEMPLATE.length;
    var result = '';
    for (var i = 0; i < size; i++) {
        var index = parseInt(Math.random() * MAX);
        result = result + TEMPLATE.charAt(index);
    }
    return result;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

module.exports = Util;
