/**
 * 日志处理
 */
const log4js = require('koa-log4');
const config = require('../config/log4js.config');

/**
 * 载入配置
 */
log4js.configure(config);

/**
 * 导出日志接口
 */
const serverlog = log4js.getLogger('server'),
    errorlog = log4js.getLogger('error');

export { serverlog, errorlog };