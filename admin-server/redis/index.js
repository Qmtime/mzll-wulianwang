const Redis = require("redis");
const bluebird  = require('bluebird');
import { serverlog, errorlog } from '../lib/logger.lib'; //日记记录

const CONFIG = require('../config/base.config');

const env = process.env.NODE_ENV || "development";
const connectOptions = {
	host:CONFIG.redis.host,
	port:CONFIG.redis.port,
	retry_strategy: function (options) {
		if (options.error && options.error.code === 'ECONNREFUSED') {
			// End reconnecting on a specific error and flush all commands with
			// a individual error
			serverlog.error('【Redis】------> The server refused the connection');
			return new Error('【Redis】------> The server refused the connection');
		}
		if (options.total_retry_time > 1000 * 10) {
			// End reconnecting after a specific timeout and flush all commands
			// with a individual error
			serverlog.error('【Redis】------> Retry time exhausted');
			process.exit(0);
		}
		if (options.attempt > 10) {
			// End reconnecting with built in error
			return undefined;
		}
		// reconnect after
		return Math.min(options.attempt * 100, 750);
	}
};

const redis = Redis.createClient(connectOptions);

if (env != 'development') {
	redis.auth(CONFIG.redis.auth);
}

bluebird.promisifyAll(Redis.RedisClient.prototype);
bluebird.promisifyAll(Redis.Multi.prototype);

redis.on("error", function(err) {
	serverlog.error('【Redis】------>',err);
});



module.exports = redis;